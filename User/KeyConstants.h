//
//  KeyConstants.h
//  User
//
//  Created by CSS on 18/01/18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString *const currentLocations;
extern NSString *const SourceLocations;
extern NSString *const DestinationLocations;

extern NSString *const sourceLat;
extern NSString *const sourceLang;
extern NSString *const DestinationLat;
extern NSString *const DestinationLang;

extern NSString *const vehicleName;
extern NSString *const paymentMode;
extern NSString *const cardNumber;
extern NSString *const fareType;

extern NSString *const vehicleId;
extern BOOL  const IsShedule;

@interface KeyConstants : NSObject
+(KeyConstants *)constants;

+(BOOL)CheckKeyExcistswithDict :(NSDictionary *)dict withKey:(NSString *)key;

@property(strong,nonatomic)NSDictionary *LocationInfo;
@property(strong,nonatomic)NSDictionary *RideInfo;
@property(strong,nonatomic)NSDictionary *RequestInfo;

@property(nonatomic,strong) NSString * WalletStatus;

@property(strong,nonatomic)NSDictionary *DestinationInfo;
@property(strong,nonatomic)NSDictionary *paymentInfo;
@property(strong,nonatomic)NSString * RideStatus;
@property(strong,nonatomic)NSString * RideUpdateStatus;
@property(nonatomic) BOOL paidStatus;
@property(nonatomic) BOOL otpStatus;
@property(nonatomic)BOOL RideCompleteStatus;

@end
