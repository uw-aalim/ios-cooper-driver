//
//  CommonAnimations.h
//  WalkThroughObjc
//
//  Created by CSS on 11/09/17.
//  Copyright © 2017 Appoets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CommonAnimations : NSObject

+(void)AnimationAppearenceforWalkthrough:(UIView *)View withSideOfAnimation :(BOOL)isRight;

+(void)AnimationForMoveview:(UIView *)view withAnimationDirations:(CGFloat )duration;

+(UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius;


@end
