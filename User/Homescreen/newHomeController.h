//
//  newHomeController.h
//  Provider
//
//  Created by CSS on 26/01/18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftMenuView.h"
#import "LoadingViewClass.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "AppDelegate.h"
#import <HCSStarRatingView/HCSStarRatingView.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <MessageUI/MessageUI.h>
#import "Provider-Swift.h"
#import "KOPinCodeView.h"

@interface TrackCell :UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *trackImageview;

@end

@interface newHomeController : UIViewController<GMSMapViewDelegate,CLLocationManagerDelegate,LeftMenuViewprotocol,UIGestureRecognizerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,KOPinCodeViewDelegate>{
    LeftMenuView *leftMenuViewClass;
    AppDelegate *appDelegate;
    CLLocation * CurrentUpdateLocation;
    UIView *waitingBGView;
    CLGeocoder *geocoder;
    NSString *strRating;
    NSMutableArray * arrayCollection;
    int trackCount;
    int timecountForRequestAccept;
    NSString * usrnameStr;
    NSString * passwordStr;
    AVAudioPlayer *audioPlayer;
    NSTimer * demoServerCheck;
    NSString * isValid;
    NSString * otpSyting;
    
}

#pragma Home interfaces
@property(nonatomic, strong) NSTimer *timerForReqProgress ,*TimerRequest;

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *locationBtn;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
- (IBAction)menuBtnAction:(UIButton *)sender;
- (IBAction)locationBtnActon:(UIButton *)sender;
@property(strong,nonatomic)NSMutableArray * markerList;
- (IBAction)offlineOnlineBtnDidTab:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *sosBtn;
- (IBAction)sosBtnDidtab:(UIButton *)sender;

#pragma OtpView

@property (weak, nonatomic) IBOutlet UIView *OtpView;
@property (weak, nonatomic) IBOutlet UILabel *OtpTitleView;
@property (weak, nonatomic) IBOutlet UIButton *OtpSubmitBtn;
- (IBAction)OtpSubmitBtnDidTab:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *OtpFirstTxt;
@property (weak, nonatomic) IBOutlet UITextField *OtpSecondTxt;
@property (weak, nonatomic) IBOutlet UITextField *OtpThirdTxt;
@property (weak, nonatomic) IBOutlet UITextField *OtpFourthTxt;

@property (weak, nonatomic) IBOutlet KOPinCodeView *picodeView;

#pragma Notifyview
@property (weak, nonatomic) IBOutlet UIView *mapCircle;
@property (weak, nonatomic) IBOutlet GMSMapView *notifyMap;
@property (weak, nonatomic) IBOutlet UIView *notifyView;
@property (weak, nonatomic) IBOutlet UILabel *NotifyAddressLbl;
@property (weak, nonatomic) IBOutlet UILabel *notifyUsernameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *notifyUserImageview;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *notifyUserRateView;
@property (weak, nonatomic) IBOutlet UIButton *RejectBtn;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
- (IBAction)RejectBtnAction:(UIButton *)sender;
- (IBAction)acceptBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *notifySecondsLbl;


//OFFLINEVIEW
@property (weak, nonatomic) IBOutlet UIView *offlineView;
@property (weak, nonatomic) IBOutlet UIImageView *offlineImageview;
@property (weak, nonatomic) IBOutlet UIButton *logoutBtn;
- (IBAction)nopermissionLogoutBtnAction:(UIButton *)sender;


#pragma addressView
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UILabel *addressTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIButton *navicatioBtn;
- (IBAction)NavicationBtnDidTab:(UIButton *)sender;

#pragma RideInfoView

@property (weak, nonatomic) IBOutlet UICollectionView *trackCollectionView;
@property (weak, nonatomic) IBOutlet UIView *rideInfoView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *rideRateView;
@property (weak, nonatomic) IBOutlet UIImageView *RideImageview;
@property (weak, nonatomic) IBOutlet UILabel *rideUserNameLbl;
@property (weak, nonatomic) IBOutlet UIButton *RideCancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *rideArraivedBtn;
@property (weak, nonatomic) IBOutlet UIButton *RideCallBtn;
- (IBAction)rideCallBtnDidTab:(UIButton *)sender;
- (IBAction)rideArraiveBtnAction:(UIButton *)sender;
- (IBAction)rideCancelBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *parkedStatusBtn;
- (IBAction)ParkedStatusDidTab:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *reachedStautsWaiitingView;
@property (weak, nonatomic) IBOutlet UILabel *reachedStatuslbl;


#pragma InvoiceView

@property (weak, nonatomic) IBOutlet UIView *InvoiceView;
@property (weak, nonatomic) IBOutlet UILabel *invoiceTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *invoiceTotlaLblk;
@property (weak, nonatomic) IBOutlet UILabel *invoiceTotalValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *invoiceDivider;
//@property (weak, nonatomic) IBOutlet UILabel *invoiceTipsLbl;
//@property (weak, nonatomic) IBOutlet UILabel *invoiceTipsValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *invoiceAmountPaidLbl;
@property (weak, nonatomic) IBOutlet UILabel *invoiceAmountPaidValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *invoicWalletLbl;
@property (weak, nonatomic) IBOutlet UILabel *invoiceWaletValueLbl;
@property (weak, nonatomic) IBOutlet UIImageView *invoicePaymentmodeImageview;
@property (weak, nonatomic) IBOutlet UILabel *invoicePaymentmodelbl;
@property (weak, nonatomic) IBOutlet UIButton *invoiceConfirmBtn;
@property (weak, nonatomic) IBOutlet UIView *invoiceWaittingView;
@property (weak, nonatomic) IBOutlet UILabel *invoiceWaittingLbl;

- (IBAction)invoiceConfirmBtnAction:(UIButton *)sender;

#pragma Rateview Configure

@property (weak, nonatomic) IBOutlet UIView *rateview;
@property (weak, nonatomic) IBOutlet UIImageView *RateUserImageveiw;
@property (weak, nonatomic) IBOutlet UILabel *rateNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *rateTitleLbl;
@property (weak, nonatomic) IBOutlet UITextView *rateCommentsview;
@property (weak, nonatomic) IBOutlet UIButton *RateSubmitBtn;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *RateRayteview;
- (IBAction)RateSubmitBtnDidTab:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UISegmentedControl *onlineOfflineControl;
@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UIView *menuBackgroundView;


@end
