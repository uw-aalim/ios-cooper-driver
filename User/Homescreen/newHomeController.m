//
//  newHomeController.m
//  Provider
//
//  Created by CSS on 26/01/18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import "newHomeController.h"
#import "EmailViewController.h"
#import "WalletViewController.h"
#import "YourTripViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "ViewController.h"
#import "CSS_Class.h"
#import "config.h"
#import "ProfileViewController.h"
#import "UIScrollView+EKKeyboardAvoiding.h"
#import "AFNHelper.h"
#import "Utilities.h"
#import <SocketIO/SocketIO-Swift.h>
#import "RouteViewController.h"
#import "UIImage+animatedGIF.h"
#import "EarningsViewController.h"
#import "HelpViewController.h"
#import "SummaryViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import <Lottie/Lottie.h>
#import "AnimationView.h"
#import "UIColor+CoOper-Objc.h"
#import "Provider-Swift.h"
#import "SettingsViewController.h"

@implementation TrackCell

-(void)awakeFromNib{
    [super awakeFromNib];
}

@end


@interface newHomeController ()
{
    GMSPlacesClient *placesClient;
    
}
@property CLLocationManager * location;
@property (nonatomic, retain) GMSPolyline *pathPoly;

@end

@implementation newHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    placesClient = [GMSPlacesClient sharedClient];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
   
    [KeyConstants constants].otpStatus =NO;
    [_picodeView setTypeKeyboard:UIKeyboardTypeNumbersAndPunctuation];

    [KeyConstants constants].RideStatus = @"ONLINE";
    [self checkViewUpdateStatusChange];
    _location = [[CLLocationManager alloc] init];
   // _markerList = [[NSMutableArray alloc]init];
    self.location.delegate = self;
    self.location.desiredAccuracy = kCLLocationAccuracyBest;
    self.location.distanceFilter = kCLDistanceFilterNone;
    [_location requestWhenInUseAuthorization];

    if ([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                             message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
            [alert show];
        }
    }
    arrayCollection = [[NSMutableArray alloc]init];
   NSArray * collectionArray = @[@{
                            @"image":@"arrived",
                            @"status":@"STARTED",
                            @"activeImage":@"arrived-select",
                            },@{
                            @"image":@"pickup",
                            @"status":@"ARRIVED",
                            @"activeImage":@"pickup-select",
                            },
                                 @{
                            @"image":@"finished",
                            @"status":@"PICKEDUP",
                            @"activeImage":@"finished-select",
                            }];
    [arrayCollection addObjectsFromArray:collectionArray];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self initializeDefaultValues];
    [self ViewStyleAppearence];
	if (leftMenuViewClass == NULL) {
		leftMenuViewClass = [[[NSBundle mainBundle] loadNibNamed:@"LeftMenuView" owner:self options:nil] objectAtIndex:0];
		[leftMenuViewClass setFrame:CGRectMake(-(self.view.frame.size.width), 0, self.view.frame.size.width, self.view.frame.size.height)];
		
		leftMenuViewClass.LeftMenuViewDelegate =self;
		leftMenuViewClass.backgroundColor =[UIColor whiteColor];
		[self.view addSubview:leftMenuViewClass];
		
	}
	else {
		[leftMenuViewClass updateNameAndImage];
	}
	[self BackgroundPerfomenceStartMethod];
  //  [self BackgroundPerfomenceStartMethod];
   // [self checkViewUpdateStatusChange];
}


-(void)initializeDefaultValues{
    
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    
    //SERVICE_URL = [user objectForKey:@"serviceurl"];
    Client_SECRET = [user objectForKey:@"passport"];
    ClientID = [user objectForKey:@"clientid"];
    WEB_SOCKET = [user objectForKey:@"websocket"];
    APP_NAME = [user objectForKey:@"appname"];
    APP_IMG_URL = [user objectForKey:@"appimg"];
    isValid = [user objectForKey:@"is_valid"];
    usrnameStr = [user objectForKey:@"username"];
    passwordStr =  [user objectForKey:@"password"];
    //    GMSMAP_KEY = [user objectForKey:@"mapkey"];
    //    GMSPLACES_KEY = [user objectForKey:@"placekey"];
    //    Stripe_KEY = [user objectForKey:@"stripekey"];
    
}
#pragma View Style Update
-(void)ViewStyleAppearence{
	
	self.onlineOfflineControl.tintColor = UIColor.orangeColourObjC;
    //[_onOffLineBtn setTitle:LocalizedString(@"OFFLINE") forState:UIControlStateNormal];
    //_logoutBtn.backgroundColor = BASE1Color;
    //_onOffLineBtn.backgroundColor = BASE1Color;
    //_acceptBtn.backgroundColor = BASE1Color;
    //_RejectBtn.backgroundColor = BASE2Color;
	self.invoiceConfirmBtn.backgroundColor = UIColor.blackColor;
	self.invoiceDivider.backgroundColor = UIColor.blackColor;
    //_RideCancelBtn.backgroundColor = BASE2Color;
    //_parkedStatusBtn.backgroundColor = BASE1Color;
    //_rideArraivedBtn.backgroundColor =BASE1Color;
	
	self.acceptBtn.backgroundColor = UIColor.orangeColourObjC;
	self.RejectBtn.backgroundColor = UIColor.blackColor;
	
	self.mapCircle.layer.borderColor = UIColor.orangeColourObjC.CGColor;
	self.mapCircle.layer.borderWidth = 4.0;
	self.notifyMap.userInteractionEnabled = NO;
	
	self.RideCancelBtn.backgroundColor = UIColor.orangeColourObjC;
	self.rideArraivedBtn.backgroundColor = UIColor.blackColor;
	self.parkedStatusBtn.backgroundColor = UIColor.tealColourObjC;
	
	self.OtpSubmitBtn.backgroundColor = UIColor.tealColourObjC;
	
	self.invoiceDivider.backgroundColor = UIColor.blackColor;
	//_OtpSubmitBtn.backgroundColor = BASE1Color;
}



-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    NSLog(@"called");
    CurrentUpdateLocation = [locations lastObject];
	
	[self updateCarMarker];
    
    [geocoder reverseGeocodeLocation:CurrentUpdateLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0)
        {
            CLPlacemark *placemark = [placemarks lastObject];
            NSString * currentAddress = [Utilities removeNullFromString: [NSString stringWithFormat:@"%@,%@,%@",placemark.name,placemark.locality,placemark.subAdministrativeArea]];
            NSLog(@"Placemark %@",currentAddress);
            
            
            
            [KeyConstants constants].LocationInfo = @{
                                                      currentLocations : currentAddress,
                                                      sourceLat : [NSString stringWithFormat:@"%f",CurrentUpdateLocation.coordinate.latitude],
                                                      sourceLang:[NSString stringWithFormat:@"%f",CurrentUpdateLocation.coordinate.longitude],
                                                      SourceLocations :currentAddress
                                                      
                                                      
                                                      };//Location Update to server
//            NSDictionary *params=@{@"latitude":[KeyConstants constants].LocationInfo[sourceLat],@"longitude":[KeyConstants constants].LocationInfo[sourceLang],@"address":currentAddress};
//            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
//            [afn getDataFromPath:MD_UPDATELOCATION withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
//                if (response) {
//
//                }
//            }];
        }
        else
        {
            NSLog(@"%@", error.debugDescription);
        }
    } ];
    
}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            [_location requestWhenInUseAuthorization];
            break;
            
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.location startUpdatingLocation];
            [self GetCurrentLocationUsingPlaces];
            break;
            
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.location startUpdatingLocation];
            [self GetCurrentLocationUsingPlaces];
            break;
            
        case kCLAuthorizationStatusRestricted:
            [self AlertviewForLocationFailureAction:@"Location!" :@"You have not enabled access to your location. Do you want to enable now?"];
            break;
            
        case kCLAuthorizationStatusDenied:
            [self AlertviewForLocationFailureAction:@"Location!" :@"You have not enabled access to your location. Do you want to enable now?"];
            break;
            
        default:
            break;
    }
}

-(void)updateCarMarker {
	if (CurrentUpdateLocation != NULL) {
		[self.mapView clear];
        
        // Added by Nami
        if (self.pathPoly != nil) {
            self.pathPoly.map = _mapView;
        }
        
		GMSMarker *newMarker = [GMSMarker markerWithPosition:CurrentUpdateLocation.coordinate];
		if (self.onlineOfflineControl.selectedSegmentIndex == 0) {
			UIImage *image = [UIImage imageNamed:@"online_car_driver"];
			UIImageView *markerImage = [[UIImageView alloc]initWithImage:image];
			markerImage.frame = CGRectMake(0, 0, 64, 64);
			newMarker.iconView = markerImage;
			newMarker.rotation = CurrentUpdateLocation.course;
		}
		else {
			UIImage *image = [UIImage imageNamed:@"offline_car_driver"];
			UIImageView *markerImage = [[UIImageView alloc]initWithImage:image];
			markerImage.frame = CGRectMake(0, 0, 64, 64);
			newMarker.iconView = markerImage;
			newMarker.rotation = CurrentUpdateLocation.course;
		}
		newMarker.map = self.mapView;
	}
}

#pragma Mapview Delegats
-(void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
}
-(void)AlertviewForLocationFailureAction :(NSString *)title :(NSString *)message{
    UIAlertController * alert=[UIAlertController
                               
                               alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes, please"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                    
                                    
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No, thanks"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}
//Fetch curreent locations
-(void)GetCurrentLocationUsingPlaces{

    _markerList = [[NSMutableArray alloc]init];
    [placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *placeLikelihoodList, NSError *error){
        if (error != nil) {
            NSLog(@"Pick Place error %@", [error localizedDescription]);
            return;
        }
        if (placeLikelihoodList != nil) {
            GMSPlace *place = [[[placeLikelihoodList likelihoods] firstObject] place];
            if (place != nil) {
                NSLog(@"name%@",place.placeID);
                
                //[self markerUpdateViews:place.coordinate.latitude :place.coordinate.longitude :place.name];
                //if([[KeyConstants constants].RideStatus isEqualToString:@"CATEGORY"]){
                [self loadMapview:place.coordinate.latitude :place.coordinate.longitude];
                
                //[self markerUpdateViews:place.coordinate.latitude :place.coordinate.longitude :place.name:@""];
                // [self fitAllMarkers];
                //                self.nameLabel.text = place.name;
                //self.locationTextfield.text = [[place.formattedAddress componentsSeparatedByString:@","]
                //  componentsJoinedByString:@"\n"];
                NSLog(@"%@",[[place.formattedAddress componentsSeparatedByString:@","]componentsJoinedByString:@""]);
                [KeyConstants constants].LocationInfo = @{
                                                          currentLocations : place.formattedAddress,
                                                          sourceLat : [NSString stringWithFormat:@"%f",place.coordinate.latitude],
                                                          sourceLang:[NSString stringWithFormat:@"%f",place.coordinate.longitude],
                                                          SourceLocations :place.formattedAddress,
                                                          
                                                          @"placeId":place.placeID
                                                          };
                
//                NSDictionary *params=@{@"latitude":[NSString stringWithFormat:@"%f",place.coordinate.latitude],@"longitude":[NSString stringWithFormat:@"%f",place.coordinate.longitude],@"address":[[place.formattedAddress componentsSeparatedByString:@","]componentsJoinedByString:@"\n"]};
//                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
//                [afn getDataFromPath:MD_UPDATELOCATION withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
//                    if (response) {
//
//                    }
//                }];
                
                //}
                
            }
        }
    }];
}





#pragma Map Load for start and update Locations
-(void)loadMapview: (double)lat :(double)lang{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                            longitude:lang
                                                                 zoom:12];
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"map_style" withExtension:@"json"];
    NSError *error;
    
    // Set the map style by passing the URL for style.json.
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    _mapView.mapStyle = style;
    _mapView.myLocationEnabled = NO;
    _mapView.camera = camera;
    [_mapView animateToCameraPosition:camera];
}

// Marker update for new loctations
-(void)markerUpdateViews :(double)lat :(double)lang  :(NSString *)title :(NSString *)imageURL{
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(lat,lang);
    GMSMarker* marker = [GMSMarker markerWithPosition:position];
    marker.title = title;
    // marker.iconView = marker;
    marker.icon = [UIImage imageNamed:imageURL];
    marker.tracksViewChanges = YES;
    marker.map = self.mapView;
    [_markerList addObject:marker];
}
//Aftr markrt update map load for marker postions
-(void)fitAllMarkers{
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:_markerList];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    
    CLLocationCoordinate2D myLocation = ((GMSMarker *)_markerList.firstObject).position;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:myLocation coordinate:myLocation];
    
    for (GMSMarker *marker in arrayWithoutDuplicates){
        bounds = [bounds includingCoordinate:marker.position];
    }
    [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:80.0]];
    
}


#pragma GetPathForMapView
- (void)getPath
{
    NSString *googleUrl = @"https://maps.googleapis.com/maps/api/directions/json";

    NSString *urlString = [NSString stringWithFormat:@"%@?origin=%@,%@&destination=%@,%@&sensor=false&waypoints=%@&mode=driving&key=%@", googleUrl, [KeyConstants constants].LocationInfo[sourceLat], [KeyConstants constants].LocationInfo[sourceLang], [KeyConstants constants].DestinationInfo[DestinationLat], [KeyConstants constants].DestinationInfo[DestinationLang], @"",@"AIzaSyAsVN1AHgbMr2n3SYtNZxFiT7e1bz3tP5s"];
    
    NSLog(@"my driving api URL --- %@", urlString);
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error)
      {
          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
          
          NSArray *routesArray = [json objectForKey:@"routes"];
          
          if ([routesArray count] > 0)
          {
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self markerUpdateViews:[[KeyConstants constants].LocationInfo[sourceLat] doubleValue] :[[KeyConstants constants].LocationInfo[sourceLang] doubleValue] :[KeyConstants constants].LocationInfo[SourceLocations] :@"ub__ic_pin_pickup"];
                  [self markerUpdateViews:[[KeyConstants constants].DestinationInfo[DestinationLat] doubleValue] :[[KeyConstants constants].DestinationInfo[DestinationLang] doubleValue] :[KeyConstants constants].DestinationInfo[DestinationLocations] :@"ub__ic_pin_dropoff"];
                  
                  // Edited by Nami
                  self.pathPoly = nil;
                  [self.pathPoly setMap:nil];
                  NSDictionary *routeDict = [routesArray objectAtIndex:0];
                  NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                  NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                  GMSPath *path = [GMSPath pathFromEncodedPath:points];
                  self.pathPoly = [GMSPolyline polylineWithPath:path];
                  self.pathPoly.strokeWidth = 5.f;
                  // [self FetchTime:[routeDict[@"legs"][0][@"duration"][@"value"] intValue]];
                  //polyline.strokeColor = BASE2Color;
                  self.pathPoly.map = self->_mapView;
                  [self fitAllMarkers];
              });
              
          }
      }] resume];
    
}

//view chnage status
-(void)checkViewUpdateStatusChange{
    
    if(![[KeyConstants constants].RideUpdateStatus isEqualToString:[KeyConstants constants].RideStatus]){
        NSLog(@"%@",[KeyConstants constants].RideStatus);
        [KeyConstants constants].RideUpdateStatus=[KeyConstants constants].RideStatus;
        [self StatusUpdate:[KeyConstants constants].RideUpdateStatus];
    }
}
-(void)StatusUpdate :(NSString *)status{
    
    if([status isEqualToString:@"NOPERMISSION"]){
        
        LOTAnimationView *animation = [LOTAnimationView animationNamed:@"search"];
        animation.frame = CGRectMake(0, 0, _offlineImageview.frame.size.width, _offlineImageview.frame.size.height);
        animation.loopAnimation = YES;
        [self.offlineImageview addSubview:animation];
        [animation playWithCompletion:^(BOOL animationFinished) {
            // Do Something
        }];
        _logoutBtn.hidden = NO;
        //_onOffLineBtn.hidden = YES;
		self.onlineOfflineControl.hidden = YES;
        _sosBtn.hidden = YES;

        //[self.view sendSubviewToBack:_menuBtn];
//        [self.view sendSubviewToBack:_locationBtn];
        [self ShowView:_offlineView :YES];
        [self ShowView:_mapView :NO];
        [self ShowView:_notifyView :NO];
        [self ShowView:_addressView :NO];
        [self ShowView:_rideInfoView :NO];
        [self ShowView:_InvoiceView :NO];
        [self ShowView:_rateview :NO];
        [self ShowView:_OtpView :NO];
        [self.view sendSubviewToBack:_OtpView];

        
    }else if([status isEqualToString:@"OFFLINE"]){
        _logoutBtn.hidden = YES;
        //_onOffLineBtn.hidden = NO;
		self.onlineOfflineControl.hidden = NO;
		self.menuView.hidden = NO;
       _sosBtn.hidden = YES;
		
		//NSLog(@"newHomeController - set view offline");

		self.onlineOfflineControl.selectedSegmentIndex = 1;
        //[_onOffLineBtn setTitle:LocalizedString(@"ONLINE") forState:UIControlStateNormal];
        ///[self.view bringSubviewToFront:_menuBtn];
		
        //[self.view bringSubviewToFront:_onOffLineBtn];
		[self.view bringSubviewToFront:self.menuBackgroundView];
		[self.view bringSubviewToFront:self.menuView];
        [self.view sendSubviewToBack:_locationBtn];
        [self ShowView:_offlineView :NO];
        [self ShowView:_mapView :YES];
		
        [self ShowView:_addressView :NO];
        [self ShowView:_notifyView :NO];
        [self ShowView:_rideInfoView :NO];
        [self ShowView:_InvoiceView :NO];
        [self ShowView:_rateview :NO];
        [self ShowView:_OtpView :NO];
		
		//NSLog(@"Should set view offline %@", self.menuView);
		//NSLog(@"Should set view offline %@", self.offlineView);
		[self updateCarMarker];
		
        [self.view sendSubviewToBack:_OtpView];

    }else if([status isEqualToString:@"ONLINE"]){
		
		NSLog(@"newHomeController - set view online");
		
        _logoutBtn.hidden = YES;
        //_onOffLineBtn.hidden = NO;
		self.onlineOfflineControl.hidden = NO;
       _sosBtn.hidden = YES;
        [self BackgroundPerfomenceStartMethod];
        //[_onOffLineBtn setTitle:LocalizedString(@"OFFLINE") forState:UIControlStateNormal];
		self.onlineOfflineControl.selectedSegmentIndex = 0;
        //[self.view bringSubviewToFront:_menuBtn];
		
//        [self.view bringSubviewToFront:_locationBtn];
        [self ShowView:_offlineView :NO];
        [self ShowView:_mapView :YES];
        [self ShowView:_addressView :NO];
        [self ShowView:_notifyView :NO];
        [self ShowView:_rideInfoView :NO];
        [self ShowView:_InvoiceView :NO];
        [self ShowView:_rateview :NO];
        [self ShowView:_OtpView :NO];
        [self BackgroundPerfomenceStartMethod];
        [self.view sendSubviewToBack:_OtpView];
		[self.view bringSubviewToFront:self.menuBackgroundView];
		[self.view bringSubviewToFront:self.menuView];
		
		[self updateCarMarker];
		

    }
	else if([status isEqualToString:@"SEARCHING"]) {
        _logoutBtn.hidden = YES;
       // _onOffLineBtn.hidden = YES;
		self.onlineOfflineControl.hidden = YES;
        _sosBtn.hidden = YES;

        [self audioFile];
        if(![[KeyConstants constants].RequestInfo[@"request"][@"user"] isKindOfClass:[NSNull class]]){
            _notifyUsernameLbl.text = [NSString stringWithFormat:@"%@ %@",[Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"first_name"]],[Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"last_name"]]];
            _notifyUserRateView.value = [[KeyConstants constants].RequestInfo[@"request"][@"user"][@"rating"] intValue];
            
          
            NSString * imageURL = [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"picture"]];
            
            if ([imageURL containsString:@"http"])
            {
                imageURL = [NSString stringWithFormat:@"%@",[Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"picture"]]];
            }else{
                 imageURL = [NSString stringWithFormat:@"%@/storage/%@",SERVICE_URL, [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"picture"]]];
            }
            [_notifyUserImageview sd_setImageWithURL:[NSURL URLWithString:imageURL]
                        placeholderImage:[UIImage imageNamed:@"user_profile"]];
            NSLog(@"%d",timecountForRequestAccept);
            timecountForRequestAccept = [[KeyConstants constants].RequestInfo[@"time_left_to_respond"]intValue];
            [self checkAlarmTime:[[KeyConstants constants].RequestInfo[@"time_left_to_respond"]intValue]];
            _NotifyAddressLbl.text = [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"s_address"]];
			
			//[KeyConstants constants].RequestInfo
			if (([KeyConstants constants].RequestInfo[@"request"][@"s_latitude"] != NULL) &&
				([KeyConstants constants].RequestInfo[@"request"][@"s_longitude"] != NULL)) {
				
				double latitude = [(NSNumber *)([KeyConstants constants].RequestInfo[@"request"][@"s_latitude"]) doubleValue];
				double longitude = [(NSNumber *)([KeyConstants constants].RequestInfo[@"request"][@"s_longitude"]) doubleValue];
				
				GMSCameraPosition *position = [GMSCameraPosition cameraWithTarget:CLLocationCoordinate2DMake(latitude, longitude) zoom:15.0];
				[self.notifyMap animateToCameraPosition: position];
			}
		
        }

       // [_onOffLineBtn setTitle:@"OFFLINE" forState:UIControlStateNormal];
       // [self.view bringSubviewToFront:_menuBtn];
		[self.view bringSubviewToFront:self.menuBackgroundView];
        [self.view bringSubviewToFront:self.menuView];
		
//        [self.view bringSubviewToFront:_locationBtn];
        [self ShowView:_offlineView :NO];
        //[self ShowView:_mapView :YES];
        [self ShowView:_addressView :NO];
        [self ShowView:_notifyView :YES];
        [self ShowView:_rideInfoView :NO];
        [self ShowView:_InvoiceView :NO];
        [self ShowView:_rateview :NO];
        [self ShowView:_OtpView :NO];
        [self.view sendSubviewToBack:_OtpView];

    }
	else if([status isEqualToString:@"STARTED"] || [status isEqualToString:@"ARRIVED"] || [status isEqualToString:@"PICKEDUP"] ){
        [_mapView clear];
		[self updateCarMarker];
        if([status isEqualToString:@"PICKEDUP"]){
             _sosBtn.hidden = NO;
        }else{
            _sosBtn.hidden = YES;
        }
       
        
        _logoutBtn.hidden = YES;
        //_onOffLineBtn.hidden = YES;
		self.onlineOfflineControl.hidden = YES;
        
        
        if(![[KeyConstants constants].RequestInfo[@"request"][@"user"] isKindOfClass:[NSNull class]]){
            _rideUserNameLbl.text = [NSString stringWithFormat:@"%@ %@",[Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"first_name"]],[Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"last_name"]]];
            _rideRateView.value = [[KeyConstants constants].RequestInfo[@"request"][@"user"][@"rating"] intValue];
          //  NSString * imageURL = [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"picture"]];
            
      
            
            NSString * imageURL = [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"picture"]];
            
            
            if ([imageURL containsString:@"http"])
            {
                imageURL = [NSString stringWithFormat:@"%@",[Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"picture"]]];
            }else{
                imageURL = [NSString stringWithFormat:@"%@/storage/%@",SERVICE_URL, [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"picture"]]];
            }
            
            [_RideImageview sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                    placeholderImage:[UIImage imageNamed:@"user_profile"]];
            
            
        }
        if([status isEqualToString:@"STARTED"]){
            _parkedStatusBtn.hidden = YES;
            _rideArraivedBtn.hidden = NO;
            _RideCancelBtn.hidden = NO;
            _addressTitleLbl.text = @"Pick up Location";
            [self.view sendSubviewToBack:_parkedStatusBtn];
            _reachedStautsWaiitingView.hidden = YES;

            _addressLbl.text = [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"s_address"]];
           	[_rideArraivedBtn setTitle:@"ARRIVED" forState:UIControlStateNormal];

        }else if([status isEqualToString:@"ARRIVED"]){
            [_picodeView initPinWithCountView:4];
            [_picodeView setSecure:NO];
            _parkedStatusBtn.hidden = YES;
            _rideArraivedBtn.hidden = NO;
            _RideCancelBtn.hidden = NO;
            _addressTitleLbl.text = @"Pick up Location";
            _reachedStautsWaiitingView.hidden = YES;
            _addressLbl.text = [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"s_address"]];
            [self.view sendSubviewToBack:_parkedStatusBtn];
            [_rideArraivedBtn setTitle:@"PICKED UP" forState:UIControlStateNormal];

        }else if([status isEqualToString:@"PICKEDUP"]){
            
            
            
                _parkedStatusBtn.hidden = NO;
                _rideArraivedBtn.hidden = YES;
                _RideCancelBtn.hidden = YES;
                [self.view bringSubviewToFront:_parkedStatusBtn];

                [_parkedStatusBtn setTitle:@"DROPPED OFF" forState:UIControlStateNormal];
             _addressLbl.text = [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"d_address"]];
            _addressTitleLbl.text = @"Dropped Location";
            _reachedStautsWaiitingView.hidden = YES;


        }
        [_trackCollectionView reloadData];
		
		[self.view bringSubviewToFront:self.menuBackgroundView];
		[self.view bringSubviewToFront:self.menuView];
		
            //[self.view bringSubviewToFront:_onOffLineBtn];
//            [self.view bringSubviewToFront:_locationBtn];
            [self ShowView:_offlineView :NO];
            //[self ShowView:_mapView :YES];
            [self ShowView:_addressView :YES];
            [self ShowView:_notifyView :NO];
            [self ShowView:_rideInfoView :YES];
            [self ShowView:_InvoiceView :NO];
            [self ShowView:_rateview :NO];
            [self ShowView:_OtpView :NO];
            [self.view sendSubviewToBack:_OtpView];

        
        
    }else if([status isEqualToString:@"DROPPED"]){
        _logoutBtn.hidden = YES;
        //_onOffLineBtn.hidden = YES;
		self.onlineOfflineControl.hidden = YES;
        _sosBtn.hidden = NO;

          NSString *currency = [[NSUserDefaults standardUserDefaults] valueForKey:@"currency"];
        if(![[KeyConstants constants].RequestInfo[@"request"][@"payment"] isKindOfClass:[NSNull class]]){
            _invoiceTitleLbl.text = [NSString stringWithFormat:@"INVOICE -%@",[KeyConstants constants].RequestInfo[@"request"][@"booking_id"]];
            _invoiceTotalValueLbl.text = [NSString stringWithFormat:@"%@ %@",currency,[KeyConstants constants].RequestInfo[@"request"][@"payment"][@"total"]];
            
            //_invoiceTipsLbl.text = @"";
            //_invoiceTipsValueLbl.text = @"";
//            _invoiceTipsValueLbl.text =[NSString stringWithFormat:@"%@ %@",currency,[KeyConstants constants].RequestInfo[@"request"][@"payment"][@"tips"]];
            _invoiceAmountPaidValueLbl.text = [NSString stringWithFormat:@"%@ %@",currency,[KeyConstants constants].RequestInfo[@"request"][@"payment"][@"payable"]];
            if([[KeyConstants constants].RequestInfo[@"request"][@"use_wallet"] intValue] == 0){
                _invoicWalletLbl.text = @"";
                _invoiceWaletValueLbl.text = @"";
            }else{
                _invoicWalletLbl.text = @"Wallet";
                _invoiceWaletValueLbl.text = [NSString stringWithFormat:@"%@ %.2f",currency,[[KeyConstants constants].RequestInfo[@"request"][@"payment"][@"wallet"] doubleValue]];
            }
            
            if ([[KeyConstants constants].RequestInfo[@"request"][@"payment_mode"] isEqualToString:@"CASH"]){
                _invoiceWaittingView.hidden = YES;
                _invoiceConfirmBtn.hidden = NO;
                
                _invoicePaymentmodeImageview.image = [UIImage imageNamed:@"money_icon"];
            }else{
                _invoiceWaittingView.hidden = NO;
                _invoiceConfirmBtn.hidden = YES;
                _invoicePaymentmodeImageview.image = [UIImage imageNamed:@"payment"];
            }
            _invoicePaymentmodelbl.text = [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"payment_mode"]];
            
        }
        
        //[self.view bringSubviewToFront:_menuBtn];
		[self.view bringSubviewToFront:self.menuBackgroundView];
        [self.view bringSubviewToFront:self.menuView];
		
//        [self.view bringSubviewToFront:_locationBtn];
        [self ShowView:_offlineView :NO];
        //[self ShowView:_mapView :YES];
        [self ShowView:_addressView :NO];
        [self ShowView:_notifyView :NO];
        [self ShowView:_rideInfoView :NO];
        [self ShowView:_InvoiceView :YES];
        
        [self ShowView:_rateview :NO];
        [self ShowView:_OtpView :NO];
        [self.view sendSubviewToBack:_OtpView];

    }else if([status isEqualToString:@"OTP"]){
        _sosBtn.hidden = NO;
        _logoutBtn.hidden = YES;
        //_onOffLineBtn.hidden = YES;
		self.onlineOfflineControl.hidden = YES;
        _parkedStatusBtn.hidden = YES;
        _rideArraivedBtn.hidden = NO;
        _RideCancelBtn.hidden = NO;
        _addressTitleLbl.text = @"Pick up Location";
        _reachedStautsWaiitingView.hidden = YES;
        _addressLbl.text = [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"s_address"]];
        [self.view sendSubviewToBack:_parkedStatusBtn];
		
		[self.view bringSubviewToFront:self.menuBackgroundView];
        [self.view bringSubviewToFront:self.menuView];
		
        //[self.view bringSubviewToFront:_onOffLineBtn];
//        [self.view bringSubviewToFront:_locationBtn];
        [self ShowView:_offlineView :NO];
        //[self ShowView:_mapView :YES];
        [self ShowView:_addressView :YES];
        [self ShowView:_notifyView :NO];
        [self ShowView:_rideInfoView :YES];
        [self ShowView:_OtpView :YES];
        [self ShowView:_InvoiceView :NO];
        [self.view bringSubviewToFront:_OtpView];

    }else if([status isEqualToString:@"COMPLETED"]){
        _logoutBtn.hidden = YES;
        self.onlineOfflineControl.hidden = YES;
        _sosBtn.hidden = YES;
        [_mapView clear];
        
        // Nami added: ---
        self.pathPoly = nil;
        
		[self updateCarMarker];
        _rateCommentsview.text = @"Write your comments";
        [_markerList removeAllObjects];
        
        if(![[KeyConstants constants].RequestInfo[@"request"][@"user"] isKindOfClass:[NSNull class]]){
            _rateNameLbl.text = [NSString stringWithFormat:@"%@ %@",[Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"first_name"]],[Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"last_name"]]];
           
           // NSString * imageURL = [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"picture"]];
            
           // NSString * imageURL = [NSString stringWithFormat:@"%@/storage/%@",SERVICE_URL, [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"picture"]]];
            NSString * imageURL = [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"picture"]];
            
            if ([imageURL containsString:@"http"])
            {
                imageURL = [NSString stringWithFormat:@"%@",[Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"picture"]]];
            }else{
                imageURL = [NSString stringWithFormat:@"%@/storage/%@",SERVICE_URL, [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"picture"]]];
            }
            
            [_RateUserImageveiw sd_setImageWithURL:[NSURL URLWithString:imageURL]
                              placeholderImage:[UIImage imageNamed:@"user_profile"]];
            
        }
        
        [self.view bringSubviewToFront:self.menuBackgroundView];
        [self.view bringSubviewToFront:self.menuView];
		
        //[self.view bringSubviewToFront:_onOffLineBtn];
//        [self.view bringSubviewToFront:_locationBtn];
        [self ShowView:_offlineView :NO];
        //[self ShowView:_mapView :YES];
        [self ShowView:_addressView :NO];
        [self ShowView:_notifyView :NO];
        [self ShowView:_rideInfoView :NO];
        [self ShowView:_InvoiceView :NO];
        [self ShowView:_rateview :YES];
        [self ShowView:_OtpView :NO];
        [self.view sendSubviewToBack:_OtpView];

    }
}


-(void)pushTransition:(CFTimeInterval)duration :(UIView *)view withDirection :(int)direction
{
    CATransition *animation = [CATransition new];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.type = kCATruncationNone;
    if(direction == 1){
        animation.subtype = kCATransitionFromRight;
    }else if(direction == 2){
        animation.subtype = kCATransitionFromLeft;
    }else if (direction == 3){
        animation.subtype = kCATransitionFromTop;
        
    }else{
        animation.subtype = kCATransitionFromBottom;
        
    }
    
    animation.duration = duration;
    [view.layer addAnimation:animation forKey:kCATransitionMoveIn];
}


-(void)ShowView :(UIView *)view :(BOOL)isShow{
    if(isShow){
        
        if([[KeyConstants constants].RideStatus isEqualToString:@"STARTED"] || [[KeyConstants constants].RideStatus isEqualToString:@"ARRIVED"] || [[KeyConstants constants].RideStatus isEqualToString:@"PICKEDUP"]){
            [self pushTransition:0.0 :view withDirection:3];
            
        }else{
            [self pushTransition:0.0 :view withDirection:3];
            
        }
        view.hidden = NO;
        
    }else{
        view.hidden = YES;
        [self pushTransition:0.0 :view withDirection:4];
        
    }
    
}

#pragma mark - Slide Menu controllers

-(void)LeftMenuView{
    
    if(self->leftMenuViewClass.frame.origin.x == -(self.view.frame.size.width)){
        [UIView animateWithDuration:0.3 animations:^{
            
            self->leftMenuViewClass.frame = CGRectMake(0, 0, self.view.frame.size.width,  self.view.frame.size.height);
            
        }];
		[self->leftMenuViewClass updateNameAndImage];
        [self backgroundPerformStopMethod];
        waitingBGView = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width  ,self.view.frame.size.height)];
        UITapGestureRecognizer *tapGesture_condition=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ViewOuterTap)];
        tapGesture_condition.cancelsTouchesInView=NO;
        tapGesture_condition.delegate=self;
        [waitingBGView addGestureRecognizer:tapGesture_condition];
        [waitingBGView setBackgroundColor:[UIColor blackColor]];
        [waitingBGView setAlpha:0.6];
        [self.view addSubview:waitingBGView];
        [self.view bringSubviewToFront:self->leftMenuViewClass];
        
    }
	else {
        [UIView animateWithDuration:0.3 animations:^{
            
            self->leftMenuViewClass.frame = CGRectMake(-(self.view.frame.size.width), 0, self.view.frame.size.width,  self.view.frame.size.height);
            
        }];
        [self BackgroundPerfomenceStartMethod];
        [waitingBGView removeFromSuperview];
    }

}
- (void)ViewOuterTap{
    
    [self LeftMenuView];
}


/*
//left view selected index action
-(void)setSelectedIndexforMenu:(NSString *)title{
    
    [self LeftMenuView];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

    if([title isEqualToString:LocalizedString(@"Your Trips")]){
        YourTripViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"YourTripViewController"];
        [self.navigationController pushViewController:wallet animated:YES];
        
    }
	else if([title isEqualToString:LocalizedString(@"Earnings")]){
        EarningsViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"EarningsViewController"];
        [self.navigationController pushViewController:wallet animated:YES];
    }else if([title isEqualToString:LocalizedString(@"Summary")]){
        SummaryViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"SummaryViewController"];
        [self.navigationController pushViewController:wallet animated:YES];
    }else if([title isEqualToString:LocalizedString(@"Help")]){
        HelpViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
        [self.navigationController pushViewController:wallet animated:YES];
    }else if([title isEqualToString:LocalizedString(@"Share")]){
        
        UIImage *img = [UIImage imageNamed:@"icon"];
        NSMutableArray *sharingItems = [NSMutableArray new];
        [sharingItems addObject:@"coOper DRIVER"];
        [sharingItems addObject:img];
        [sharingItems addObject:[NSURL URLWithString:@"https://itunes.apple.com/us/app/coOper-driver/id1204269279?mt=8"]];
        
        UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
        [self presentViewController:activityController animated:YES completion:nil];
        
    }else if([title isEqualToString:LocalizedString(@"Settings")]){
        
        settingdController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"settingdController"];
        [self.navigationController pushViewController:wallet animated:YES];
    }else if([title isEqualToString:@"Logout"]){
        [self logOut];
    }
} */

//profileview
-(void)profileView {
    [self LeftMenuView];
    //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

    ProfileViewController *viewController = [ProfileViewController initController];
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)yourTripsView {
	[self LeftMenuView];
	UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
	YourTripViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"YourTripViewController"];
	[self.navigationController pushViewController:wallet animated:YES];

}

-(void)yourDocuments {
    [self LeftMenuView];
    
    YourDocumentsViewController * vc = [YourDocumentsViewController getNewInstance];
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)earningsView {
	[self LeftMenuView];
	UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
	EarningsViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"EarningsViewController"];
	[self.navigationController pushViewController:wallet animated:YES];
}

-(void)summaryView {
	[self LeftMenuView];
	UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
	SummaryViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"SummaryViewController"];
	[self.navigationController pushViewController:wallet animated:YES];
}

-(void)helpView {
	[self LeftMenuView];
	UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
	HelpViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
	[self.navigationController pushViewController:wallet animated:YES];
}

-(void)shareView {
	[self LeftMenuView];
	UIImage *img = [UIImage imageNamed:@"icon"];
	NSMutableArray *sharingItems = [NSMutableArray new];
	[sharingItems addObject:@"coOper DRIVER"];
	[sharingItems addObject:img];
	[sharingItems addObject:[NSURL URLWithString:@"https://itunes.apple.com/us/app/coOper-driver/id1204269279?mt=8"]];
	
	UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
	[self presentViewController:activityController animated:YES completion:nil];
}

-(void)Settings {
	[self LeftMenuView];
	//UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
	SettingsViewController *viewController = [SettingsViewController initController];
	[self.navigationController pushViewController:viewController animated:YES];
}

-(void)logOutMenu {
	[self LeftMenuView];
	[self logOut];
}

-(void)closeMenuPressed {
	[self LeftMenuView];
}

#pragma GetIncomming request
-(void)getIncommingRequest{

dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    dispatch_async(dispatch_get_main_queue(), ^{
    if([appDelegate internetConnected])
    {
        
      NSDictionary * params =  @{@"latitude":[NSString stringWithFormat:@"%.8f", CurrentUpdateLocation.coordinate.latitude], @"longitude":[NSString stringWithFormat:@"%.8f", CurrentUpdateLocation.coordinate.longitude]};
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        
        [afn getDataFromPath_NoLoader:INCOMING_REQUEST withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response && error == nil){
                
              
                [self.mapView clear];
				[self updateCarMarker];
                [_markerList removeAllObjects];
                
                // Nami added: ---
                self.pathPoly = nil;
                
                if([response[@"requests"] count] == 0){
                    
                    if([response[@"account_status"] isEqualToString:@"banned"]|| [response[@"account_status"] isEqualToString:@"onboarding"]){
                        [KeyConstants constants].RideStatus = @"NOPERMISSION";
                    }else if([response[@"account_status"] isEqualToString:@"approved"]){
                        if([response[@"service_status"] isEqualToString:@"active"]){
                            [self GetCurrentLocationUsingPlaces];
                             [KeyConstants constants].RideStatus = @"ONLINE";
                        }else{
                             [KeyConstants constants].RideStatus = @"OFFLINE";
                        }
                    }
                    if([KeyConstants constants].RequestInfo != nil){
                        [self TimerStop];
                        
                        [KeyConstants constants].RideStatus = @"ONLINE";
                        
                    }
                    [self checkViewUpdateStatusChange];
                }else{
                    [KeyConstants constants].RequestInfo = [response[@"requests"] firstObject];
                    
                    [[NSUserDefaults standardUserDefaults]setValue:[KeyConstants constants].RequestInfo[@"request_id"] forKey:@"request_id"];
                    NSLog(@"%@",[KeyConstants constants].RequestInfo);
                    
                    if([[KeyConstants constants].RequestInfo[@"request"][@"status"] isEqualToString:@"STARTED"] || [[KeyConstants constants].RequestInfo[@"request"][@"status"] isEqualToString:@"ARRIVED"]){
                       
                        [KeyConstants constants].LocationInfo = @{
                                                                  sourceLat:[NSString stringWithFormat:@"%f",CurrentUpdateLocation.coordinate.latitude],
                                                                  sourceLang :[NSString stringWithFormat:@"%f",CurrentUpdateLocation.coordinate.longitude],
                                                                //  SourceLocations :[KeyConstants constants].RequestInfo[@"request"][@"s_address"],
                                                                //  currentLocations:[KeyConstants constants].RequestInfo[@"request"][@"s_address"],
                                                                  };
                        
                        [KeyConstants constants].DestinationInfo = @{
                                                                     DestinationLat :[KeyConstants constants].RequestInfo[@"request"][@"s_latitude"],
                                                                     DestinationLang:[KeyConstants constants].RequestInfo[@"request"][@"s_longitude"],
                                                                     DestinationLocations:[KeyConstants constants].RequestInfo[@"request"][@"s_address"]
                                                                     
                                                                     };
                    }else{
                        
                        [KeyConstants constants].LocationInfo = @{
                                                                  sourceLat:[KeyConstants constants].RequestInfo[@"request"][@"s_latitude"],
                                                                  sourceLang :[KeyConstants constants].RequestInfo[@"request"][@"s_longitude"],
                                                                  SourceLocations :[KeyConstants constants].RequestInfo[@"request"][@"s_address"],
                                                                  currentLocations:[KeyConstants constants].RequestInfo[@"request"][@"s_address"],
                                                                  };
                        
                        [KeyConstants constants].DestinationInfo = @{
                                                                     DestinationLat :[KeyConstants constants].RequestInfo[@"request"][@"d_latitude"],
                                                                     DestinationLang:[KeyConstants constants].RequestInfo[@"request"][@"d_longitude"],
                                                                     DestinationLocations:[KeyConstants constants].RequestInfo[@"request"][@"d_address"]
                                                                     
                                                                 };
                       
                        
                    }
                    
                    if(![[KeyConstants constants].RequestInfo[@"request"][@"status"] isEqualToString:@"COMPLETED"] || (![[KeyConstants constants].RequestInfo[@"request"][@"status"] isEqualToString:@"SEARCHING"])){
                        [self getPath];
                        
                        // added by Nami
//                        [self backgroundPerformStopMethod];
                    }else{
                        [_mapView clear];
						[_markerList removeAllObjects];
                        
                        // Nami added: ---
                        self.pathPoly = nil;
                        
						[self updateCarMarker];
                    }
                    
                    if(![KeyConstants constants].otpStatus){
                        [KeyConstants constants].RideStatus = [KeyConstants constants].RequestInfo[@"request"][@"status"];
                    }
                    [self checkViewUpdateStatusChange];
                }
                
                
            }else{
                NSLog(@"%@",error);
                if ([errorcode intValue]==2)
                {
                    if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                    {
                        //Refresh token
                        [self TokenExpired];
                    }
                    else
                    {
                        [self TokenExpired];
                    }
                }
            }
        }];
    }
    });
});
}


-(void)TimerStop{
    if(audioPlayer != nil){
        [audioPlayer stop];
        audioPlayer = nil;
    }
    if(_TimerRequest != nil){
        [_TimerRequest invalidate];
        _TimerRequest = nil;
    }
}
-(void)audioFile
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *filePath = [mainBundle pathForResource:@"alert_tone" ofType:@"mp3"];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    
    NSError *error = nil;
    
    if(audioPlayer == nil){
        [audioPlayer stop];
        audioPlayer = [[AVAudioPlayer alloc] initWithData:fileData error:&error];
        [audioPlayer prepareToPlay];
        [audioPlayer play];
        [audioPlayer setVolume:3];
        [audioPlayer setNumberOfLoops:-1];
    }
    
}

-(void)checkAlarmTime :(int)timeDuration{
    
    if(_TimerRequest == nil){
        _TimerRequest = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                         target: self
                                                       selector:@selector(timerUpdate)
                                                       userInfo: nil repeats:YES];
    }
   
}

-(void)timerUpdate{
    
    if(timecountForRequestAccept > 0){
        timecountForRequestAccept = timecountForRequestAccept-1;
        if(timecountForRequestAccept != 0){
            _notifySecondsLbl.text = [NSString stringWithFormat:@"%d",timecountForRequestAccept];
            
        }else{
            [_TimerRequest invalidate];
            _TimerRequest = nil;
            [audioPlayer stop];
            audioPlayer = nil;
        }
    }
    
    
}
-(void)BackgroundPerfomenceStartMethod{
    if(_timerForReqProgress == nil){
        _timerForReqProgress=[NSTimer scheduledTimerWithTimeInterval:5.0
                                                              target:self
                                                            selector:@selector(getIncommingRequest)
                                                            userInfo:nil
                                                             repeats:YES];
    }
    
}
-(void)backgroundPerformStopMethod{
    [_timerForReqProgress invalidate];
    _timerForReqProgress = nil;
}
//Menu btn action
- (IBAction)menuBtnAction:(UIButton *)sender {
    [self LeftMenuView];
    
}
//location btn action
- (IBAction)locationBtnActon:(UIButton *)sender {
    [self GetCurrentLocationUsingPlaces];
}
//Reject btn action
- (IBAction)RejectBtnAction:(UIButton *)sender {
    [audioPlayer stop];
    audioPlayer=nil;

    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"Are you sure to want to cancel this ride.") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction actionWithTitle:LocalizedString(@"NO") style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        MINT_METHOD_TRACE_START
        if([appDelegate internetConnected]){
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *request_ID = [defaults valueForKey:@"request_id"];
            
            NSString *url = [NSString stringWithFormat: @"%@/api/provider/trip/%@", SERVICE_URL, request_ID];
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:DELETE_METHOD];
            [afn getDataFromPath:url  withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
             {
                 if (response)
                 {
                     NSLog(@"RESPONSE ...%@", response);
                     
                     
                     [_mapView clear];
                     [_markerList removeAllObjects];
                     
                     // Nami added: ---
                     self.pathPoly = nil;
                     
					 [self updateCarMarker];
                     [KeyConstants constants].RideStatus = @"ONLINE";
                     [self checkViewUpdateStatusChange];
                 }
                 else
                 {
                     if ([strErrorCode intValue]==1)
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                     else if ([strErrorCode intValue]==2)
                     {
                         if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                         {
                             //Refresh token
                         }
                         else
                         {
                             [self logOut];
                         }
                     }
                     else if ([strErrorCode intValue]==3)
                     {
                         if ([error objectForKey:@"password"]) {
                             [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                         } else if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                         {
                             //Refresh token
                             [self TokenExpired];
                             
                         }
                         else
                         {
                             [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                         }
                     }
                     else
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                     NSLog(@"%@",error);
                     
                 }
             }];
        }
        else
        {
            [CSS_Class alertviewController_title:@"" MessageAlert:LocalizedString(@"CONNECTION") viewController:self okPop:NO];

        }
    }];
    [alertController addAction:ok];
    [alertController addAction:no];
    [self presentViewController:alertController animated:YES completion:nil];
        MINT_NONARC_METHOD_TRACE_STOP
}
//AcceptBtn action
- (IBAction)acceptBtnAction:(UIButton *)sender {
    
    [audioPlayer stop];
    audioPlayer=nil;

        if([appDelegate internetConnected])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *request_ID = [defaults valueForKey:@"request_id"];
            
            NSString *url = [NSString stringWithFormat: @"/api/provider/trip/%@", request_ID];
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:url  withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
             {
                 if (response)
                 {
                     NSLog(@"RESPONSE ...%@", response);
                     
                     if (IsShedule){
                         [KeyConstants constants].RideStatus = @"ONLINE";
                     }
                     else{
                        
                     }
                     [self TimerStop];
                     [self checkViewUpdateStatusChange];
                     
                 }
                 else
                 {
                     if ([strErrorCode intValue]==1)
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                     else if ([strErrorCode intValue]==2)
                     {
                         if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                         {
                             //Refresh token
                         }
                         else
                         {
                             [self logOut];
                         }
                     }
                     else if ([strErrorCode intValue]==3)
                     {
                         
                         if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                         {
                             //Refresh token
                             [self TokenExpired];
                             
                         }else if ([error objectForKey:@"password"]) {
                             
                             [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                         }
                         else
                         {
                             [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                         }
                     }
                     else
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                     NSLog(@"%@",error);
                     
                 }
             }];
        }
        else
        {
            [CSS_Class alertviewController_title:@"" MessageAlert:LocalizedString(@"CONNECTION") viewController:self okPop:NO];
          
        }
}


//logout method


-(void)logOut
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"Are you sure want to logout?") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction actionWithTitle:LocalizedString(@"NO") style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self logoutMethod_FromApp];
        
    }];
    [alertController addAction:ok];
    [alertController addAction:no];
    [self presentViewController:alertController animated:YES completion:nil];
}
//Method for logout
-(void)logoutMethod_FromApp
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *idStr = [defaults valueForKey:@"id"];
        NSDictionary *param = @{@"id":idStr};
        
        NSString *url = [NSString stringWithFormat:@"/api/provider/logout"];
        [afn getDataFromPath_NoLoader:url withParamData:param withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                [self ViewOuterTap];
                [KeyConstants constants].RideStatus = @"";
                [KeyConstants constants].RideUpdateStatus = @"";
                [defaults setValue:[NSNumber numberWithBool:0] forKey:@"is_valid"];

                [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
				
				/*UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

				
                PageViewController * wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];

                [self.navigationController pushViewController:wallet animated:YES]; */
				
				WelcomeViewController *viewController = [WelcomeViewController initController];
				UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
				[self.navigationController presentViewController:navigationController animated:YES completion:^{
					NSLog(@"Presented welcome view controller");
				}];
            }
            else
            {
                if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                {
                    //Refresh token
                    [self TokenExpired];
                    
                }else{
                   [CSS_Class alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                
            }
        }];
    }
    else
    {
        [CSS_Class alertviewController_title:LocalizedString(@"Alert") MessageAlert:NSLocalizedString(@"CHKNET", nil) viewController:self okPop:NO];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)nopermissionLogoutBtnAction:(UIButton *)sender {
    [self logOut];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self backgroundPerformStopMethod];
}
- (IBAction)offlineOnlineBtnDidTab:(UISegmentedControl *)sender {
    NSString * status;
    
    //NSString * currentTitle = LocalizedString(sender.currentTitle);
    if(sender.selectedSegmentIndex == 1) {
        status = @"offline";
    }
	else {
         status = @"active";
    }
    MINT_METHOD_TRACE_START
    if([appDelegate internetConnected])
    {
       NSDictionary *params=@{@"service_status":status};
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn getDataFromPath:STATUS withParamData:params withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
         {
             if (response)
             {
                 if ([response objectForKey:@"error"])
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[response objectForKey:@"error"] viewController:self okPop:NO];
                 }
                 else
                 {
                     //Success
                     NSLog(@"service_status ...%@", response);
                     NSString *first_name = [response valueForKey:@"first_name"];
                     NSString *last_name = [response valueForKey:@"last_name"];
                     
                     NSString *avatar =[Utilities removeNullFromString:[response valueForKey:@"avatar"]];
                     NSString *status =[Utilities removeNullFromString:[response valueForKey:@"status"]];
                     
                     NSDictionary *dict = [response valueForKey:@"service"];
                     NSString *serviceStatus = [dict valueForKey:@"status"];
                     
                     if ([serviceStatus isEqualToString:@"active"]){
                         //[sender setTitle:LocalizedString(@"OFFLINE") forState:UIControlStateNormal];
						 sender.selectedSegmentIndex = 0;
                         [KeyConstants constants].RideStatus = @"ONLINE";
                         
                     }
                     else{
                       
                         //[sender setTitle:LocalizedString(@"ONLINE") forState:UIControlStateNormal];
						 sender.selectedSegmentIndex = 1;
                         [KeyConstants constants].RideStatus = @"OFFLINE";
                       
                     }
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setObject:first_name forKey:@"first_name"];
                     [defaults setObject:last_name forKey:@"last_name"];
                     [defaults setObject:avatar forKey:@"avatar"];
                     [defaults setObject:status forKey:@"status"];
                     [self checkViewUpdateStatusChange];
                 }
                 
             }
             else
             {
                 if ([strErrorCode intValue]==1)
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
                 else if ([strErrorCode intValue]==2)
                 {
                     if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                     {
                         //Refresh token
                         [self TokenExpired];
                         
                     }
                     else
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[error valueForKey:@"error"]  viewController:self okPop:NO];
                     }
                 }
                 else if ([strErrorCode intValue]==3)
                 {
                     if ([error objectForKey:@"email"]) {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                     }
                     else if ([error objectForKey:@"password"]) {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"password"] objectAtIndex:0]  viewController:self okPop:NO];
                     }
                     else
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG")  viewController:self okPop:NO];
                     }
                 }
                 else
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG")  viewController:self okPop:NO];
                 }
                 NSLog(@"%@",error);
                 
             }
         }];
    }
    else
    {
        [CSS_Class alertviewController_title:@"" MessageAlert:LocalizedString(@"CONNECTION") viewController:self okPop:NO];
        
    }
    MINT_NONARC_METHOD_TRACE_STOP
    
}

-(void)TokenExpired{
    [self ViewOuterTap];
    [KeyConstants constants].RideStatus = @"";
    [KeyConstants constants].RideUpdateStatus = @"";
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:0] forKey:@"is_valid"];
    
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
	
	/*
    PageViewController * wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];

    [self.navigationController pushViewController:wallet animated:YES]; */
	WelcomeViewController *viewController = [WelcomeViewController initController];
	UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
	[self.navigationController presentViewController:navigationController animated:YES completion:^{
		NSLog(@"Presented welcome view controller");
	}];
}
#pragma Colleection
#pragma CatergoryList update collectionview
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [arrayCollection count];
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    TrackCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TrackCell" forIndexPath:indexPath];
    
    if([arrayCollection[indexPath.item][@"status"] isEqualToString:[KeyConstants constants].RideStatus]){
    
        cell.trackImageview.image = [UIImage imageNamed:arrayCollection[indexPath.item][@"activeImage"]];
    }else{
        cell.trackImageview.image = [UIImage imageNamed:arrayCollection[indexPath.item][@"image"]];

    }
    
    
    return cell;
    
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
   
        return CGSizeMake(collectionView.frame.size.width/3.3, collectionView.frame.size.height);
}



//Ride Sttu update
-(void)statusUpdateMethod:(NSDictionary*)parameter
{
    
    if([appDelegate internetConnected]){
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *request_ID = [defaults valueForKey:@"request_id"];
    
    NSString *url = [NSString stringWithFormat: @"%@/api/provider/trip/%@", SERVICE_URL, request_ID];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:PATCH_METHOD];
    [afn getDataFromPath:url  withParamData:parameter withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
     {
         if (response)
         {
             NSLog(@"RESPONSE ...%@", response);
             [self getIncommingRequest];
         }
         else
         {
             if ([strErrorCode intValue]==1)
             {
                // [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
             }
             else if ([strErrorCode intValue]==2)
             {
                 if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                 {
                     //Refresh token
                 }
                 else
                 {
                     [self logOut];
                 }
             }
             else if ([strErrorCode intValue]==3)
             {
                 if ([error objectForKey:@"password"]) {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                 }
                 else
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
             }
             else
             {
                 [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
             }
             NSLog(@"%@",error);
             
         }
     }];
    }else{
         [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"CONNECTION") viewController:self okPop:NO];
    }
}



#pragma Navication Btn Actio
- (IBAction)NavicationBtnDidTab:(UIButton *)sender {
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=%@,%@&daddr=%@,%@&dirflg=r",[KeyConstants constants].LocationInfo[sourceLat], [KeyConstants constants].LocationInfo[sourceLang], [KeyConstants constants].DestinationInfo[DestinationLat], [KeyConstants constants].DestinationInfo[DestinationLang]]];
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://?"]])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        [CSS_Class alertviewController_title:LocalizedString(@"Alert!")  MessageAlert:LocalizedString(@"Please download google maps for navigation") viewController:self okPop:NO];
    }
}

#pragma Ride Call Btn action
- (IBAction)rideCallBtnDidTab:(UIButton *)sender {
    NSString * mobileString = [Utilities removeNullFromString:[KeyConstants constants].RequestInfo[@"request"][@"user"][@"mobile"]];
    if ([mobileString isEqualToString:@""])
    {
        [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:LocalizedString(@"User was not gave the mobile number") viewController:self okPop:NO];
    }
    else
    {
        NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:mobileString]];
        NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:mobileString]];
        
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl])
        {
            [UIApplication.sharedApplication openURL:phoneUrl options:@{} completionHandler:nil];
        }
        else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl])
        {
            [UIApplication.sharedApplication openURL:phoneFallbackUrl options:@{} completionHandler:nil];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CALL") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
    }}
    
}

#pragma Ride Status Btn Acton
- (IBAction)rideArraiveBtnAction:(UIButton *)sender {
    NSDictionary * params;
	NSLog(@"Ride arrived pressed");
    if([sender.titleLabel.text isEqualToString:@"ARRIVED"]){
        
         params = @{@"status":@"ARRIVED" ,@"_method":@"PATCH"};
		NSLog(@"Ride arrived - should update status method");
         [self statusUpdateMethod:params];
    } else if([sender.titleLabel.text isEqualToString:@"PICKED UP"]) {
            [KeyConstants constants].RideStatus = @"OTP";
            [KeyConstants constants].otpStatus =YES;
            [self checkViewUpdateStatusChange];
    }
}

#pragma Ride cancel Btn action
- (IBAction)rideCancelBtnAction:(UIButton *)sender {
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"Are you sure to want to cancel this ride.") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction actionWithTitle:LocalizedString(@"NO") style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if([appDelegate internetConnected])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *request_ID = [defaults valueForKey:@"request_id"];
            
            NSString *url = [NSString stringWithFormat: @"/api/provider/cancel"];
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            
            NSDictionary *param = @{@"id":request_ID};
            [afn getDataFromPath:url  withParamData:param withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
             {
                 if (response)
                 {
                     NSLog(@"CANCEL RESPONSE ...%@", response);
                     [_mapView clear];
                     [_markerList removeAllObjects];
                     // Nami added: ---
                     self.pathPoly = nil;
					 [self updateCarMarker];
                 }
                 else
                 {
                     if ([strErrorCode intValue]==2)
                     {
                         if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                         {
                             //Refresh token
                         }
                         else
                         {
                             [self logOut];
                         }
                     }
                     else {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                     NSLog(@"%@",error);
                     
                 }
             }];
        }
        else
        {
            [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"CONNECTION") viewController:self okPop:NO];
            
        }
        
    }];
    [alertController addAction:ok];
    [alertController addAction:no];
    [self presentViewController:alertController animated:YES completion:nil];
    
    

    MINT_NONARC_METHOD_TRACE_STOP
}
//Parked Status Btn Action
- (IBAction)ParkedStatusDidTab:(UIButton *)sender {
    
    NSDictionary * params;
	if([LocalizedString(sender.currentTitle) isEqualToString:@"DROPPED OFF"]){
		params = @{@"status":@"DROPPED" ,@"_method":@"PATCH"};
	}
	[self statusUpdateMethod:params];
}

//Pragma TextfieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if(_OtpFirstTxt == textField){
        if ([_OtpFirstTxt.text length] == 1) {
            [_OtpFirstTxt setText:newString];
            [_OtpSecondTxt becomeFirstResponder ];
        }
        
    }
    if(_OtpSecondTxt == textField){
        
        if ([_OtpSecondTxt.text length] == 1) {
            [_OtpSecondTxt setText:newString];
            [_OtpThirdTxt becomeFirstResponder];
        }
        
    }
    
    if(_OtpThirdTxt == textField){
        
        if ([_OtpThirdTxt.text length] == 1) {
            [_OtpThirdTxt setText:newString];
            [_OtpFourthTxt becomeFirstResponder];
        }
        
    }
    
    if(_OtpFourthTxt == textField){
        
        if ([_OtpFourthTxt.text length] == 1) {
            [_OtpFourthTxt setText:newString];
        }
        
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 1 && newLength == 2) ? NO : YES;
}


#pragma Textview Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Write your comments"]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Write your comments";
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}
#pragma Invoce paymanet btn actions
- (IBAction)invoiceConfirmBtnAction:(UIButton *)sender {
    NSDictionary * params;
    params = @{@"status":@"COMPLETED" ,@"_method":@"PATCH"};
    [self statusUpdateMethod:params];

}
- (IBAction)sosBtnDidtab:(UIButton *)sender {
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *sosNumber = [Utilities removeNullFromString:[def valueForKey:@"sos"]];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"Are you sure want to Call Emergency?") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction actionWithTitle:LocalizedString(@"NO") style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([sosNumber isEqualToString:@""])
        {
            //No SOS number was provided
        }
        else
        {
            NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:sosNumber]];
            NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:sosNumber]];
            
            if ([UIApplication.sharedApplication canOpenURL:phoneUrl])
            {
                [UIApplication.sharedApplication openURL:phoneUrl options:@{} completionHandler:nil];
            }
            else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl])
            {
                [UIApplication.sharedApplication openURL:phoneFallbackUrl options:@{} completionHandler:nil];
            }
            else
            {
                [CSS_Class alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"Your device does not support calling") viewController:self okPop:NO];
               
            }
        }
        
    }];
    [alertController addAction:ok];
    [alertController addAction:no];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
- (IBAction)RateSubmitBtnDidTab:(UIButton *)sender {
    
    MINT_METHOD_TRACE_START
    if([appDelegate internetConnected])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *request_ID = [defaults valueForKey:@"request_id"];
        
        NSInteger num=(NSUInteger)(floor(_notifyUserRateView.value));
        NSString*rat=[NSString stringWithFormat:@"%ld",num];
        
       NSDictionary *params = @{@"rating":rat,@"comment":_rateCommentsview.text};
        
        NSString *url = [NSString stringWithFormat: @"/api/provider/trip/%@/rate", request_ID];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:url  withParamData:params withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
         {
             if (response){
                 [self getIncommingRequest];
                 [KeyConstants constants].DestinationInfo = nil;
                 [self.mapView clear];
                 [_markerList removeAllObjects];
                 // Nami added: ---
                 self.pathPoly = nil;
				 [self updateCarMarker];
                 [KeyConstants constants].RideStatus = @"ONLINE";
                 [self GetCurrentLocationUsingPlaces];
             }else
             {
                 if ([strErrorCode intValue]==1)
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
                 else if ([strErrorCode intValue]==2)
                 {
                     if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                     {
                         //Refresh token
                     }
                     else
                     {
                         [self logOut];
                     }
                 }
                 else if ([strErrorCode intValue]==3)
                 {
                     if ([error objectForKey:@"password"]) {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                     }
                     else
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                 }
                 else
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
                 NSLog(@"%@",error);
                 
             }
         }];
    }
    else
    {
        [CSS_Class alertviewController_title:@"" MessageAlert:LocalizedString(@"CONNECTION") viewController:self okPop:YES];
        
    }
    
    MINT_NONARC_METHOD_TRACE_STOP
    
}
-(void)pinDidEnterAllSymbol:(NSArray *)symbolArray string:(NSString *)pin{
    NSLog(@"%@",pin);
    otpSyting = pin;
    
}

- (IBAction)OtpSubmitBtnDidTab:(UIButton *)sender {
    if(otpSyting.length == 4){
        if([[KeyConstants constants].RequestInfo[@"request"][@"otp"] isEqualToString:otpSyting]){
        
            NSDictionary * params = @{@"status":@"PICKEDUP" ,@"_method":@"PATCH"};
            
            [self statusUpdateMethod:params];
            [KeyConstants constants].otpStatus =NO;
        }else{
            [CSS_Class alertviewController_title:@"" MessageAlert:@"Please enter valid OTP" viewController:self okPop:NO];
        }
        
    }else{
        [CSS_Class alertviewController_title:@"" MessageAlert:@"Please enter your OTP" viewController:self okPop:NO];
    }
    
}
@end
