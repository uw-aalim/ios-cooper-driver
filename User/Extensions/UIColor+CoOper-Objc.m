//
//  UIColor+CoOper-Objc.m
//  User
//
//  Created by Benjamin Cortens on 2018-10-26.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import "UIColor+CoOper-Objc.h"

@implementation UIColor (cooper)

+(UIColor *) tealColourObjC {
	return [UIColor colorWithRed:(59.0/255.0) green:(179.0/255.0) blue:(194.0/255.0) alpha:1.0];
}

+(UIColor *) orangeColourObjC {
	return [UIColor colorWithRed:(231.0/255.0) green:(120.0/255.0) blue:(23.0/255.0) alpha:1.0];
}

@end
