//
//  UIColor+CoOper.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-07.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension UIColor {
	class func orangeColour() -> UIColor {
		return UIColor(red: (231.0/255.0), green: (120.0/255.0), blue: (23.0/255.0), alpha: 1.0)
	}
	
	class func tealColour() -> UIColor {
		return UIColor(red: (59.0/255.0), green: (179.0/255.0), blue: (194.0/255.0), alpha: 1.0)
	}
}
