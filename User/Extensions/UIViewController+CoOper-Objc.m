//
//  UIViewController+CoOper-Objc.m
//  Provider
//
//  Created by Benjamin Cortens on 2018-11-14.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import "UIViewController+CoOper-Objc.h"


@implementation UIViewController (cooper)

-(void)showAlertviewControllerWithTitle: (NSString*) title
					messageAlert:(NSString*) msg {
	
	UIAlertController * alert = [UIAlertController
								 alertControllerWithTitle:title
								 message:msg
								 preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* yesButton = [UIAlertAction
								actionWithTitle:LocalizedString(@"OK")
								style:UIAlertActionStyleDefault
								handler:^(UIAlertAction * action) {
									
								}];
	
	[alert addAction:yesButton];
	//    [alert addAction:noButton];
	//[self present]
	[self presentViewController:alert animated:YES completion:nil];
}

@end
