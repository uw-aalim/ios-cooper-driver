//
//  UIViewController+CoOper-Objc.h
//  Provider
//
//  Created by Benjamin Cortens on 2018-11-14.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//
#import <UIKit/UIKit.h>

#ifndef UIViewController_CoOper_Objc_h
#define UIViewController_CoOper_Objc_h

@interface UIViewController (cooper)
-(void)showAlertviewControllerWithTitle: (NSString*) title
						   messageAlert:(NSString*) msg ;
@end

#endif /* UIViewController_CoOper_Objc_h */
