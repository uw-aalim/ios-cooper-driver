//
//  KeyConstants.m
//  User
//
//  Created by CSS on 18/01/18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import "KeyConstants.h"

@implementation KeyConstants
NSString *const SourceLocations = @"SourceLocations";
NSString *const DestinationLocations = @"DestinationLocations";
NSString *const currentLocations=@"currentLocations";
NSString *const sourceLat = @"sourceLat";
NSString *const sourceLang=@"sourceLang";
NSString *const DestinationLat = @"DestinationLat";
NSString *const  DestinationLang=@"DestinationLang";

NSString *const vehicleName = @"vehicleName";
NSString *const paymentMode= @"paymentMode";
NSString *const cardNumber=@"cardNumber";
NSString *const fareType=@"fareType";
NSString *const vehicleId=@"vehicleId";

BOOL  const IsShedule = NO;

+(KeyConstants *)constants
{
    static KeyConstants *constants=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        constants=[[KeyConstants alloc]init];
    });
    return  constants;
}
+(BOOL)CheckKeyExcistswithDict :(NSDictionary *)dict withKey:(NSString *)key{
    
    if([[dict allKeys] containsObject:key]){
        return true;
    }
    return false;
}

@end
