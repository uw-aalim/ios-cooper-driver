//
//  LeftMenuView.m
//  caretaker_user
//
//  Created by apple on 12/15/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "LeftMenuView.h"
#import "config.h"
#import "CSS_Class.h"
#import "Colors.h"
#import "ProfileViewController.h"
#import "Utilities.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import <SDWebImage/UIButton+WebCache.h>


@implementation LeftMenuView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self updateNameAndImage];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    //[self setDesign];
	[self updateNameAndImage];
}

-(void) updateNameAndImage {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	self.nameLabel.text =[NSString stringWithFormat:@"%@ %@",[defaults valueForKey:@"first_name"],[defaults valueForKey:@"last_name"]];
	NSString *strProfile = [defaults valueForKey:@"avatar"];
	
	NSString *statusStr = [defaults valueForKey:@"status"];
	
	if ([statusStr isEqualToString:@"onboarding"]) {
		[self.statusImageView setImage:[UIImage imageNamed:@"yellow"]];
	}
	else if ([statusStr isEqualToString:@"approved"]) {
		[self.statusImageView setImage:[UIImage imageNamed:@"green"]];
	}
	else {
		[self.statusImageView setImage:[UIImage imageNamed:@"red"]];
	}
	
	if (![strProfile isKindOfClass:[NSNull class]]) {
		NSURL *imgUrl;
		if ([strProfile length]!=0) {
			if ([strProfile containsString:@"http"]) {
				imgUrl = [NSURL URLWithString:strProfile];
				
				[self.imageView sd_setBackgroundImageWithURL:imgUrl
																forState:UIControlStateNormal
														placeholderImage:[UIImage imageNamed:@"menu_profile_placeholder"]];
			}
			else {
				imgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/storage/%@", SERVICE_URL, strProfile]];
				
				[self.imageView sd_setBackgroundImageWithURL:imgUrl
																forState:UIControlStateNormal
														placeholderImage:[UIImage imageNamed:@"menu_profile_placeholder"]];
			}
		}
		else {
			self.imageView.imageView.image = [UIImage imageNamed:@"menu_profile_placeholder"];
		}
	}
}


- (IBAction)profilePressed:(id)sender {
    [self.LeftMenuViewDelegate profileView];
}

- (IBAction)yourTripsPressed:(id)sender {
	[self.LeftMenuViewDelegate yourTripsView];
}

- (IBAction)earningsPressed:(id)sender {
	[self.LeftMenuViewDelegate earningsView];
}

- (IBAction)summaryPressed:(id)sender {
	[self.LeftMenuViewDelegate summaryView];
}

- (IBAction)helpPressed:(id)sender {
	[self.LeftMenuViewDelegate helpView];
}

- (IBAction)sharePressed:(id)sender {
	[self.LeftMenuViewDelegate shareView];
}

- (IBAction)settingsPressed:(id)sender {
	[self.LeftMenuViewDelegate Settings];
}

- (IBAction)closePressed:(id)sender {
	[self.LeftMenuViewDelegate closeMenuPressed];
}

- (IBAction)logoutPressed:(id)sender {
	[self.LeftMenuViewDelegate logOutMenu];
}

- (IBAction)yourDocumentsPressed:(id)sender {
    [self.LeftMenuViewDelegate yourDocuments];
}



//	------------------------------------------------------------------------------------------------------------
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
	if (scrollView.contentOffset.y > 0) {
		CGFloat newSize = 72.0 - self.scrollview.contentOffset.y;
		if (newSize < 32.0) {
			newSize = 32.0;
		}
		self.profileImageWidth.constant = newSize;
	}
	else {
		self.profileImageWidth.constant = 72 + (-scrollView.contentOffset.y);
	}
	self.imageView.layer.cornerRadius = (self.profileImageWidth.constant)/2.0;
}
//	------------------------------------------------------------------------------------------------------------

@end
