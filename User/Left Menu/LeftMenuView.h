//
//  LeftMenuView.h
//  caretaker_user
//
//  Created by apple on 12/15/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LeftMenuViewprotocol;

@interface LeftMenuView : UIView<UIScrollViewDelegate>
{
}

@property (nonatomic,strong) IBOutlet NSLayoutConstraint * profileImageWidth;
@property (nonatomic,strong) IBOutlet UIButton * imageView;
@property (nonatomic,strong) IBOutlet UIImageView * statusImageView;
@property (nonatomic,strong) IBOutlet UIScrollView * scrollview;
@property (nonatomic,strong) IBOutlet UILabel * nameLabel;
@property (nonatomic,weak) id <LeftMenuViewprotocol> LeftMenuViewDelegate;

-(void) updateNameAndImage;

@end



@protocol LeftMenuViewprotocol <NSObject>
//-(void)setSelectedIndexforMenu:(NSString *)title;

-(void)profileView;
-(void)yourTripsView;
-(void)yourDocuments;
-(void)earningsView;
-(void)summaryView;
-(void)helpView;
-(void)shareView;
-(void)Settings;
-(void)logOutMenu;
-(void)closeMenuPressed;

@end
