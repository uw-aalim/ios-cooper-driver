//
//  Header.h
//  TruckLogics
//
//  Created by STS-Manoj on 9/8/15.
//  Copyright (c) 2015 SPAN Technology Services. All rights reserved.
//

#ifndef XUBER_Colors_h
#define XUBER_Colors_h

#define TEXTCOLOR_LIGHT RGB(82, 87, 96)
#define TEXTCOLOR RGB(18, 18, 18)
#define BLACKCOLOR RGB(18, 18, 18)
#define BLUECOLOR_TEXT RGB(52, 118, 179)


#define BASE1Color RGB(147,116,209)
#define BASE2Color RGB(244,101,149)
#endif
