//
//  PageViewController.swift
//  PageControl
//
//  Created by Andrew Seeley on 2/2/17.
//  Copyright © 2017 Seemu. All rights reserved.
//

import UIKit


class PageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    
    var pageControl = UIPageControl()
    var loginBtn = UIButton()
    var SignUp = UIButton()

    var SocialBtn = UIButton()

    
    var timer = Timer()
    var bottomView = UIView()
    var carAnimview = UIImageView()

    // MARK: UIPageViewControllerDataSource
    
    lazy var orderedViewControllers: [UIViewController] = {
        return [self.newVc(viewController: "one"),
                self.newVc(viewController: "two"),self.newVc(viewController: "three")]
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        

        //Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.nextAction), userInfo: nil, repeats: true)
        
        
       // NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification), name: Notification.Name("MyNotification"), object: nil)
        (constantsWalk.sharedManager() as! constantsWalk).isRight = true

        // This sets up the first view that will show up on our page control
        // Do any additional setup after loading the view.
    }
    
//    func methodOfReceivedNotification(notification: Notification){
//            print(notification.object!)
//            if((notification.object as AnyObject)["status"] as? Bool)!{
//                self.nextBtn.isHidden = true
//            }else{
//                
//                self.nextBtn.isHidden = false
//
//            }
//        }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)

        }
        self.configurePageControl()
        CommonAnimations.animation(forMoveview: carAnimview, withAnimationDirations: 10.0)


    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)

    }
    //MARK: configure page UI controll
    func configurePageControl() {
        
        
        bottomView = UIView(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - 160,width: UIScreen.main.bounds.width,height: 160))
        bottomView.backgroundColor = UIColor.white
        bottomView.layer.shadowOffset = CGSize(width: 0,height :0)
        bottomView.layer.shadowOpacity = 0.5
        self.view.addSubview(bottomView)
        let BgView = UIView(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 32))
        BgView.backgroundColor = UIColor.white
        self.bottomView.addSubview(BgView)
        
        carAnimview = UIImageView(frame: CGRect(x: 0,y: 15,width:BgView.frame.size.width,height: 32))
        carAnimview.image = UIImage(named:"caranimation.gif")
        carAnimview.backgroundColor = UIColor.clear
        carAnimview.contentMode = UIViewContentMode.scaleAspectFit
        
        BgView.addSubview(carAnimview)
        
        
        // The total number of pages that are available is based on how many available colors we have.
        pageControl = UIPageControl(frame: CGRect(x: 0,y: carAnimview.frame.origin.y+carAnimview.frame.size.height-20,width: UIScreen.main.bounds.width,height: 50))
        self.pageControl.numberOfPages = orderedViewControllers.count
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.white
        self.pageControl.pageIndicatorTintColor = UIColor.gray
        self.pageControl.currentPageIndicatorTintColor = UIColor.black
        self.bottomView.addSubview(pageControl)
        
        loginBtn = UIButton(frame : CGRect(x: 30,y:pageControl.frame.origin.y+pageControl.frame.size.height-10,width: (UIScreen.main.bounds.size.width/2)-40,height: 40))
        loginBtn.layer.cornerRadius = 2.0
        loginBtn.layer.masksToBounds = true
        loginBtn.setTitleColor(UIColor.white, for: .normal)
        loginBtn.setTitle("SIGN IN", for: .normal)
        loginBtn.addTarget(self, action: #selector(LoginBtnAction(_:)), for: .touchUpInside)
        loginBtn.backgroundColor = UIColor(red:0.96, green:0.40, blue:0.58, alpha:1.0)
        self.bottomView.addSubview(loginBtn)
        
        
        SignUp = UIButton(frame : CGRect(x: loginBtn.frame.origin.x+loginBtn.frame.size.width+20,y:loginBtn.frame.origin.y,width: loginBtn.frame.size.width,height: 40))
        SignUp.layer.cornerRadius = 2.0
        SignUp.layer.masksToBounds = true
        SignUp.setTitleColor(UIColor.white, for: .normal)
        SignUp.setTitle("SIGN UP", for: .normal)
        SignUp.addTarget(self, action: #selector(signUpAction(_:)), for: .touchUpInside)
        SignUp.backgroundColor = UIColor(red:0.58, green:0.45, blue:0.82, alpha:1.0)
        self.bottomView.addSubview(SignUp)
        
        
        
        SocialBtn = UIButton(frame : CGRect(x: 30,y:loginBtn.frame.origin.y+loginBtn.frame.size.height+2,width: self.view.frame.size.width-60,height: 30))
        SocialBtn.layer.cornerRadius = 2.0
        
        SocialBtn.layer.masksToBounds = true
        SocialBtn.setTitleColor(UIColor(red:0.58, green:0.45, blue:0.82, alpha:1.0), for: .normal)
        SocialBtn.setTitle("Or connect with social", for: .normal)
        SocialBtn.titleLabel?.textAlignment = .left
        SocialBtn.addTarget(self, action: #selector(socialBtnAction(_:)), for: .touchUpInside)
        //SocialBtn.backgroundColor = UIColor(red:0.58, green:0.45, blue:0.82, alpha:1.0)
        self.bottomView.addSubview(SocialBtn)


    }
    
    @objc func LoginBtnAction(_ sender:UIButton){
        
        let controller = storyboard?.instantiateViewController(withIdentifier: "EmailViewController") as? EmailViewController
        navigationController?.pushViewController(controller ?? UIViewController(), animated: true)

    }
    @objc func signUpAction(_ sender:UIButton){

        let controller = storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController
        navigationController?.pushViewController(controller ?? UIViewController(), animated: true)
        
    }
    
    @objc func socialBtnAction(_ sender:UIButton){
        
        let controller = storyboard?.instantiateViewController(withIdentifier: "SocailMediaViewController") as? SocailMediaViewController
        navigationController?.pushViewController(controller ?? UIViewController(), animated: true)
        
    }
    func nextAction(){
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let nextViewController = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) else { return }
        setViewControllers([nextViewController], direction: .forward, animated: true, completion: nil)
       // self.pageControl.currentPage = orderedViewControllers.index(of: currentViewController)!+1
    }
    
    
    
    func newVc(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: viewController)
        
    }
    
    
    // MARK: Delegate methords
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = orderedViewControllers.index(of: pageContentViewController)!
    }
    
    
    // MARK: Data source functions.
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1

        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
           // return orderedViewControllers.last
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
             return nil
        }
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        (constantsWalk.sharedManager() as! constantsWalk).isRight = false
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
//        guard orderedViewControllersCount != nextIndex else {
//            return orderedViewControllers.first
//            // Uncommment the line below, remove the line above if you don't want the page control to loop.
//            // return nil
//        }
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        (constantsWalk.sharedManager() as! constantsWalk).isRight = true

        return orderedViewControllers[nextIndex]
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("MyNotification"), object: nil)
    }
    

}
