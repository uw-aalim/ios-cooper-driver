//
//  LoginRegisterService.m
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import "LoginRegisterService.h"

#import "AppDelegate.h"
#import "AFNHelper.h"
#import "Constant.h"
#import "AFNetworking.h"
#import "Colors.h"
#import "CoOperNotificationStrings.h"
#import "UIColor+CoOper-Objc.h"
#import "Utilities.h"


@interface LoginRegisterService () {
	
	//	MARK:- Properties
	AppDelegate *appDelegate;
	
	//	The type of login currently being performed - may be email-password, or may be social
	NSString *strLoginType;
	//	The access tokens and phone number logins for google and facebook
	NSString *googleAccessToken, *phoneNumberStr;
	NSString *fbAccessToken, *loginByStr;
}

@property (nonatomic) NSString * email;
@property (nonatomic) NSString * password;
@property (nonatomic) NSString * firstName;
@property (nonatomic) NSString * lastName;
@property (nonatomic) NSString * phoneNumber;
@property (nonatomic, weak) UIViewController * viewController;

@end






@implementation LoginRegisterService {
	AKFAccountKit *_accountKit;
	UIViewController<AKFViewController> *_pendingLoginViewController;
	NSString *_authorizationCode;
}






//	============================================================================================================
//	MARK:- Initialization and Setup
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
+ (instancetype)sharedInstance {
	
	static LoginRegisterService *sharedInstance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedInstance = [[LoginRegisterService alloc] init];
		// Do any other initialisation stuff here
	});
	
	[sharedInstance performSetup];
	
	return sharedInstance;
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
-(void)performSetup {
	if (_accountKit == nil) {
		// may also specify AKFResponseTypeAccessToken
		_accountKit = [[AKFAccountKit alloc] initWithResponseType:AKFResponseTypeAccessToken];
	}
	if (_pendingLoginViewController == nil) {
		// view controller for resuming login
		_pendingLoginViewController = [_accountKit viewControllerForLoginResume];
	}
	
	
	strLoginType=@"manual";
	appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	//appDelegate.isLocationUpdate = YES;
	
	[GIDSignIn sharedInstance].uiDelegate = self;
	[GIDSignIn sharedInstance].delegate = self;
}
//	------------------------------------------------------------------------------------------------------------






//	============================================================================================================
//	MARK:- Registration Entry Points
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
/**
 Validate Email Address by calling into the server and checking to determine if the email is available or taken.
 @param email - the email address to be validated, must not be null
 */
-(void)validateEmailAddress: (NSString *) email {
	
	NSLog(@"LoginRegisterService Validate email address");
	//	Call into perform setup to make sure that the view is going to be correctly configured
	[self performSetup];
	
	//	Check to make sure the internet is connected and then once you have determined that it is validate the
	//	email address to ensure that it is not already used in the database.
	if([appDelegate internetConnected]) {
		NSDictionary * params=@{@"email":email};
		
		NSLog(@"LoginRegisterService - validateEmailAddress");
		//[AFNHelper  onStartLoader];
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
		[afn getDataFromPath:@"/api/provider/verify" withParamData:params withBlock:^(id response, NSDictionary *Error,NSString *strCode) {
			//[appDelegate onEndLoader];
			if(response) {
				[[NSNotificationCenter defaultCenter]
				 postNotificationName:RegisterAccountEmailValidationSuccess
				 object:self];
			}
			else {
				if ([strCode intValue]==1) {
					[self failedLoginWithErrorMessage:LocalizedString(@"ERRORMSG")];
				}
				else {
					if ([Error objectForKey:@"email"]) {
						[self failedLoginWithErrorMessage:[[Error objectForKey:@"email"] objectAtIndex:0]];
					}
					else if ([Error objectForKey:@"password"]) {
						[self failedLoginWithErrorMessage:[[Error objectForKey:@"password"] objectAtIndex:0]];
					}
				}
				
			}
		}];
	}
	else {
		[self failedLoginWithErrorMessage:LocalizedString(@"CHKNET")];
	}
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
/**
 Register for a new account with a previously validated email address and a known good password.
 @param viewController The view controller which is handling the registration process, this is
 important as the registration process uses an email authentication 
 */
-(void)registerAccountWithEmail: (NSString *) email
				   withPassword: (NSString *) password
				  withFirstName: (NSString *) firstName
				   withLastName: (NSString *) lastName
			   inViewController: (UIViewController *) viewController {
	
	//	Call into perform setup to make sure that the view is going to be correctly configured
	[self performSetup];
	
	//	The type of login being performed
	self->loginByStr = @"";
	
	//	Configure the login service for logging in
	self.email = email;
	self.password = password;
	self.firstName = firstName;
	self.lastName = lastName;
	self.viewController = viewController;
	
	[self authenticateRegistrationWithPhoneNumber:self];
}
//	------------------------------------------------------------------------------------------------------------






//	============================================================================================================
//	MARK:- Login Entry Point
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void) loginAccountWithEmail: (NSString *) email
				 withPassword: (NSString *) password {
	
	//	Call into perform setup to make sure that the view is going to be correctly configured
	[self performSetup];
	
	//	The type of login being performed
	self->loginByStr = @"";
	
	//	Configure the login service for logging in
	self.email = email;
	self.password = password;
	
	[self performLogin];
}
//	------------------------------------------------------------------------------------------------------------





//	============================================================================================================
//	MARK:- Phone Number Account Authentication
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
/**
 Only call this function after the email address has been validated with the server. Calling afterwards can cause
 a failure in the registration process.
 */
- (void)authenticateRegistrationWithPhoneNumber:(id)sender {
	
	NSLog(@"LoginRegisterService authenticateRegistrationWithPhoneNumber");
	[[NSUserDefaults standardUserDefaults]setObject:@"User" forKey:@"SocialLogin"];
	NSString *inputState = [[NSUUID UUID] UUIDString];
	UIViewController<AKFViewController> *autheticateRegistrationViewController = [_accountKit viewControllerForPhoneLoginWithPhoneNumber:nil state:inputState];
	// defaults to NO, need to allow YES for users without a phone number
	autheticateRegistrationViewController.enableSendToFacebook = YES;
	[self stylizeAuthenticateRegistrationViewController:autheticateRegistrationViewController];
	[self.viewController presentViewController:autheticateRegistrationViewController animated:YES completion:NULL];
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
/**
 Configure the appearance of the AKFViewController for getting an authentication code from the users's phone or
 facebook account.
 */
- (void)stylizeAuthenticateRegistrationViewController:(UIViewController<AKFViewController> *)authenticateViewController {
	authenticateViewController.delegate = self;
	// Optionally, you may use the Advanced UI Manager or set a theme to customize the UI.
	authenticateViewController.uiManager = [[AKFSkinManager alloc]
									 initWithSkinType:AKFSkinTypeClassic
									 primaryColor:UIColor.blackColor
									 backgroundImage:[UIImage imageNamed:@"register_background"]
									 backgroundTint:AKFBackgroundTintWhite
									 tintIntensity:0.32];
	
	authenticateViewController.uiManager.theme.inputBackgroundColor = [UIColor clearColor];
	authenticateViewController.uiManager.theme.inputTextColor = [UIColor blackColor];
	authenticateViewController.uiManager.theme.inputBorderColor = [UIColor orangeColourObjC];
	authenticateViewController.uiManager.theme.buttonBackgroundColor = [UIColor orangeColourObjC];
	authenticateViewController.uiManager.theme.buttonTextColor = [UIColor whiteColor];
	authenticateViewController.uiManager.theme.buttonBorderColor = [UIColor clearColor];
	//    loginViewController.uiManager.theme.backgroundColor = [UIColor whiteColor];
}
//	------------------------------------------------------------------------------------------------------------





//	============================================================================================================
//	MARK:- Facebook Login
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void)loginWithFacebookInViewController: (UIViewController *) viewController {
	
	self.viewController = viewController;
	[self performSetup];
	
	if ([appDelegate internetConnected]) {
		/*********  logout the current session ************/
		FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
		[login logOut];
		[FBSDKAccessToken setCurrentAccessToken:nil];
		[FBSDKProfile setCurrentProfile:nil];
		/*********  logout the current session ************/
		
		/*********  start the new session for login ************/
		
		// FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
		login.loginBehavior = FBSDKLoginBehaviorWeb;
		[login logInWithReadPermissions:@[@"email"] fromViewController:self.viewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
			if (error) {
				// Process error
			}
			else if (result.isCancelled) {
				// Handle cancellations
			}
			else {
				if ([result.grantedPermissions containsObject:@"email"]) {
					if ([FBSDKAccessToken currentAccessToken]) {
						[[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name, last_name, picture.type(normal), accounts{username},email, gender, locale, timezone, about"}]
						 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
							 if (!error) {
								 NSLog(@"fetched user:%@", result);
								 self->fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
								 NSLog(@"fbAccessToken=>%@", self->fbAccessToken);
								 
								 NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
								 [user setValue:self->fbAccessToken forKey:@"FB_ACCESSTOKEN"];
								 self->loginByStr = @"FB";
								 [self authenticateRegistrationWithPhoneNumber:self];
								 
							 }
						 }];
					}
				}
			}
		}];
		NSLog( @"### running FB sdk version: %@", [FBSDKSettings sdkVersion] );
	}
	else {
		[self failedLoginWithErrorMessage:LocalizedString(@"CHKNET")];
	}
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
-(void) finishFacebookLogin {
	
	NSLog(@"LoginRegisterService - finishFacebokLogin");
	if ([appDelegate internetConnected]) {
		//[appDelegate onStartLoader];
		
		NSString* UDID_Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
		
		NSDictionary *params=@{
							   @"accessToken":fbAccessToken,
							   @"device_token":appDelegate.device_tokenStr,
							   @"device_id":UDID_Identifier ,
							   @"device_type":@"ios",
							   @"login_by":@"facebook",
							   @"mobile":self.phoneNumber};
		
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
		
		[afn getDataFromPath:MD_FACEBOOK withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
			//[appDelegate onEndLoader];
			
			NSLog(@"FB CHECK response:%@", response);
			
			NSLog(@"FB CHECK ERROR:%@", error);
			
			//            NSString *statusError = [error[@"status"]stringValue];
			
			NSString *statusResponse = [response[@"status"]stringValue];
			
			if ([statusResponse isEqualToString:@"1"]) {
				NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
				[user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
				[user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
				[user setValue:response[@"currency"] forKey:@"currency"];
				[user setBool:true forKey:@"isLoggedin"];
				[self getProfileAfterLogin];
			}
			else if ([statusResponse isEqualToString:@"0"]) {
				/*[CSS_Class alertviewController_title:@"" MessageAlert:error[@"message"] viewController:self.viewController okPop:NO]; */
				[self failedLoginWithErrorMessage:@"Unknown error when finishing facebook login."];
			}
			else {
				[self failedLoginWithErrorMessage:@"Unknown error when finishing facebook login."];
			}
		}];
	}
	else {
		[self failedLoginWithErrorMessage:LocalizedString(@"CHKNET")];
	}
}
//	------------------------------------------------------------------------------------------------------------






//	============================================================================================================
//	MARK:- Google Login
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void) loginGoogleInViewController: (UIViewController *) viewController {
	self.viewController = viewController;
	[self performSetup];
	[[GIDSignIn sharedInstance] signIn];
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void) finishGmailLogin {
	
	NSLog(@"LoginRegisterService - finishGmailLogin");
	//[appDelegate onStartLoader];
	
	
	NSLog(@"Attempt to get UDID");
	NSString* UDID_Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
	
	NSLog(@"Attempt to get Google access token %@",googleAccessToken);
	NSLog(@"Attempt to get str device token %@",appDelegate.device_tokenStr);
	NSLog(@"Attempt to get UDID %@",UDID_Identifier);
	NSLog(@"Attempt to get phone number %@",self.phoneNumber);
	
	if([appDelegate internetConnected]) {
	
		NSDictionary *params=@{@"accessToken":googleAccessToken,
						   @"device_token":appDelegate.device_tokenStr,
						   @"device_id":UDID_Identifier ,
						   @"device_type":@"ios",
						   @"login_by":@"google",
						   @"mobile":self.phoneNumber};
	
		NSLog(@"Attempt to set params");
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
		
		
		[afn getDataFromPath:MD_GOOGLE withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
			if (response) {
				NSLog(@"RESPONSE ...%@", response);
				NSString *statusResponse = [response[@"status"]stringValue];
				if ([statusResponse isEqualToString:@"0"]){
					[self failedLoginWithErrorMessage:@"Empty error message received"];
				}
				else if ([statusResponse isEqualToString:@"1"]){
					NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
					[user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
					[user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
					[user setValue:response[@"currency"] forKey:@"currency"];
					[user setBool:true forKey:@"isLoggedin"];
					[self getProfileAfterLogin];
				}
				else {
					[self failedLoginWithErrorMessage:@"Empty error message received"];
				}
			}
			else {
				[self failedLoginWithErrorMessage:@"Empty error message received"];
			}
		}];
	}
	else {
		[self failedLoginWithErrorMessage:LocalizedString(@"CHKNET")];
	}
	 
	 /*
	[afn getDataFromPath:MD_GOOGLE WithType:POST_METHOD WithParameters:params WithCompletedSuccess:^(id response) {
		
		//[appDelegate onEndLoader];
		
		
	} OrValidationFailure:^(NSString *errorMessage) {
		//[appDelegate onEndLoader];
		[self failedLoginWithErrorMessage:errorMessage];
		//[CSS_Class alertviewController_title:@"" MessageAlert:errorMessage viewController:self okPop:NO];
		
	} OrErrorCode:^(NSString *error) {
		//[appDelegate onEndLoader];
		[self failedLoginWithErrorMessage:error];
		//[CSS_Class alertviewController_title:@"" MessageAlert:error viewController:self okPop:NO];
		
	} OrIntentet:^(NSString *internetFailure) {
		//[appDelegate onEndLoader];
		[self failedLoginWithErrorMessage:LocalizedString(@"CHKNET")];
		//[CSS_Class alertviewController_title:@"" MessageAlert:internetFailure viewController:self okPop:NO];
		
	}]; */
}
//	------------------------------------------------------------------------------------------------------------






//	============================================================================================================
//	MARK:- Login and Get Profile
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
/**
 After the login parameters of username and password have been set (either from registration or login)
 this will then be called to actually perform the login and get the user profile.
 
 Note that this will be called after a successful registration after the validation of the phone
 number so-as to ensure that the registration has occurred before an attempted username-password login.
 */
-(void)performLogin {
	
	if([appDelegate internetConnected]) {
		NSDictionary * params=@{@"email":self.email,
								@"password":self.password,
								@"device_token":appDelegate.device_tokenStr,
								@"device_id":appDelegate.device_UDID ,
								//@"grant_type":@"password",
								@"device_type":@"ios" };
								//@"client_id":ClientID,
								//@"client_secret":Client_SECRET};
		
		//[appDelegate onStartLoader];
		
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
		[afn getDataFromPath:LOGIN withParamData:params withBlock:^(id response, NSDictionary *Error,NSString *strCode) {
			//[appDelegate onEndLoader];
				   
			if(response) {
				NSLog(@"RESPONSE ...%@", response);
				NSString *access_token = [response valueForKey:@"access_token"];
				NSString *first_name = [response valueForKey:@"first_name"];
				NSString *avatar =[Utilities removeNullFromString:[response valueForKey:@"avatar"]];
				NSString *status =[Utilities removeNullFromString:[response valueForKey:@"status"]];
				if([status isEqualToString:@"active"]){
					[KeyConstants constants].RideStatus = @"ONLINE";
				}else{
					[KeyConstants constants].RideStatus = @"NOPERMISSION";
				}
				NSString *currencyStr=[response valueForKey:@"currency"];
				NSString *socialId=[Utilities removeNullFromString:[response valueForKey:@"social_unique_id"]];
				NSString *last_name = [response valueForKey:@"last_name"];
				NSString *nameStr = [NSString stringWithFormat:@"%@ %@", [Utilities removeNullFromString: response[@"first_name"]], [Utilities removeNullFromString: response[@"last_name"]]];
				
				NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
				[defaults setObject:last_name forKey:@"last_name"];
				[defaults setObject:access_token forKey:@"access_token"];
				[defaults setObject:first_name forKey:@"first_name"];
				[defaults setObject:avatar forKey:@"avatar"];
				
				[defaults setObject:status forKey:@"status"];
				[defaults setValue:currencyStr forKey:@"currency"];
				[defaults setValue:socialId forKey:@"social_unique_id"];
				[defaults setObject:[response valueForKey:@"id"] forKey:@"id"];
				[defaults setObject:[response valueForKey:@"sos"] forKey:@"sos"];
				[[NSUserDefaults standardUserDefaults]setObject:@"User" forKey:@"SocialLogin"];
				
				// MARK: Set User Infomation locally
				[defaults setObject:SERVICE_URL forKey:@"serviceurl"];
				[defaults setObject:Client_SECRET forKey:@"passport"];
				[defaults setObject:ClientID forKey:@"clientid"];
				[defaults setObject:WEB_SOCKET forKey:@"websocket"];
				[defaults setObject:APP_NAME forKey:@"appname"];
				[defaults setObject:nameStr forKey:@"username"];
				//            [user setObject:APP_IMG_URL forKey:@"appimg"];
				//                    [defaults setObject:GMSMAP_KEY forKey:@"googleApiKey"];
				//                    [defaults setObject:Stripe_KEY forKey:@"stripekey"];
				
				[defaults setObject:@"1" forKey:@"is_valid"];
				[defaults synchronize];
				//[self getProfileAfterLogin];
				[self successfulLogin];
				
			}	//	END OF IF - found a login response
			else {
				if ([strCode intValue]==1) {
					[self failedLoginWithErrorMessage:LocalizedString(@"ERRORMSG")];
				}
				else {
					if ([Error objectForKey:@"email"]) {
						//	Get the email address error
						[self failedLoginWithErrorMessage:[[Error objectForKey:@"email"]objectAtIndex:0]];
					}
					else if ([Error objectForKey:@"password"]) {
						//	Get the password error
						[self failedLoginWithErrorMessage:[[Error objectForKey:@"password"]objectAtIndex:0]];
					}
					else {
						[self failedLoginWithErrorMessage:@"Could not log in, your credentials appear to be incorrect."];
					}
				}
			}	//	END OF ELSE - no response
		}];
	}
	else {
		[self failedLoginWithErrorMessage:LocalizedString(@"CHKNET")];
	}
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void)getProfileAfterLogin {
	
	//	Ensure we still have an internet connection
	if ([appDelegate internetConnected]) {
		
		//	User alamofire networking to get the suers profile
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
		//[appDelegate onStartLoader];
		[afn getDataFromPath:PROFILE withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
			//[appDelegate onEndLoader];
			if (response) {
				NSLog(@"RESPONSE ...%@", response);
				NSString *first_name = [response valueForKey:@"first_name"];
				NSString *avatar =[Utilities removeNullFromString:[response valueForKey:@"avatar"]];
				NSString *status =[Utilities removeNullFromString:[response valueForKey:@"status"]];
				NSString *last_name = [response valueForKey:@"last_name"];
				
				NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
				[defaults setObject:last_name forKey:@"last_name"];
				[defaults setObject:first_name forKey:@"first_name"];
				[defaults setObject:avatar forKey:@"avatar"];
				[defaults setObject:status forKey:@"status"];
				[defaults setObject:[response valueForKey:@"id"] forKey:@"id"];
				[defaults setObject:[response valueForKey:@"sos"] forKey:@"sos"];
				
				NSString *nameStr = [NSString stringWithFormat:@"%@ %@", [Utilities removeNullFromString: response[@"first_name"]], [Utilities removeNullFromString: response[@"last_name"]]];
				
				// MARK: Set user
				[defaults setObject:SERVICE_URL forKey:@"serviceurl"];
				[defaults setObject:Client_SECRET forKey:@"passport"];
				[defaults setObject:ClientID forKey:@"clientid"];
				[defaults setObject:WEB_SOCKET forKey:@"websocket"];
				[defaults setObject:APP_NAME forKey:@"appname"];
				[defaults setObject:nameStr forKey:@"username"];
				//                 [defaults setObject:GMSMAP_KEY forKey:@"googleApiKey"];
				//                 [defaults setObject:Stripe_KEY forKey:@"stripekey"];
				
				[defaults setObject:@"1" forKey:@"is_valid"];
				[defaults synchronize];
				
				//	THIS IS THE SUCCESSFUL LOGIN RESPONSE
				[self successfulLogin];
			}
			else {
				if ([errorcode intValue]==1) {
					[self failedLoginWithErrorMessage:LocalizedString(@"ERRORMSG")];
				}
				else if ([errorcode intValue]==3) {
					
					[self failedToGetProfileShouldAttemptToRefreshToken];
				}
				else {
					[self failedLoginWithErrorMessage:@"Could not get your profile, try to login again later."];
				}
			}
		}];
	}
	else {
		[self failedLoginWithErrorMessage:LocalizedString(@"CHKNET")];
	}
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void)successfulLogin {
	//	Wrap up a successful login message in a notification and send it off to appropriate view controllers	
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:LoginRegisterProcessSuccess
	 object:self
	 userInfo: NULL];
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void)failedLoginWithErrorMessage: (NSString *) message {
	//	Should wrap this up in a notification and send it off
	
	//[CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
	
	NSDictionary *userInfo = @{@"ERRORMSG":message};
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:LoginRegisterProcessFailed
	 object:self
	 userInfo: userInfo];
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
-(void) failedToGetProfileShouldAttemptToRefreshToken {
	
	if ([appDelegate internetConnected]) {
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
		
		[afn refreshMethod_NoLoader:
		 @""withBlock:^(id responseObject, NSDictionary *error, NSString *errorcode) {
			 
			 if (responseObject)
			 {
				 NSLog(@"Refresh Method ...%@", responseObject);
				 NSString *access_token = [responseObject valueForKey:@"access_token"];
				 NSString *first_name = [responseObject valueForKey:@"first_name"];
				 NSString *avatar =[Utilities removeNullFromString:[responseObject valueForKey:@"avatar"]];
				 NSString *status =[Utilities removeNullFromString:[responseObject valueForKey:@"status"]];
				 NSString *currencyStr=[responseObject valueForKey:@"currency"];
				 NSString *socialId=[Utilities removeNullFromString:[responseObject valueForKey:@"social_unique_id"]];
				 NSString *last_name = [responseObject valueForKey:@"last_name"];
				 
				 
				 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
				 [defaults setObject:last_name forKey:@"last_name"];
				 [defaults setObject:access_token forKey:@"access_token"];
				 [defaults setObject:first_name forKey:@"first_name"];
				 [defaults setObject:avatar forKey:@"avatar"];
				 [defaults setObject:status forKey:@"status"];
				 [defaults setValue:currencyStr forKey:@"currency"];
				 [defaults setValue:socialId forKey:@"social_unique_id"];
				 [defaults setObject:[responseObject valueForKey:@"id"] forKey:@"id"];
				 [defaults setObject:[responseObject valueForKey:@"sos"] forKey:@"sos"];
			 }
			 else
			 {
				 [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
				 //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
				 
				 [self failedLoginWithErrorMessage:LocalizedString(@"ERRORMSG")];
			 }
		 }];
	}
	else {
		[self failedLoginWithErrorMessage:LocalizedString(@"CHKNET")];
		//[CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
	}
}
//	------------------------------------------------------------------------------------------------------------






//	============================================================================================================
//	MARK:- AKFViewControllerDelegate Methods
//	============================================================================================================

- (void) viewController:(UIViewController<AKFViewController> *)viewController
	didCompleteLoginWithAccessToken:(id<AKFAccessToken>)accessToken
				  state:(NSString *)state {
	
	NSLog(@"LoginRegisterService viewController - AKFViewController - didCompleteLoginWithAccessToken");
	
	AKFAccountKit *accountKit = [[AKFAccountKit alloc] initWithResponseType:AKFResponseTypeAccessToken];
	[accountKit requestAccount:^(id<AKFAccount> account, NSError *error) {
		
		//[appDelegate onStartLoader];
		
		NSLog(@"accountID ... %@",account.accountID);
		if ([account.emailAddress length] > 0) {
			NSLog(@"accountID ... %@",account.emailAddress);
		}
		else if ([account phoneNumber] != nil) {
			NSLog(@"account phone number ... %@",[[account phoneNumber] stringRepresentation]);
			self.phoneNumber = [[account phoneNumber] stringRepresentation];
		}
		
		if([self->loginByStr isEqualToString:@"FB"]) {
			//[appDelegate onEndLoader];
			[self finishFacebookLogin];
		}
		else if([self->loginByStr isEqualToString:@"GOOGLE"]) {
			//[appDelegate onEndLoader];
			[self finishGmailLogin];
		}
		else if([self->appDelegate internetConnected]) {
			NSString* UDID_Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
			NSLog(@"output is : %@", UDID_Identifier);
			
			NSDictionary*params;
			if([self->strLoginType isEqualToString:@"manual"]){
				params=@{
						 @"email":self.email,
						 @"password":self.password,
						 @"password_confirmation":self.password,
						 @"first_name":self.firstName,
						 @"last_name":self.lastName,
						 @"mobile":self.phoneNumber,
						 
						 @"device_token":self->appDelegate.device_tokenStr,
						 @"device_type":@"ios",
						 
						 @"login_by":@"manual",
						 
						 @"device_id":self->appDelegate.device_UDID};
			}
			
			NSLog(@"AFNHelper - POST - MD_REGISTER");
			AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
			[afn getDataFromPath:REGISTER withParamData:params withBlock:^(id response, NSDictionary *Error,NSString *strCode) {
				//[appDelegate onEndLoader];
				if(response) {
					[self performLogin];
					//[self successfulLogin];
				}
				else {
					if ([strCode intValue]==1) {
						[self failedLoginWithErrorMessage:LocalizedString(@"ERRORMSG")];
					}
					else {
						if ([Error objectForKey:@"email"]) {
							[self failedLoginWithErrorMessage:[[Error objectForKey:@"email"] objectAtIndex:0]];
						}
						else if ([Error objectForKey:@"first_name"]) {
							[self failedLoginWithErrorMessage:[[Error objectForKey:@"first_name"] objectAtIndex:0]];
							
						}
						else if ([Error objectForKey:@"last_name"]) {
							[self failedLoginWithErrorMessage:[[Error objectForKey:@"last_name"] objectAtIndex:0]];
						}
						else if ([Error objectForKey:@"mobile"]) {
							[self failedLoginWithErrorMessage:[[Error objectForKey:@"mobile"] objectAtIndex:0]];
						}
						else if ([Error objectForKey:@"password"]) {
							[self failedLoginWithErrorMessage:[[Error objectForKey:@"password"] objectAtIndex:0]];
						}
					}
				}
			}];
		}
		else {
			[self failedLoginWithErrorMessage:LocalizedString(@"CHKNET")];
		}
	}];
	[accountKit logOut];
}

- (void) viewController:(UIViewController<AKFViewController> *)viewController
  didCompleteLoginWithAuthorizationCode:(NSString *)code
								  state:(NSString *)state {
	
}

- (void)viewController:(UIViewController<AKFViewController> *)viewController didFailWithError:(NSError *)error {
	// ... implement appropriate error handling ...
	NSLog(@"%@ did fail with error: %@", viewController, error);
}

- (void)viewControllerDidCancel:(UIViewController<AKFViewController> *)viewController {
	// ... handle user cancellation of the login process ...
}






//	============================================================================================================
//	MARK:- GoogleSignInDelegate Methods
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
	
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
	[self.viewController presentViewController:viewController animated:YES completion:nil];
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
	[self.viewController dismissViewControllerAnimated:YES completion:nil];
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
	 withError:(NSError *)error {
	
	if(!error) {
		NSString *userId = user.userID;
		googleAccessToken = user.authentication.accessToken;
		NSLog(@"UserId %@",userId);
		NSLog(@"Google Access Token %@",googleAccessToken);
		loginByStr = @"GOOGLE";
		[self authenticateRegistrationWithPhoneNumber:self];
	}
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
	 withError:(NSError *)error {
	NSLog(@"Google Error...%@", error);
}
//	------------------------------------------------------------------------------------------------------------




@end
