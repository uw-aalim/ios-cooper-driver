//
//  LoginRegisterService.h
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import <Foundation/Foundation.h>

//	The AccountKit system is used to present a user interface and API for authenticating a request with a phone number
#import <AccountKit/AccountKit.h>
#import <AccountKit/AKFTheme.h>

#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface LoginRegisterService : NSObject<AKFViewControllerDelegate, GIDSignInDelegate, GIDSignInUIDelegate>
/**
 To use this class to register:
 Register for appropriate notifications - email validation success, loginRegisterSuccess, loginRegisterFailed
 Call validateEmailAddress - if get successful email validation notification
 	Call into register account with email, password, firstname, lastname, viewcontroller
 
 To use this class to login:
 Register for appropriate notifications: loginRegisterSuccess, loginRegisterFailed
 Call loginAccountWithEmail
 */
+ (instancetype)sharedInstance;
-(void)validateEmailAddress: (NSString *) email;
-(void)registerAccountWithEmail: (NSString *) email
				   withPassword: (NSString *) password
				  withFirstName: (NSString *) firstName
				   withLastName: (NSString *) lastName
			   inViewController: (UIViewController *) viewController;
-(void) loginAccountWithEmail: (NSString *) email
				 withPassword: (NSString *) password;
-(void) loginWithFacebookInViewController: (UIViewController *) viewController;
-(void) loginGoogleInViewController: (UIViewController *) viewController ;
@end
