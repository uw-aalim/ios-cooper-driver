//
//  Constant.m
//  Provider
//
//  Created by APPLE on 12/7/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "Constant.h"

//Staging
//NSString * SERVICE_URL = @"http://18.216.251.57/public";
//NSString *const SERVICE_URL = @"http://34.204.43.32";
NSString *const SERVICE_URL = @"https://login.trycooper.com";
NSString * APP_IMG_URL = @"";
NSString * APP_NAME = @"CoOper Driver";
//client_id = 1  &&& client_secret = LxkjKzSznvTzF14qcT1MsEC5WNloNyLXzLfyKGMr
//client_id = 2 &&& client_secret = fHe39PiU8xroenWDIBH3L9pMZEWKFFo3nt6n6Z2F
//Client ID: 6
//Client Secret: zpTsmHxa4A5BLHN02WvpiD0CSkUBnoSaBZtj5FMf
NSString * Client_SECRET = @"zpTsmHxa4A5BLHN02WvpiD0CSkUBnoSaBZtj5FMf";
NSString * ClientID = @"6";
//NSString * WEB_SOCKET = @"http://18.216.251.57:7000";
NSString * WEB_SOCKET = @"http://34.204.43.32:8000";

 NSString *const  REGISTER =  @"/api/provider/register";
 NSString *const  LOGIN = @"/api/provider/oauth/token";
 NSString *const  PROFILE = @"/api/provider/profile";
 NSString *const  STATUS = @"/api/provider/profile/available";
 NSString *const  CHANGE_PASSWORD = @"/api/provider/profile/password";
 NSString *const  LOCATION = @"/api/provider/location";
 NSString *const  INCOMING_REQUEST = @"/api/provider/trip";
 NSString *const  STATUS_CHECK = @"/api/provider/requests/status";
 NSString *const  PAST_TRIPS = @"/api/provider/requests/history";
 NSString *const  HISTORY_DETAILS = @"/api/provider/requests/history/details";

 NSString *const  UPCOMING_HISTORYDETAILS = @"/api/provider/requests/upcoming/details";
 NSString *const  UPCOMING_TRIPS = @"/api/provider/requests/upcoming";

 NSString *const  CANCEL_REQUEST = @"/api/provider/cancel";

 NSString *const  GET_EARNINGS = @"/api/provider/target";

 NSString *const  MD_FORGOTPASSWORD = @"/api/provider/forgot/password";
 NSString *const  MD_RESETPASSWORD = @"/api/provider/reset/password";
 NSString *const  MD_FACEBOOK = @"/api/provider/auth/facebook";
 NSString *const  MD_GOOGLE = @"/api/provider/auth/google";
 NSString *const  MD_HELP = @"/api/provider/help";
 NSString *const  MD_SUMMARY = @"/api/provider/summary";
 NSString *const  MD_REFRESH = @"/api/provider/refresh/token";

NSString *const UD_ACCESS_TOKEN =@"access_token";
NSString *const UD_TOKEN_TYPE = @"token_type";

NSString *const UD_REFERSH_TOKEN = @"token_type";
NSString *const UD_SOCIAL = @"ref_token";
