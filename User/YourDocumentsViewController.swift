//
//  YourDocumentsViewController.swift
//  Provider
//
//  Created by sandy on 2019/2/13.
//  Copyright © 2019 iCOMPUTERS. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class YourDocumentsViewController: UIViewController {
    
    @IBOutlet weak var tableView: YourDocumentTableView! {
        didSet {
            self.tableView.parentVC = self
        }
    }
    
    @IBOutlet weak var loadingView: UIView!
    
    @IBOutlet weak var webviewContainerView: UIView!
    @IBOutlet weak var webView: UIWebView!
    
    
    var imagePickerOnceDismissed = false
    var imagePicker: UIImagePickerController!
    
    var currentUploadingDocument: Document?
    
    
    @objc
    static func getNewInstance() -> YourDocumentsViewController {
        let storyboardName = "YourDocuments"
        let viewControllerIdentifier = "YourDocumentsViewController"
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: viewControllerIdentifier) as! YourDocumentsViewController
        return vc
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.scalesPageToFit = true
        
        self.initImagePicker()
        
        self.loadDocuments()
        
        
    }
    
    func initImagePicker() {
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        
    }
    
    func loadDocuments() {
        
        
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        
        if !appDelegate.internetConnected() {
            
            self.showAlert("Error", "Internet is not connected")

            return
        }
        
        guard let providerId = UserDefaults.standard.value(forKey: "id") as? Int else { return }
        
        
        let path = "/api/user/provider/" + String(providerId) + "/documents"
        
        let afn = AFNHelper.init(requestMethod: GET_METHOD)
        
        
        afn?.getDataFromPath(path, withParamData: [:], with: { (response, error, strErrorCode) in
            
            
            if response != nil {
                
                if let rawData = response as? NSArray {
                    
                    var tableData = [Document]()
                    
                    for item in rawData {
                        
                        if let item = item as? NSDictionary {
                            tableData.append(Document.init(item))
                        }
                    }
                    
                    self.tableView.setData(tableData)
                    self.tableView.reloadData()
                }
                
            } else {
                
                if let strCode = strErrorCode,
                    let code = Int(strCode) {
                    if code == 1 {
                        self.showAlert("Error", "Server error")
                    } else {
                        self.showAlert("Error", "Data error")
                    }
                }
                
            }
            
        })
        
    }

    func showAlert(_ title: String, _ msg: String) {
    
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alertController.addAction(alertAction)
        
        alertController.popoverPresentationController?.sourceView = self.view
        
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    
    func showLoadingView() {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.loadingView.alpha = 1
        }) { (_) in
            self.loadingView.isHidden = false
        }
        
    }
    
    func hideLoadingView() {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.loadingView.alpha = 0
        }) { (_) in
            self.loadingView.isHidden = true
        }
        
    }
    
    
    
    func showDetailView() {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.webviewContainerView.alpha = 1
        }) { (_) in
            self.webviewContainerView.isHidden = false
        }
    }
    
    func hideDetailView() {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.webviewContainerView.alpha = 0
        }) { (_) in
            self.webviewContainerView.isHidden = true
        }
        
        
    }
    
    
    
    @IBAction func onBtnBackClick(_ sender: Any) {

        self.navigationController?.popViewController(animated: true)
        
    }
    
    func showDocument(_ providerDocument: ProviderDocument) {
        
        let urlString = SERVICE_URL + "/storage/" + providerDocument.url
        
        guard let url = URL(string: urlString) else { return }
        
        let request = URLRequest(url: url)
        
        self.webView.loadRequest(request)
        
        
        self.showDetailView()
        
    }
    
    
    func uploadDocument(_ document: Document) {
        
        self.currentUploadingDocument = document
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(
            UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        )
        
        alertController.addAction(
            UIAlertAction(title: "From Photos", style: .default) { (alertAction) in
                self.imagePicker.sourceType = .photoLibrary
                self.imagePickerOnceDismissed = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        )
        
        alertController.addAction(
            UIAlertAction(title: "From Camera", style: .default) { (alertAction) in
                self.imagePicker.sourceType = .camera
                self.imagePickerOnceDismissed = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        )
        
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func uploadImage(_ image: UIImage) {
        
       
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        
        if !appDelegate.internetConnected() {
            
            self.showAlert("Error", "Internet is not connected")
            
            return
        }
        
        guard let providerId = UserDefaults.standard.value(forKey: "id") as? Int else { return }
        
        guard let documentId = self.currentUploadingDocument?.id else { return }
        
        
        let path = "/api/provider/" + String(providerId) + "/documents/" + String(documentId)
        
        let afn = AFNHelper.init(requestMethod: POST_METHOD)
        
        
        
        afn?.getDataFromPath(path, withDocumentImage: image, with: { (response, error, strErrorCode) in
            
            
            if response != nil {
                
                self.loadDocuments()
                
            } else {
                
                if let strCode = strErrorCode,
                    let code = Int(strCode) {
                    if code == 1 {
                        self.showAlert("Error", "Server error")
                    } else {
                        self.showAlert("Error", "Data error")
                    }
                }
                
            }
            
            
            
        })
        
        
        
        
    }
    
    @IBAction func onCloseButtonClick(_ sender: Any) {
        
        self.hideDetailView()
    }
    
    
    func resizeImage(source src_img: UIImage!, newSize size: CGSize) -> UIImage! { // returns resized image with size
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
        src_img.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: size.width, height: size.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getRandomFileName() -> String {
        let timestamp = Date().timeIntervalSince1970
        return "tempfile_" + String(timestamp)
    }
    
}



// UIImagePickerControllerDelegate
extension YourDocumentsViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if self.imagePickerOnceDismissed {
            return
        }
        self.imagePickerOnceDismissed = true
        
        var chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        chosenImage = self.resizeImage(source: chosenImage, newSize: CGSize(width: 1000, height: 1000))
        
        dismiss(animated: true) {
            self.uploadImage(chosenImage)
        }
        
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        if self.imagePickerOnceDismissed {
            return
        }
        self.imagePickerOnceDismissed = true
        
        dismiss(animated: true, completion: nil)
    }
}

// UINavigationControllerDelegate
extension YourDocumentsViewController: UINavigationControllerDelegate {
    
}
