//
//  oneViewController.m
//  WalkThroughObjc
//
//  Created by CSS on 11/09/17.
//  Copyright © 2017 Appoets. All rights reserved.
//

#import "oneViewController.h"


@interface oneViewController ()
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIImageView *MoveImgeview;
@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (weak, nonatomic) IBOutlet UIImageView *topIconImageview;

@end

@implementation oneViewController

- (void)viewDidLoad {
    [super viewDidLoad];

   // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self viewDidApperaence];
    });
    
}


-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];

}

#pragma ViewAppearecnce for Walkthrough

-(void)viewDidApperaence{
  
    [CommonAnimations roundCornersOnView:_baseView onTopLeft:NO topRight:NO bottomLeft:YES bottomRight:YES radius:[UIScreen mainScreen].bounds.size.width/2];
    [CommonAnimations roundCornersOnView:_infoView onTopLeft:YES topRight:NO bottomLeft:YES bottomRight:YES radius:[UIScreen mainScreen].bounds.size.width/2];
    [CommonAnimations AnimationAppearenceforWalkthrough:_infoView withSideOfAnimation:[[constantsWalk sharedManager] isRight]];
    [CommonAnimations AnimationForMoveview:_MoveImgeview withAnimationDirations:10.0];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
