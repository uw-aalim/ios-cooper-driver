//
//  YourDocumentsModels.swift
//  Provider
//
//  Created by sandy on 2019/2/13.
//  Copyright © 2019 iCOMPUTERS. All rights reserved.
//

import Foundation

enum DocumentType: String {
    case driver = "DRIVER"
    case vehicle = "VEHICLE"
}

enum ProviderDocumentStatus: String {
    case active = "ACTIVE"
    case assessing = "ASSESSING"
    case missing = "MISSING"
}

class Document {
    var id: Int!
    var name: String!
    var type: DocumentType!
    
    var providerDocument: ProviderDocument?
    
    init(_ json: NSDictionary) {
        
        if let id = json["id"] as? Int {
           self.id = id
        } else {
            self.id = 0
        }
        
        if let name = json["name"] as? String {
            self.name = name
        } else {
            self.name = "---"
        }
        
        if let type = json["type"] as? String {
            if type == "DRIVER" {
                self.type = .driver
            } else {
                self.type = .vehicle
            }
        } else {
            self.type = .driver
        }
        
        if let providerDocument = json["provider_document"] as? NSDictionary {
            self.providerDocument = ProviderDocument(providerDocument)
        } else {
            self.providerDocument = nil
        }
    }
    
}

class ProviderDocument {
    
    var documentId: Int!
    var url: String!
    var status: ProviderDocumentStatus!
    var expiresAt: String!
    
    init(_ json: NSDictionary) {
        
        
        if let documentId = json["document_id"] as? Int {
            self.documentId = documentId
        } else {
            self.documentId = 0
        }
        
        
        if let url = json["url"] as? String {
            self.url = url
        } else {
            self.url = ""
        }
        
        if let status = json["status"] as? String {
            if status == "ACTIVE" {
                self.status = .active
            } else {
                self.status = .assessing
            }
        } else {
            self.status = .missing
        }
        
        if let expiresAt = json["expires_at"] as? String {
            self.expiresAt = expiresAt
        } else {
            self.expiresAt = "---"
        }
        
    }
    
}



