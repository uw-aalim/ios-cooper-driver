//
//  RegisterViewController.m
//  User
//
//  Created by iCOMPUTERS on 12/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "RegisterViewController.h"
#import "CSS_Class.h"
#import "EmailViewController.h"
#import "config.h"
#import "UIScrollView+EKKeyboardAvoiding.h"
#import "HomeViewController.h"
#import "AFNHelper.h"
#import "Colors.h"
#import "ViewController.h"
#import "Utilities.h"
#import "newHomeController.h"
@interface RegisterViewController ()

@end

@implementation RegisterViewController
{
    AKFAccountKit *_accountKit;
    UIViewController<AKFViewController> *_pendingLoginViewController;
    NSString *_authorizationCode;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self LocalizationUpdate];
    
    [_firstNameText setUserInteractionEnabled:NO];
    [_lastNameText setUserInteractionEnabled:NO];
    [_passwordText setUserInteractionEnabled:NO];
    [_confirmPasswordText setUserInteractionEnabled:NO];
    
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapReceived:)];
    [tapGestureRecognizer setDelegate:self];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    [_detailsScrollView setContentSize:[_detailsScrollView frame].size];
    [_detailsScrollView setKeyboardAvoidingEnabled:YES];
    
    [self setDesignStyles];
    
   // NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //NSString *emailStr = [defaults valueForKey:@"Email"];
    //_emailText.text = emailStr;
    
//    pickerViewContainer                 = [[UIView alloc] init];
//    comm_PickerView                          = [[UIPickerView alloc] init];
//    pickerViewContainer.backgroundColor = [UIColor whiteColor];
//    
//    [comm_PickerView setDataSource:self];
//    [comm_PickerView setDelegate:self];
    
    
    // initialize Account Kit
    if (_accountKit == nil) {
        // may also specify AKFResponseTypeAccessToken
        _accountKit = [[AKFAccountKit alloc] initWithResponseType:AKFResponseTypeAccessToken];
    }
    
    // view controller for resuming login
    _pendingLoginViewController = [_accountKit viewControllerForLoginResume];

}

-(void)LocalizationUpdate{
    _headerLbl.text = LocalizedString(@"Enter the details to register");
    _emailLbl.text = LocalizedString(@"E-mail");
    _firstNameLbl.text = LocalizedString(@"Name");
    _firstNameText.placeholder =LocalizedString(@"First name");
    _lastNameText.placeholder =LocalizedString(@"Last name");
    _passwordLbl.text = LocalizedString(@"Password");
    _confirmPassLbl.text = LocalizedString(@"Confirm password");
    _phoneLbl.text = LocalizedString(@"Phone Number");
    
}
-(void)tapReceived:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)_prepareLoginViewController:(UIViewController<AKFViewController> *)loginViewController
{
    loginViewController.delegate = self;
    // Optionally, you may use the Advanced UI Manager or set a theme to customize the UI.
    loginViewController.uiManager = [[AKFSkinManager alloc]
                                     initWithSkinType:AKFSkinTypeTranslucent
                                     primaryColor:BLACKCOLOR
                                     backgroundImage:[UIImage imageNamed:@"bg-1536"]
                                     backgroundTint:AKFBackgroundTintWhite
                                     tintIntensity:0.32];
    loginViewController.uiManager.theme.buttonTextColor = [UIColor whiteColor];
    loginViewController.enableSendToFacebook = NO;
    

}
- (void)loginWithPhone:(id)sender
{
    [[NSUserDefaults standardUserDefaults]setObject:@"User" forKey:@"SocialLogin"];

    NSString *inputState = [[NSUUID UUID] UUIDString];
    UIViewController<AKFViewController> *viewController = [_accountKit viewControllerForPhoneLoginWithPhoneNumber:nil state:inputState];
    viewController.enableSendToFacebook = YES; // defaults to NO
    viewController.enableGetACall=YES;

    [viewController.uiManager.theme setButtonTextColor:[UIColor whiteColor]];
   // loginViewController.enableSendToFacebook = YES;
    [self _prepareLoginViewController:viewController]; // see below
    [self presentViewController:viewController animated:YES completion:NULL];
}

- (void) viewController:(UIViewController<AKFViewController> *)viewController
didCompleteLoginWithAccessToken:(id<AKFAccessToken>)accessToken state:(NSString *)state
{
    AKFAccountKit *accountKit = [[AKFAccountKit alloc] initWithResponseType:AKFResponseTypeAccessToken];
    [accountKit requestAccount:^(id<AKFAccount> account, NSError *error) {
        // account ID
        
        NSLog(@"accountID ... %@",account.accountID);
        if ([account.emailAddress length] > 0) {
            NSLog(@"accountID ... %@",account.emailAddress);
        }
        else if ([account phoneNumber] != nil) {
            NSLog(@"accountID ... %@",[[account phoneNumber] stringRepresentation]);
            _phoneText.text =[[account phoneNumber] stringRepresentation];
        }
        if([appDelegate internetConnected])
        {
            NSDictionary*params;
            params=@{
					 @"email":_emailText.text,
					 @"password":_passwordText.text,
					 @"password_confirmation":_passwordText.text,
					 @"first_name":_firstNameText.text,
					 @"last_name":_lastNameText.text,
					 @"mobile":_phoneText.text,
					 
					 @"device_token":appDelegate.device_tokenStr,
					 @"device_type":@"ios",
					 
					 @"login_by":@"manual",
					 
					 
					 @"device_id":appDelegate.device_UDID};
            
            NSLog(@"PARAMS...%@", params);
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            
            [afn getDataFromPath:REGISTER withParamData:params withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
             {
                 if (response)
                 {
                     NSLog(@"RESPONSE ...%@", response);
                     
                     NSDictionary*params;
                     params=@{@"email":_emailText.text,@"password":_passwordText.text,@"device_token":appDelegate.device_tokenStr,@"device_id":appDelegate.device_UDID, @"device_type":@"ios"};
                     
                     AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                     
                     [afn getDataFromPath:LOGIN withParamData:params withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
                      {
                          if (response)
                          {
                              NSLog(@"RESPONSE ...%@", response);
                              NSString *access_token = [response valueForKey:@"access_token"];
                              NSString *first_name = [response valueForKey:@"first_name"];
                              NSString *avatar =[Utilities removeNullFromString:[response valueForKey:@"avatar"]];
                              NSString *status =[Utilities removeNullFromString:[response valueForKey:@"status"]];
                              NSString *currency =[Utilities removeNullFromString:[response valueForKey:@"currency"]];
                              NSString *last_name = [response valueForKey:@"last_name"];
                              
                              
                              NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                              [defaults setObject:last_name forKey:@"last_name"];
                              [defaults setObject:access_token forKey:@"access_token"];
                              [defaults setObject:first_name forKey:@"first_name"];
                              [defaults setObject:avatar forKey:@"avatar"];
                              [defaults setObject:status forKey:@"status"];
                              [defaults setObject:currency forKey:@"currency"];
                              [defaults setObject:[response valueForKey:@"id"] forKey:@"id"];
                              [defaults setObject:[response valueForKey:@"sos"] forKey:@"sos"];
                              
                              NSString *nameStr = [NSString stringWithFormat:@"%@ %@", [Utilities removeNullFromString: response[@"first_name"]], [Utilities removeNullFromString: response[@"last_name"]]];
                              // MARK: Set user
                              
                              [defaults setObject:SERVICE_URL forKey:@"serviceurl"];
                              [defaults setObject:Client_SECRET forKey:@"passport"];
                              [defaults setObject:ClientID forKey:@"clientid"];
                              [defaults setObject:WEB_SOCKET forKey:@"websocket"];
                              [defaults setObject:APP_NAME forKey:@"appname"];
                              [defaults setObject:nameStr forKey:@"username"];
//                              [defaults setObject:GMSMAP_KEY forKey:@"googleApiKey"];
//                              [defaults setObject:Stripe_KEY forKey:@"stripekey"];
                              
                              [defaults setObject:@"1" forKey:@"is_valid"];
                              [defaults synchronize];
                              
                              [self nextAction];
                          }
                          else
                          {
                              _passwordText.text = @"";
                              if ([strErrorCode intValue]==1)
                              {
                                  [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                              }
                              else if ([strErrorCode intValue]==2)
                              {
                                  if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                                  {
                                      //Refresh token
                                  }
                                  else
                                  {
                                      [self refreshMethod];

//                                      [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
//                                      ViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//                                      [self.navigationController pushViewController:wallet animated:YES];
                                  }
                              }
                              else if ([strErrorCode intValue]==3)
                              {
                                  if ([error objectForKey:@"email"]) {
                                      [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                                  }
                                  else if ([error objectForKey:@"password"]) {
                                      [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"password"] objectAtIndex:0]  viewController:self okPop:NO];
                                  }
                                  else
                                  {
                                      [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG")  viewController:self okPop:NO];
                                  }
                              }
                              else
                              {
                                  [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG")  viewController:self okPop:NO];
                              }
                              NSLog(@"%@",error);
                              
                          }
                      }];
                 }
                 else
                 {
                     if ([strErrorCode intValue]==1)
                     {
                         [CSS_Class alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                     else if ([strErrorCode intValue]==2)
                     {
                         if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                         {
                             //Refresh token
                         }
                         else
                         {
                             [self refreshMethod];

//                             [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
//                             ViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//                             [self.navigationController pushViewController:wallet animated:YES];
                         }
                     }
                     else if ([strErrorCode intValue]==3)
                     {
                         if ([error objectForKey:@"email"]) {
                             [CSS_Class alertviewController_title:@"" MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                         }
                         else if ([error objectForKey:@"first_name"]) {
                             [CSS_Class alertviewController_title:@"" MessageAlert:[[error objectForKey:@"first_name"] objectAtIndex:0]  viewController:self okPop:NO];
                         }
                         else if ([error objectForKey:@"last_name"]) {
                             [CSS_Class alertviewController_title:@"" MessageAlert:[[error objectForKey:@"last_name"] objectAtIndex:0]  viewController:self okPop:NO];
                         }
                         else if ([error objectForKey:@"mobile"]) {
                             [CSS_Class alertviewController_title:@"" MessageAlert:[[error objectForKey:@"mobile"] objectAtIndex:0]  viewController:self okPop:NO];
                         }
                         else if ([error objectForKey:@"password"]) {
                             [CSS_Class alertviewController_title:@"" MessageAlert:[[error objectForKey:@"password"] objectAtIndex:0]  viewController:self okPop:NO];
                         }
                         else if ([error objectForKey:@"password_confirmation"]) {
                             [CSS_Class alertviewController_title:@"" MessageAlert:[[error objectForKey:@"password_confirmation"] objectAtIndex:0]  viewController:self okPop:NO];
                         }
                         else
                         {
                             [CSS_Class alertviewController_title:@"Error!" MessageAlert:@"Please try again later"  viewController:self okPop:NO];
                         }
                     }
                     else
                     {
                         [CSS_Class alertviewController_title:@"Error!" MessageAlert:@"Please try again later"  viewController:self okPop:NO];
                     }
                     NSLog(@"%@",error);
                     
                 }
             }];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }];
    [accountKit logOut];
}



-(void)nextAction{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"New" bundle: nil];
    
    newHomeController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"newHomeController"];
    [self.navigationController pushViewController:controller animated:YES];
}
- (void)                 viewController:(UIViewController<AKFViewController> *)viewController
  didCompleteLoginWithAuthorizationCode:(NSString *)code
                                  state:(NSString *)state
{
    
}

- (void)viewController:(UIViewController<AKFViewController> *)viewController didFailWithError:(NSError *)error
{
    // ... implement appropriate error handling ...
    NSLog(@"%@ did fail with error: %@", viewController, error);
}

- (void)viewControllerDidCancel:(UIViewController<AKFViewController> *)viewController
{
    // ... handle user cancellation of the login process ...
}

-(void)setDesignStyles
{
    [CSS_Class APP_labelName:_headerLbl];
    [CSS_Class APP_textfield_Outfocus:_emailText];
    [CSS_Class APP_textfield_Outfocus:_firstNameText];
    
    [CSS_Class APP_textfield_Outfocus:_lastNameText];
    [CSS_Class APP_textfield_Outfocus:_passwordText];
    [CSS_Class APP_textfield_Outfocus:_phoneText];
    [CSS_Class APP_textfield_Outfocus:_confirmPasswordText];
    
    [CSS_Class APP_labelName_Small:_emailLbl];
    [CSS_Class APP_labelName_Small:_firstNameLbl];
    [CSS_Class APP_labelName_Small:_lastNameLbl];
    [CSS_Class APP_labelName_Small:_passwordLbl];
    [CSS_Class APP_labelName_Small:_phoneLbl];
    [CSS_Class APP_labelName_Small:_confirmPassLbl];
}

-(IBAction)backBtn:(id)sender
{
//    PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
//    [self.navigationController pushViewController:controller animated:YES];
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_emailText)
    {
        [self emailValidateMethod];
    }
    else if(textField==_firstNameText)
    {
        [_lastNameText becomeFirstResponder];
    }
    else if(textField==_lastNameText)
    {
        [_passwordText becomeFirstResponder];
    }
    else if(textField==_passwordText)
    {
        [_confirmPasswordText becomeFirstResponder];
    }
    else if(textField==_confirmPasswordText)
    {
        [_confirmPasswordText resignFirstResponder];
    }
//    else if(textField==_carNumberText)
//    {
//        [_carNumberText resignFirstResponder];
//    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField == _emailText)
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= INPUTLENGTH || returnKey;
    }
    else if ((textField == _firstNameText) || (textField == _lastNameText))
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    else if (textField == _phoneText)
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= PHONELENGTH || returnKey;
    }
    else
    {
        return YES;
    }
    return NO;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    [CSS_Class APP_textfield_Infocus:textField];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if(textField==_emailText)
    {
        [self emailValidateMethod];
    }
    [CSS_Class APP_textfield_Outfocus:textField];
    return YES;
}

-(IBAction)Nextbtn:(id)sender
{
    [self.view endEditing:YES];
    
//    if((_emailText.text.length==0) || (_firstNameText.text.length==0) || (_lastNameText.text.length==0) || (_passwordText.text.length==0) || (_confirmPasswordText.text.length==0))
//    {
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString (@"Alert!") message:LocalizedString(@"Please fill all the details") preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//        [alertController addAction:ok];
//        [self presentViewController:alertController animated:YES completion:nil];
//        
//    }
//    else if (![_passwordText.text isEqualToString:_confirmPasswordText.text])
//    {
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"Password doesn't match") preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
//        [alertController addAction:ok];
//        [self presentViewController:alertController animated:YES completion:nil];
//    }
    
    //else
    //{
    if([self RegisterValidations:_emailText.text :_firstNameText.text :_lastNameText.text :_passwordText.text :_confirmPasswordText.text]){
        [self loginWithPhone:self];
    }
    
    //}
}

-(BOOL)RegisterValidations :(NSString *)email :(NSString *)firstname :(NSString *)lastname :(NSString *)password :(NSString *)confirmPassword {
    if([self EmptyFieldValidation:email :LocalizedString(@"Please enter your email")]){
        
        if([self EmptyFieldValidation:firstname :LocalizedString(@"Please enter your first name")]){
            if([self EmptyFieldValidation:lastname :LocalizedString(@"Please enter your last name")]){
                
                if([self Passwordvalidaton:password]){
                    if([self ConfirmpasswordMatch:password withConfirmpassword:confirmPassword]){
                        return YES;
                    }
                }
            }
        }
    }
    return NO;
}



-(BOOL)EmptyFieldValidation:(NSString *)field :(NSString *)Aler{
    if(field.length == 0){
        [CSS_Class alertviewController_title:@"Alert" MessageAlert:Aler viewController:self okPop:NO];
        return NO;
    }
    return YES;
    
}
-(BOOL)Passwordvalidaton:(NSString *)password {
    
    if(password.length != 0){
        NSString *stricterFilterString = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,15}$";
        NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
        if( [passwordTest evaluateWithObject:password]){
            return YES;
        }else{
            [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:LocalizedString(@"The password must be minimum 8 characters and 1 Uppercase, 1 Lowercase , 1 number , and 1 special characters and maximum 16 characters ") viewController:self okPop:NO];
        }
        
    }else{
        [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:LocalizedString(@"Password is required") viewController:self okPop:NO];
    }
    return false;
}

-(BOOL)ConfirmpasswordMatch :(NSString *)password withConfirmpassword :(NSString *)confirmpassword{
    if(![password isEqualToString:confirmpassword]){
        [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:LocalizedString(@"Password doesn't match") viewController:self okPop:NO];
        return NO;
    }
    return YES;
}


-(void)emailValidateMethod
{
    if([appDelegate internetConnected])
    {
        NSDictionary*params;
        params=@{@"email":_emailText.text};
        
        NSLog(@"PARAMS...%@", params);
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn getDataFromPath:@"/api/provider/verify" withParamData:params withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
         {
             if (response)
             {
                NSLog(@"EmailVAlid ..%@", response);
                [_firstNameText becomeFirstResponder];
               
                 [_firstNameText setUserInteractionEnabled:YES];
                 [_lastNameText setUserInteractionEnabled:YES];
                 [_passwordText setUserInteractionEnabled:YES];
                 [_confirmPasswordText setUserInteractionEnabled:YES];

            }
            else
            {
                _emailText.text =@"";
                if ([strErrorCode intValue]==1)
                {
                    [CSS_Class alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"Please try again later")  viewController:self okPop:NO];
                }
                else
                {
                    if ([error objectForKey:@"email"]) {
                        [CSS_Class alertviewController_title:@"" MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                    }
                }
                
            }
        }];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


-(void)refreshMethod
{
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn refreshMethod_NoLoader:
     @""withBlock:^(id responseObject, NSDictionary *error, NSString *errorcode) {
         
         if (responseObject)
         {
             NSLog(@"Refresh Method ...%@", responseObject);
             NSString *access_token = [responseObject valueForKey:@"access_token"];
             NSString *first_name = [responseObject valueForKey:@"first_name"];
             NSString *avatar =[Utilities removeNullFromString:[responseObject valueForKey:@"avatar"]];
             NSString *status =[Utilities removeNullFromString:[responseObject valueForKey:@"status"]];
             NSString *currencyStr=[responseObject valueForKey:@"currency"];
             NSString *socialId=[Utilities removeNullFromString:[responseObject valueForKey:@"social_unique_id"]];
             NSString *last_name = [responseObject valueForKey:@"last_name"];
             
             
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setObject:last_name forKey:@"last_name"];
             [defaults setObject:access_token forKey:@"access_token"];
             [defaults setObject:first_name forKey:@"first_name"];
             [defaults setObject:avatar forKey:@"avatar"];
             [defaults setObject:status forKey:@"status"];
             [defaults setValue:currencyStr forKey:@"currency"];
             [defaults setValue:socialId forKey:@"social_unique_id"];
             [defaults setObject:[responseObject valueForKey:@"id"] forKey:@"id"];
             [defaults setObject:[responseObject valueForKey:@"sos"] forKey:@"sos"];
         }
         else
         {
             [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
			 
			 /*
             PageViewController * wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];

             [self.navigationController pushViewController:wallet animated:YES]; */
			 WelcomeViewController *viewController = [WelcomeViewController initController];
			 UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
			 [self.navigationController presentViewController:navigationController animated:YES completion:^{
				 NSLog(@"Presented welcome view controller");
			 }];
         }
     }];
}

//-(BOOL) emailValidation:(NSString *)emailTxt
//{
//    if([emailTxt length]==0)
//    {
//        [CommenMethods alertviewController_title:@"Please enter email" MessageAlert:@"" viewController:self okPop:NO];
//    }
//    else
//    {
//        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
//        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//        if([emailTest evaluateWithObject:emailTxt])
//            return YES;
//        else
//            [CommenMethods alertviewController_title:@"Please enter valid email" MessageAlert:@"" viewController:self okPop:NO];
//    }
//    return NO;
//}

@end
