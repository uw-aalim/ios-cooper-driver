//
//  HelpViewController.m
//  User
//
//  Created by iCOMPUTERS on 21/06/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "HelpViewController.h"
#import "config.h"
#import "CSS_Class.h"
#import "Colors.h"
#import "HomeViewController.h"
#import "AFNHelper.h"
#import "ViewController.h"
#import "Utilities.h"
#import "Provider-Swift.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];

    strProviderCell = @"";
    [self getHelpDetails];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self LocalizationUpdate];
}

-(void)LocalizationUpdate{
    _navicationTitleLbl.text = LocalizedString(@"HELP");
    _helpLbl.text = LocalizedString(@"coOper Help");
    _infoLbl.text = LocalizedString(@"Our team person will contact you soon!");
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)callBtn:(id)sender
{
    if ([strProviderCell isEqualToString:@""])
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert") message:LocalizedString(@"Admin was not provided the number to call.") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:strProviderCell]];
        NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:strProviderCell]];
        
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl])
        {
            [UIApplication.sharedApplication openURL:phoneUrl options:@{} completionHandler:nil];
        }
        else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl])
        {
            [UIApplication.sharedApplication openURL:phoneFallbackUrl options:@{} completionHandler:nil];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert") message:LocalizedString(@"Your device does not support calling") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

-(IBAction)mailBtn:(id)sender
{
    if ([appDelegate internetConnected])
    {
        MFMailComposeViewController* composeVC = [[MFMailComposeViewController alloc] init];
        composeVC.mailComposeDelegate = self;
        
        // Configure the fields of the interface.
        [composeVC setToRecipients:@[mailAddress]];
        [composeVC setSubject:@"coOper Mail Composer"];
        [composeVC setMessageBody:@"Hello coOper" isHTML:NO];
        
        if (![MFMailComposeViewController canSendMail]) {
            NSLog(@"Mail services are not available.");
            return;
        }
        else
        {
            // Present the view controller modally.
            [self presentViewController:composeVC animated:YES completion:nil];
        }
    }
    else
    {
        [CSS_Class alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    // Check the result or perform other tasks.
    
    [self dismissViewControllerAnimated:YES completion:nil];
    if(result ==MFMailComposeResultCancelled)
    {
        [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:LocalizedString(@"Mail cancelled by you") viewController:self okPop:NO];
    }
    else if(result ==MFMailComposeResultSent)
    {
        [CSS_Class alertviewController_title:LocalizedString(@"Success!") MessageAlert:LocalizedString(@"Our team person will contact you.") viewController:self okPop:NO];
    }
    else if(result ==MFMailComposeResultFailed)
    {
        [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:LocalizedString(@"Mail sent failed, Please try again later.") viewController:self okPop:NO];
    }
}

-(IBAction)browserBtn:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:SERVICE_URL]];
}

-(void)getHelpDetails
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:MD_HELP withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSLog(@"help response...%@", response);
                strProviderCell = [Utilities removeNullFromString:[response valueForKey:@"contact_number"]];
                mailAddress = [Utilities removeNullFromString:[response valueForKey:@"contact_email"]];
            }
            else
            {
                if ([errorcode intValue]==1)
                {
                    [CSS_Class alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
//                    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
//                    ViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//                    [self.navigationController pushViewController:wallet animated:YES];
                    
                    [self refreshMethod];
                }
            }
            
        }];
    }
    else
    {
        [CSS_Class alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }    
}


-(void)refreshMethod
{
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn refreshMethod_NoLoader:
     @""withBlock:^(id responseObject, NSDictionary *error, NSString *errorcode) {
         
         if (responseObject)
         {
             NSLog(@"Refresh Method ...%@", responseObject);
             NSString *access_token = [responseObject valueForKey:@"access_token"];
             NSString *first_name = [responseObject valueForKey:@"first_name"];
             NSString *avatar =[Utilities removeNullFromString:[responseObject valueForKey:@"avatar"]];
             NSString *status =[Utilities removeNullFromString:[responseObject valueForKey:@"status"]];
             NSString *currencyStr=[responseObject valueForKey:@"currency"];
             NSString *socialId=[Utilities removeNullFromString:[responseObject valueForKey:@"social_unique_id"]];
             NSString *last_name = [responseObject valueForKey:@"last_name"];
             
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setObject:last_name forKey:@"last_name"];
             [defaults setObject:access_token forKey:@"access_token"];
             [defaults setObject:first_name forKey:@"first_name"];
             [defaults setObject:avatar forKey:@"avatar"];
             [defaults setObject:status forKey:@"status"];
             [defaults setValue:currencyStr forKey:@"currency"];
             [defaults setValue:socialId forKey:@"social_unique_id"];
             [defaults setObject:[responseObject valueForKey:@"id"] forKey:@"id"];
             [defaults setObject:[responseObject valueForKey:@"sos"] forKey:@"sos"];
         }
         else
         {
             [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
			 
			 /*
             UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

			 
             PageViewController * wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];

             [self.navigationController pushViewController:wallet animated:YES]; */
			 
			 WelcomeViewController *viewController = [WelcomeViewController initController];
			 UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
			 [self.navigationController presentViewController:navigationController animated:YES completion:^{
				 NSLog(@"Presented welcome view controller");
			 }];
         }
     }];
}


@end
