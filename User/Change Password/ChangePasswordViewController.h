//
//  ChangePasswordViewController.h
//  Provider
//
//  Created by iCOMPUTERS on 01/02/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "LoadingViewClass.h"

@class AppDelegate;
@interface ChangePasswordViewController : UIViewController<UIGestureRecognizerDelegate, UITextFieldDelegate, UIScrollViewDelegate>
{
    AppDelegate *appDelegate;
}


@property (weak, nonatomic) IBOutlet UILabel *titleBarLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *largeTitleViewTop;


@property (weak, nonatomic) IBOutlet UITextField *passText;
@property (weak, nonatomic) IBOutlet UITextField *confirmPassText;
@property (weak, nonatomic) IBOutlet UITextField *oldPassText;

@property (weak, nonatomic) IBOutlet UIButton *changePasswordBtn;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottom;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

+(instancetype)initController;

@end
