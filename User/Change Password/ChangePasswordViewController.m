//
//  ChangePasswordViewController.m
//  Provider
//
//  Created by iCOMPUTERS on 01/02/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "EmailViewController.h"
#import "CSS_Class.h"
#import "config.h"
#import "AFNHelper.h"
#import "ViewController.h"
#import "Utilities.h"
#import "Provider-Swift.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

//	============================================================================================================
//	MARK:- Override Properties
//	============================================================================================================

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}






//	============================================================================================================
//	MARK:- Initialization
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
+(instancetype)initController {
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ChangePassword" bundle: nil];
	ChangePasswordViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
	return viewController;
}
//	------------------------------------------------------------------------------------------------------------






//	============================================================================================================
//	MARK:- View Loading and Appearance
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    self->appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
	
	[self setupViewDesign];
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
-(void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	
	//	Once the view is visible - setup the notifications
	//	Doing so after the view has appeared is key to preventing multiple registration attempts
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(keyboardWillShow:)
	 name:UIKeyboardWillShowNotification
	 object:NULL];
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(keyboardWillShow:)
	 name:UIKeyboardWillChangeFrameNotification
	 object:NULL];
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(keyboardWillHide:)
	 name:UIKeyboardWillHideNotification
	 object:NULL];
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void) viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	
	[[NSNotificationCenter defaultCenter]
	 removeObserver:self];
}
//	------------------------------------------------------------------------------------------------------------





//	============================================================================================================
//	MARK:- View Design
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void)setupViewDesign {
	
	self.oldPassText.delegate = self;
	self.passText.delegate = self;
	self.confirmPassText.delegate = self;
	
	self.scrollView.delegate = self;
}
//	------------------------------------------------------------------------------------------------------------





//	============================================================================================================
//	MARK:- Remote Data Loading
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void)refreshMethod
{
	AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
	[afn refreshMethod_NoLoader:
	 @""withBlock:^(id responseObject, NSDictionary *error, NSString *errorcode) {
		 
		 if (responseObject)
		 {
			 NSLog(@"Refresh Method ...%@", responseObject);
			 NSString *access_token = [responseObject valueForKey:@"access_token"];
			 NSString *first_name = [responseObject valueForKey:@"first_name"];
			 NSString *avatar =[Utilities removeNullFromString:[responseObject valueForKey:@"avatar"]];
			 NSString *status =[Utilities removeNullFromString:[responseObject valueForKey:@"status"]];
			 NSString *currencyStr=[responseObject valueForKey:@"currency"];
			 NSString *socialId=[Utilities removeNullFromString:[responseObject valueForKey:@"social_unique_id"]];
			 NSString *last_name = [responseObject valueForKey:@"last_name"];
			 
			 
			 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			 [defaults setObject:last_name forKey:@"last_name"];
			 [defaults setObject:access_token forKey:@"access_token"];
			 [defaults setObject:first_name forKey:@"first_name"];
			 [defaults setObject:avatar forKey:@"avatar"];
			 [defaults setObject:status forKey:@"status"];
			 [defaults setValue:currencyStr forKey:@"currency"];
			 [defaults setValue:socialId forKey:@"social_unique_id"];
			 [defaults setObject:[responseObject valueForKey:@"id"] forKey:@"id"];
			 [defaults setObject:[responseObject valueForKey:@"sos"] forKey:@"sos"];
		 }
		 else
		 {
			 [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
			 /*
			  UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
			  
			  PageViewController * wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];
			  [self.navigationController pushViewController:wallet animated:YES];
			  */
			 
			 WelcomeViewController *viewController = [WelcomeViewController initController];
			 UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
			 
			 [self.navigationController presentViewController:navigationController animated:YES completion:^{
				 NSLog(@"Presented welcome view controller");
			 }];
		 }
	 }];
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
-(void)logoutMethod_FromApp
{
	if ([appDelegate internetConnected])
	{
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		NSString *idStr = [defaults valueForKey:@"id"];
		NSDictionary *param = @{@"id":idStr};
		
		NSString *url = [NSString stringWithFormat:@"/api/provider/logout"];
		[afn getDataFromPath_NoLoader:url withParamData:param withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
			if (response)
			{
				
				
				[defaults setValue:[NSNumber numberWithBool:0] forKey:@"is_valid"];
				
				[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
				
				/*
				 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
				 
				 PageViewController * wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];
				 
				 [self.navigationController pushViewController:wallet animated:YES];
				 */
			}
			else
			{
				[CSS_Class alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
			}
		}];
	}
	else
	{
		[CSS_Class alertviewController_title:LocalizedString(@"Alert") MessageAlert:NSLocalizedString(@"CHKNET", nil) viewController:self okPop:NO];
	}
	
}
//	------------------------------------------------------------------------------------------------------------




//	============================================================================================================
//	MARK:- Action Responders
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(IBAction)backBtn:(id)sender
{
	if (self.navigationController == NULL){
		[self dismissViewControllerAnimated:YES completion:nil];
	}
	else if (self.navigationController.viewControllers.count <= 1) {
		[self.navigationController.presentingViewController dismissViewControllerAnimated:true completion:NULL];
	}
	else {
		[self.navigationController popViewControllerAnimated:YES];
	}
	
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
-(IBAction)Nextbtn:(id)sender {
	[self.view endEditing:YES];
	
	if([self ChangePasswordValidation:_passText.text :_confirmPassText.text]){
		
		if([appDelegate internetConnected])
		{
			NSDictionary*params;
			params=@{@"password":_passText.text, @"password_confirmation":_confirmPassText.text, @"password_old":_oldPassText.text};
			
			AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
			appDelegate.viewControllerName = @"Profile";
			[afn getDataFromPath:CHANGE_PASSWORD  withParamData:params withBlock:^(id response, NSDictionary *error, NSString *strErrorCode) {
				 if (response) {
					 NSLog(@"RESPONSE ...%@", response);
					 
					 
					 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Success!") message:LocalizedString(@"Your password changed successfully") preferredStyle:UIAlertControllerStyleAlert];
					 
					 ;
					 
					 UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
						 
						 [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:0] forKey:@"is_valid"];
						 [self afterSucccessPasswordChange];
					 }];
					 [alertController addAction:ok];
					 
					 [self presentViewController:alertController animated:YES completion:nil];
				 }
				 else
				 {
					 if ([strErrorCode intValue]==1)
					 {
						 [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
					 }
					 else if ([strErrorCode intValue]==2)
					 {
						 if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
						 {
							 //Refresh token
						 }
						 else
						 {
							 //                             [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
							 //                             ViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
							 //                             [self.navigationController pushViewController:wallet animated:YES];
							 
							 [self refreshMethod];
						 }
					 }
					 else if ([strErrorCode intValue]==3)
					 {
						 if ([error objectForKey:@"password"]) {
							 [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"password"] objectAtIndex:0]  viewController:self okPop:NO];
						 }
						 else if ([error objectForKey:@"password_confirmation"]) {
							 [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"password_confirmation"] objectAtIndex:0]  viewController:self okPop:NO];
						 }
						 else if ([error objectForKey:@"password_old"]) {
							 [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"password_old"] objectAtIndex:0]  viewController:self okPop:NO];
						 }
						 else
						 {
							 [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:[error objectForKey:@"error"] viewController:self okPop:NO];
						 }
					 }
					 else
					 {
						 [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
					 }
					 NSLog(@"%@",error);
					 
				 }
			 }];
		}
		else
		{
			UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
			UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
			[alertController addAction:ok];
			[self presentViewController:alertController animated:YES completion:nil];
		}
	}
	
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void)afterSucccessPasswordChange{
	
	dispatch_async(dispatch_get_main_queue(), ^{
		[self backBtn:self];
	});
}
//	------------------------------------------------------------------------------------------------------------










//	============================================================================================================
//	MARK:- UITextFieldDelegate methods
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_oldPassText) {
        [_passText becomeFirstResponder];
    }
    if(textField==_passText) {
        [_confirmPassText becomeFirstResponder];
    }
    else if(textField==_confirmPassText) {
        [_confirmPassText resignFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return YES;
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if((textField == _passText) || (textField == _oldPassText) || (textField == _confirmPassText)) {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= PASSWORDLENGTH || returnKey;
    }
    else
    {
        return YES;
    }
    
    return NO;
}
//	------------------------------------------------------------------------------------------------------------





//	MARK:- Data Validation

//	------------------------------------------------------------------------------------------------------------
-(BOOL)ChangePasswordValidation :(NSString *)password :(NSString *)confirmpassword{
    
    if([self Passwordvalidaton:_oldPassText.text :LocalizedString(@"The Old password must be minimum 8 characters and 1 Uppercase, 1 Lowercase , 1 number , and 1 special characters and maximum 16 characters") ]){
        if([self Passwordvalidaton:password :LocalizedString(@"The password must be minimum 8 characters and 1 Uppercase, 1 Lowercase , 1 number , and 1 special characters and maximum 16 characters ")]){
            //if([self Passwordvalidaton:confirmpassword :LocalizedString(@"The confirm password must be minimum 8 characters and 1 Uppercase, 1 Lowercase , 1 number , and 1 special characters and maximum 16 characters ") ]){
            
                if([self ConfirmPasswordValidation:password withConfirmPassword:confirmpassword]){
                    return  YES;
                }
            //}
        }
    }
    return  NO;
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
-(BOOL)Passwordvalidaton:(NSString *)password :(NSString * )alert{
    
    if(password.length != 0){
        NSString *stricterFilterString = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,15}$";
        NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
        if( [passwordTest evaluateWithObject:password]){
            
            return true;
        }else{
            [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:alert viewController:self okPop:NO];
        }
        
    }else{
        [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:LocalizedString(@"Password is required") viewController:self okPop:NO];
    }
    return false;
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
-(BOOL)ConfirmPasswordValidation :(NSString *)password withConfirmPassword :(NSString *)ConfirmPassword{
    if([password isEqualToString:ConfirmPassword]){
        return YES;
    }else{
        [CSS_Class alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"New password and confirm password must be same") viewController:self okPop:NO];
    }
    return NO;
}
//	------------------------------------------------------------------------------------------------------------





//	============================================================================================================
//	MARK:- Keyboard Notifications
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void) keyboardWillShow: (NSNotification *) notification {
	CGRect keyboardFrame = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	double keyboardDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	
	self.scrollViewBottom.constant = keyboardFrame.size.height;
	NSLog(@"Setting scroll view bottom constraint %f",keyboardFrame.size.height);
	[self.view setNeedsLayout];
	[UIView animateWithDuration:keyboardDuration animations:^{
		[self.view layoutIfNeeded];
	}];
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void) keyboardWillHide: (NSNotification *) notification {
	
	double keyboardDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	self.scrollViewBottom.constant = 0.0;
	[self.view setNeedsLayout];
	[UIView animateWithDuration:keyboardDuration animations:^{
		[self.view layoutIfNeeded];
	}];
}
//	------------------------------------------------------------------------------------------------------------




//	============================================================================================================
//	MARK:- UIScrollViewDelegate
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
	if (scrollView.contentOffset.y >= 0.0) {
		if (scrollView.contentOffset.y <= 44.0) {
			self.largeTitleViewTop.constant = -(scrollView.contentOffset.y);
		}
		else {
			self.largeTitleViewTop.constant = -44.0;
		}
	}
	else {
		self.largeTitleViewTop.constant = 0;
	}
	
	//	Start showing the new title bar label when we have scrolled at least 12 points
	if (scrollView.contentOffset.y > 12) {
		CGFloat alpha = (scrollView.contentOffset.y - 12) / 32.0;
		if (alpha > 1.0) {
			self.titleBarLabel.alpha = 1.0;
		}
		else if (alpha < 0.0) {
			self.titleBarLabel.alpha = 0.0;
		}
		else {
			self.titleBarLabel.alpha = alpha;
		}
	}
	else {
		self.titleBarLabel.alpha = 0.0;
	}
}
//	------------------------------------------------------------------------------------------------------------

@end
