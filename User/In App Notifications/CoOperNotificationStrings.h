//
//  CoOperNotificationStrings.h
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#ifndef CoOperNotificationStrings_h
#define CoOperNotificationStrings_h

extern NSString *const RegisterAccountEmailValidationSuccess;
extern NSString *const LoginRegisterProcessSuccess;
extern NSString *const LoginRegisterProcessFailed;

#endif /* CoOperNotificationStrings_h */


