//
//  CoOperNotifications.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-22.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

struct CoOperNotification {
	static let placeUpdatedNotification:  Notification.Name = Notification.Name("CoOperNotification_placeUpdatedNotification")
	
	static let registerAccountEmailValidationSuccess: Notification.Name = Notification.Name("RegisterAccountEmailValidationSuccessNotification")
	static let loginRegisterProcessSuccess: Notification.Name = Notification.Name("LoginRegisterProcessSuccessNotification")
	static let loginRegisterProcessFailed: Notification.Name = Notification.Name("LoginRegisterProcessFailedNotification")
	
	static let fairServicesFetchSuccess: Notification.Name = Notification.Name("FairServicesFetchSuccessNotification")
	static let fairServicesFetchFailed: Notification.Name = Notification.Name("FairServicesFetchFailedNotification")
}
