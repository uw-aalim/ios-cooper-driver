//
//  CoOperNotificationStrings.m
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoOperNotificationStrings.h"

NSString *const RegisterAccountEmailValidationSuccess = @"RegisterAccountEmailValidationSuccessNotification";
NSString *const LoginRegisterProcessSuccess = @"LoginRegisterProcessSuccessNotification";
NSString *const LoginRegisterProcessFailed = @"LoginRegisterProcessFailedNotification";

