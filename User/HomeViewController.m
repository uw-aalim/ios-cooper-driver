//
//  HomeViewController.m
//  User
//
//  Created by iCOMPUTERS on 12/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "HomeViewController.h"
#import "EmailViewController.h"
#import "WalletViewController.h"
#import "YourTripViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "ViewController.h"
#import "CSS_Class.h"
#import "config.h"
#import "ProfileViewController.h"
#import "UIScrollView+EKKeyboardAvoiding.h"
#import "AFNHelper.h"
#import "Utilities.h"
#import <SocketIO/SocketIO-Swift.h>
#import "RouteViewController.h"
#import "UIImage+animatedGIF.h"
#import "EarningsViewController.h"
#import "HelpViewController.h"
#import "SummaryViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import <Lottie/Lottie.h>
#import "AnimationView.h"
#import "Provider-Swift.h"

@interface HomeViewController ()
{
    CLLocation *myLocation, *CurrentLocation, *StartLocation, *NewLocaton, *OldLocation;
    GMSCameraPosition *lastCameraPosition;
    GMSMarker *endLocationMarker, *startLocationMarker;
    GMSCoordinateBounds *bounds;
    SocketIOClient* socket;
    AVAudioPlayer *audioPlayer;
    NSString *travelStatus, *totalDistance;
    BOOL StartLocationTaken;
    float TotalM, TotalKM;
    UIView * inforView;
    
    NSString * isValid;
    NSString * usrnameStr;
    NSString * passwordStr;
    NSTimer * demoServerCheck;
    NSString * otpStr;

}
@property (nonatomic,retain)UIView*mapV;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeDefaultValues];

    inforView = [[UIView alloc]init];
    [self.view addSubview:inforView];
    
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    

    _rating_user.value = 0.00;
    secondsLeft = 0;
    travelStatus = @"";
    isSchedule = false;
    totalDistance = @"";
    globalStatus = @"";
    scheduleStr = @"false";
    [self getBatteryStatus];
    
    NSURL *url_GIF = [[NSBundle mainBundle] URLForResource:@"driver" withExtension:@"gif"];
    _gifImage.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url_GIF]];
    _gifImage.image = [UIImage animatedImageWithAnimatedGIFURL:url_GIF];
    
    LOTAnimationView *animation = [LOTAnimationView animationNamed:@"search"];
    animation.frame = CGRectMake(_gifImage.frame.origin.x, _gifImage.frame.origin.y, _gifImage.frame.size.width, _gifImage.frame.size.height);
    animation.loopAnimation = YES;
    [self.gifImage addSubview:animation];
    [animation playWithCompletion:^(BOOL animationFinished) {
        // Do Something
    }];
    
    [_gifView setHidden:YES];
    
    [self countdownTimer];
    [self setDesignStyles];
    
    socketConnectFlag = false;
    [self socketConnect];
    
    locationMapped = false;
    gotLocation = false;
    leftmenuFlag = true;
    moveNextLocation = false;
    
    NSURL* url = [[NSURL alloc] initWithString:WEB_SOCKET];
    socket = [[SocketIOClient alloc] initWithSocketURL:url config:@{@"log": @YES,@"forcePolling":@YES}];
    [socket on:@"connect" callback:^(NSArray *data, SocketAckEmitter *ack) {
        NSLog(@"socket connected");
    }];
    
    [self currentLocation];
    //    [_currentLocationBtn setHidden:YES];
    
    [_rateScrollView setContentSize:[_rateScrollView frame].size];
    [_rateScrollView setKeyboardAvoidingEnabled:YES];
    
    _mkap = [[GMSMapView alloc]initWithFrame:_mapView.frame];
    _mkap.myLocationEnabled = YES;
    _mkap.delegate=self;
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"style" withExtension:@"json"];
    NSError *error;
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    _mkap.mapStyle = style;
    [_mapView addSubview:_mkap];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *statusStr = [defaults valueForKey:@"status"];
    [self getStatusMethod:statusStr];
    
    [self.view bringSubviewToFront:_sosBtn];
    [_sosBtn setHidden:YES];
    
    [self getIncomingRequest];
    
    
    [_pickupImg setImage:[UIImage imageNamed:@"pickup"]];
    [_pickupImg setHighlightedImage:[UIImage imageNamed:@"pickup-select"]];
    
    [_arrivedImg setImage:[UIImage imageNamed:@"arrived"]];
    [_arrivedImg setHighlightedImage:[UIImage imageNamed:@"arrived-select"]];
    
    [_finishedImg setImage:[UIImage imageNamed:@"finished"]];
    [_finishedImg setHighlightedImage:[UIImage imageNamed:@"finished-select"]];
    
    self.otpView.hidden = YES;
    
    [self textFieldInterface];
    
}

-(void)textFieldInterface{
    
    [_verfySubmit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
   // [_verfySubmit setBackgroundColor:BASE1Color];
    [_verfySubmit setTitle:NSLocalizedString(@"VERIFYBUTTON", nil) forState:UIControlStateNormal];
    
    _otpView.clipsToBounds = YES;
    _otpView.layer.cornerRadius = 6.0f;
    
    [self.firstTxt addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.secontTxt addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.thirdTxt addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.fourthTxt addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

-(void)initializeDefaultValues{
    
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    
    //SERVICE_URL = [user objectForKey:@"serviceurl"];
    Client_SECRET = [user objectForKey:@"passport"];
    ClientID = [user objectForKey:@"clientid"];
    WEB_SOCKET = [user objectForKey:@"websocket"];
    APP_NAME = [user objectForKey:@"appname"];
    APP_IMG_URL = [user objectForKey:@"appimg"];
    isValid = [user objectForKey:@"is_valid"];
    usrnameStr = [user objectForKey:@"username"];
    passwordStr =  [user objectForKey:@"password"];
    //    GMSMAP_KEY = [user objectForKey:@"mapkey"];
    //    GMSPLACES_KEY = [user objectForKey:@"placekey"];
    //    Stripe_KEY = [user objectForKey:@"stripekey"];
    
}

-(void) getBatteryStatus
{
    UIDevice *myDevice = [UIDevice currentDevice];
    [myDevice setBatteryMonitoringEnabled:YES];
    
    int state = [myDevice batteryState];
    NSLog(@"battery status: %d",state); // 0 unknown, 1 unplegged, 2 charging, 3 full
    
    double batLeft = (float)[myDevice batteryLevel] * 100;
    NSLog(@"battery left: %f", batLeft);
    
    if ((batLeft < 20) && state ==1)
    {
        NSLog(@"you need to charge");
        [self startLocalNotification];
        
    }
    else
    {
        NSLog(@"Need not to charge");
    }
}

-(void)startLocalNotification {  // Bind this method to UIButton action
    NSLog(@"startLocalNotification");
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:7];
    notification.alertBody = @"Your battery power is less to continue, Please plug it to get the request";
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.soundName = UILocalNotificationDefaultSoundName;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

-(void)audioFile
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *filePath = [mainBundle pathForResource:@"alert_tone" ofType:@"mp3"];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    
    NSError *error = nil;
    
    if(audioPlayer == nil){
        [audioPlayer stop];
        audioPlayer = [[AVAudioPlayer alloc] initWithData:fileData error:&error];
        [audioPlayer prepareToPlay];
        [audioPlayer play];
        [audioPlayer setVolume:3];
        [audioPlayer setNumberOfLoops:-1];
    }
    
}

-(void)getStatusMethod:(NSString *)statusStr
{
    MINT_METHOD_TRACE_START
    [_onlineBtn setHidden:NO];
    [_gifView setHidden:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([statusStr isEqualToString:@"onboarding"])
    {
        [_menuBtn setHidden:YES];
        [_menuImgBtn setHidden:YES];
        [_onlineBtn setHidden:YES];
        [self offlineView];
        [_logoutBtn setHidden:NO];
        [_gifView setHidden:NO];
        [_waitingLbl setHidden:NO];
        
        [_currentLocationBtn setHidden:YES];
    }
    else if ([statusStr isEqualToString:@"approved"])
    {
        NSString *activeStr = [Utilities removeNullFromString:[defaults valueForKey:@"Active"]];
        if ([activeStr isEqualToString:@"1"])
        {
            [_onlineBtn setTitle:LocalizedString(@"GO OFFLINE") forState:UIControlStateNormal];
            [self onlineView];
            [_currentLocationBtn setHidden:NO];
        }
        else
        {
            [_onlineBtn setTitle:LocalizedString(@"GO ONLINE") forState:UIControlStateNormal];
            [self offlineView];
           
            [_currentLocationBtn setHidden:YES];
        }
        [_menuBtn setHidden:NO];
        [_menuImgBtn setHidden:NO];
        [_logoutBtn setHidden:YES];
    }
    else
    {
        [_onlineBtn setHidden:YES];
        [_currentLocationBtn setHidden:YES];
        [_navigationBtn setHidden:YES];
    }
    
    leftMenuViewClass = [[[NSBundle mainBundle] loadNibNamed:@"LeftMenuView" owner:self options:nil] objectAtIndex:0];
    [leftMenuViewClass setFrame:CGRectMake(-(self.view.frame.size.width - 100), 0, self.view.frame.size.width - 100, self.view.frame.size.height)];
    
    leftMenuViewClass.LeftMenuViewDelegate =self;
    leftMenuViewClass.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:leftMenuViewClass];
    MINT_NONARC_METHOD_TRACE_STOP
}

-(void)currentLocation
{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.delegate=self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager requestWhenInUseAuthorization];
    [_locationManager requestAlwaysAuthorization];
    [_locationManager startMonitoringSignificantLocationChanges];
    [_locationManager startUpdatingLocation];
}

-(void)getIncomingRequest
{
    if([appDelegate internetConnected])
    {
        params =  @{@"latitude":[NSString stringWithFormat:@"%.8f", myLocation.coordinate.latitude], @"longitude":[NSString stringWithFormat:@"%.8f", myLocation.coordinate.longitude]};
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath_NoLoader:INCOMING_REQUEST withParamData:params withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
         {
             if (response)
             {
                 NSLog(@"response...%@", response);
                 
                 _TrackStatus = [[response[@"requests"] firstObject][@"request"][@"is_track"] boolValue];
                 _WalletStatus = [[response[@"requests"] firstObject][@"request"][@"use_wallet"] boolValue];
                 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                 
                 NSString *account_status = [response valueForKey:@"account_status"];
                 NSString *service_status = [response valueForKey:@"service_status"];
                 [defaults setObject:account_status forKey:@"status"];
                 
                 if ([account_status isEqualToString:@"onboarding"] ||[account_status isEqualToString:@"banned"])
                 {
                     [_onlineBtn setHidden:YES];
                     [_menuImgBtn setHidden:YES];
                     [_menuBtn setHidden:YES];
                     [_logoutBtn setHidden:NO];
                     [self offlineView];
                     [_gifView setHidden:NO];
                 }
                 else if ([account_status isEqualToString:@"approved"])
                 {
                     if ([service_status isEqualToString:@"active"] || [service_status isEqualToString:@"riding"])
                     {
                         [defaults setObject:@"1" forKey:@"Active"];
                         
                         if ([accountStatus isEqualToString:account_status])
                         {
                             //
                         }
                         else
                         {
                             accountStatus = account_status;
                             [self getStatusMethod:account_status];
                         }
                         
                         {
                             if ([response count] == 0)
                             {
                                 //Do Nothing
                             }
                             else
                             {
                                 d_Lng =@"";
                                 d_Lat =@"";
                                 
                                 s_Lat =@"";
                                 s_Lng =@"";
                                 
                                 NSArray *requestsArray = [response valueForKey:@"requests"];
                                 
                                 if ((requestsArray.count ==0)||[response[@"requests"] isKindOfClass:[NSNull class]])
                                 {
                                     locationMapped = false;
                                     moveNextLocation = false;
                                     GMSPolyline *polyline;
                                     polyline.map = nil;
                                     [_mkap clear];
                                     [_navigationBtn setHidden:YES];
                                     globalStatus =@"";
                                     [_sosBtn setHidden:YES];
                                     
                                     startLocationMarker.map=nil;
                                     startLocationMarker=[[GMSMarker alloc]init];
                                     
                                     endLocationMarker.map=nil;
                                     endLocationMarker=[[GMSMarker alloc]init];
                                     
                                     totalDistance =@"";
                                     TotalM = 0;
                                     TotalKM = 0;
                                     
                                     [UIView animateWithDuration:0.45 animations:^{
                                         
                                         _notifyView.frame = CGRectMake(0, (self.view.frame.origin.y+self.view.frame.size.height +_notifyView.frame.size.height), self.view.frame.size.width,  _notifyView.frame.size.height);
                                     }];
                                     
                                     [UIView animateWithDuration:0.45 animations:^{
                                         
                                         _commonRateView.frame = CGRectMake(0, self.view.frame.origin.y+self.view.frame.size.height+_commonRateView.frame.size.height, self.view.frame.size.width,  _commonRateView.frame.size.height);
                                         
                                         _invoiceView.frame = CGRectMake(0, _invoiceView.frame.origin.y, self.view.frame.size.width, _invoiceView.frame.size.height);
                                 _rateViewView.frame = CGRectMake(self.view.frame.origin.x+self.view.frame.size.width+20,0, self.view.frame.size.width, _rateViewView.frame.size.height);
                                     }];
                                     
                                     
                                     if ([scheduleStr isEqualToString:@"true"])
                                     {
                                         scheduleStr  =@"false";
                                         isSchedule = false;
                                         
                                         YourTripViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"YourTripViewController"];
                                         wallet.navigateStr = @"Home";
                                         [self.navigationController pushViewController:wallet animated:YES];
                                     }
                                     else
                                     {
                                         //Nothing
                                     }
                                 }
                                 else
                                 {
                                     
                                     NSDictionary *requestDict = [[requestsArray valueForKey:@"request"]objectAtIndex:0];
                                     otpStr = [NSString stringWithFormat:@"%@",requestDict[@"otp"]];
                                     NSDictionary *userDict = [requestDict valueForKey:@"user"];
                                     
                                     NSString *request_id = [[requestsArray valueForKey:@"request_id"]objectAtIndex:0];
                                     NSString *provider_id = [[requestsArray valueForKey:@"provider_id"]objectAtIndex:0];
                                     
                                     NSString *second=[NSString stringWithFormat:@"%@", [[requestsArray valueForKey:@"time_left_to_respond"]objectAtIndex:0]];
                                     secondsLeft = [second intValue];
                                     
                                     [defaults setValue:request_id forKey:@"request_id"];
                                     [defaults setValue:provider_id forKey:@"provider_id"];
                                     
                                     NSString *status = [requestDict valueForKey:@"status"];
                                     
                                     _invoiceIdLbl.text=[NSString stringWithFormat:@"%@ - %@",LocalizedString(@"INVOICE ID"),[Utilities removeNullFromString:[requestDict valueForKey:@"booking_id"]]];
                                     
                                     if ([globalStatus isEqualToString:status])
                                     {
                                         NSLog(@"Service in same status... Dont reload..!");
                                         
                                         if ([status isEqualToString:@"SEARCHING"])
                                         {
                                             //Do Nothing
                                         }
                                         else
                                         {
                                             [audioPlayer stop];
                                         }
                                     }
                                     else
                                     {
                                         [_sosBtn setHidden:YES];
                                         
                                         globalStatus = @"";
                                         globalStatus = status;
                                         
                                         [_timerView setHidden:YES];
                                         [_statusView setHidden:YES];
                                         [_SKUView setHidden:YES];
                                         
                                         [_pickupImg setHighlighted:NO];
                                         [_arrivedImg setHighlighted:NO];
                                         [_finishedImg setHighlighted:NO];
                                         _pickupDropAddressView.hidden = NO;
                                         [_callBtn setHidden:NO];
                                         
                                         NSString *scheduleString= [Utilities removeNullFromString:[requestDict valueForKey:@"schedule_at"]];
                                         
                                         if ([scheduleString isEqualToString:@""])
                                         {
                                             [_scheduleLbl setHidden:YES];
                                             isSchedule = false;
                                         }
                                         else
                                         {
                                             [_scheduleLbl setHidden:NO];
                                             [_scheduleLbl setText:[NSString stringWithFormat:@"Schedule Request-%@", scheduleString]];
                                             isSchedule = true;
                                         }
                                         
                                         d_Lat= [NSString stringWithFormat: @"%@", [requestDict valueForKey:@"d_latitude"]];
                                         d_Lng= [NSString stringWithFormat: @"%@", [requestDict valueForKey:@"d_longitude"]];
                                         
                                         s_Lat= [NSString stringWithFormat: @"%@", [requestDict valueForKey:@"s_latitude"]];
                                         s_Lng= [NSString stringWithFormat: @"%@", [requestDict valueForKey:@"s_longitude"]];
                                         
                                         NSString *s_address = [NSString stringWithFormat:@"%@", [requestDict valueForKey:@"s_address"]];
                                         NSString *d_address = [NSString stringWithFormat:@"%@", [requestDict valueForKey:@"d_address"]];
                                         [self getPath];
                                         _rating_user.value=[[userDict valueForKey:@"rating"]floatValue];
                                         
                                         s_address = [Utilities removeNullFromString:s_address];
                                         d_address = [Utilities removeNullFromString:d_address];
                                         
                                         _pickUpAddressLbl.text = s_address;
                                         _dropAddressLbl.text = d_address;
                                         
                                         NSString * firstName = [Utilities removeNullFromString: [userDict valueForKey:@"first_name"]];
                                         NSString * lastName = [Utilities removeNullFromString: [userDict valueForKey:@"last_name"]];
                                         
                                         _nameLbl.text = [NSString stringWithFormat:@"%@ %@",firstName, lastName];
                                         _rateNameLbl.text =[NSString stringWithFormat:@"Rate your trip with %@",_nameLbl.text];
                                         mobileStr =[Utilities removeNullFromString: [userDict valueForKey:@"mobile"]];
                                         NSString *imageUrl =[Utilities removeNullFromString: [userDict valueForKey:@"picture"]];
                                         
                                         if (![imageUrl isEqualToString:@""])
                                         {
                                             if ([imageUrl containsString:@"http"])
                                             {
                                                 imageUrl = [NSString stringWithFormat:@"%@",[userDict valueForKey:@"picture"]];
                                             }
                                             else
                                             {
                                                 imageUrl = [NSString stringWithFormat:@"%@/storage/%@",SERVICE_URL, [userDict valueForKey:@"picture"]];
                                             }
                                             
                                             [_userImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                                                         placeholderImage:[UIImage imageNamed:@"user_profile"]];
                                             
                                             [_rateUserImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                                                             placeholderImage:[UIImage imageNamed:@"userProfile"]];
                                         }
                                         else
                                         {
                                             [self.userImg setImage:[UIImage imageNamed:@"user_profile"]];
                                             [self.rateUserImg setImage:[UIImage imageNamed:@"user_profile"]];
                                         }
                                         
                                         if ([status isEqualToString:@"STARTED"] || [status isEqualToString:@"ARRIVED"] ||[status isEqualToString:@"PICKEDUP"] ||[status isEqualToString:@"DROPPED"])
                                         {
                                             
                                             if(socketConnectFlag)
                                             {
                                                 //Already connected
                                             }
                                             else
                                             {
                                                 [socket connect];
                                             }
                                         }
                                         
                                         if ([status isEqualToString:@"SEARCHING"])
                                         {
                                             [_acceptBtn setTitle:LocalizedString(@"ACCEPT") forState:UIControlStateNormal];
                                             [_rejectBtn setTitle:LocalizedString(@"REJECT") forState:UIControlStateNormal];
                                             
                                             if (secondsLeft <= 0 )
                                             {
                                                 //_notifyView.hidden=YES;
                                                 [UIView animateWithDuration:0.45 animations:^{
                                                     
                                     _notifyView.frame = CGRectMake(0, self.view.frame.origin.y+self.view.frame.size.height + _notifyView.frame.size.height, self.view.frame.size.width,  _notifyView.frame.size.height);
                                                 }];
                                             }
                                             else
                                             {
                                                 [self audioFile];
                                                 [UIView animateWithDuration:0.45 animations:^{
                                                     
                                                     _notifyView.frame = CGRectMake(0,  self.view.frame.size.height - _notifyView.frame.size.height, self.view.frame.size.width,  _notifyView.frame.size.height);
                                                     
                                                     if (!leftmenuFlag)
                                                     {
                                                         [self.view bringSubviewToFront:leftMenuViewClass];
                                                     }
                                                     else
                                                     {
                                                         [self.view bringSubviewToFront:_notifyView];
                                                     }
                                                     
                                                 }];
                                             }
                                             [_navigationBtn setHidden:YES];
                                             [_timerView setHidden:NO];
                                             [_statusView setHidden:NO];
                                             [_callBtn setHidden:YES];
                                             
                                         }
                                         else if ([status isEqualToString:@"STARTED"])
                                         {
                                             [_statusView setHidden:NO];
                                             if(audioPlayer != nil){
                                                 [audioPlayer stop];
                                                 audioPlayer = nil;
                                             }
                                             
                                             
                                             [self.view bringSubviewToFront:_navigationBtn];
                                             
                                             [_navigationBtn setHidden:NO];
                                             [_SKUView setHidden:NO];
                                             _pickupDropAddressView.hidden = NO;
                                             
                                             [_acceptBtn setTitle:LocalizedString(@"ARRIVED") forState:UIControlStateNormal];
                                             [_rejectBtn setTitle:LocalizedString(@"CANCEL") forState:UIControlStateNormal];
                                             
                                             [_statusBtn setTitle:LocalizedString(@"ARRIVED") forState:UIControlStateNormal];
                                             [UIView animateWithDuration:0.45 animations:^{
                                                 
                                                 
                                                 
                                                 _notifyView.frame = CGRectMake(0,  self.view.frame.size.height - _notifyView.frame.size.height, self.view.frame.size.width,  _notifyView.frame.size.height);
                                                 
                                                 if (!leftmenuFlag)
                                                 {
                                                     [self.view bringSubviewToFront:leftMenuViewClass];
                                                 }
                                                 else
                                                 {
                                                     [self.view bringSubviewToFront:_notifyView];
                                                 }
                                                 
                                             }];
                                             
                                         }
                                         else if ([status isEqualToString:@"ARRIVED"])
                                         {
                                             [_statusView setHidden:NO];
                                             
                                             [self.view bringSubviewToFront:_navigationBtn];
                                             
                                             [_navigationBtn setHidden:NO];
                                             [_SKUView setHidden:NO];
                                             
                                             [_arrivedImg setHighlighted:YES];
                                             _pickupDropAddressView.hidden = NO;
                                             
                                             [_acceptBtn setTitle:LocalizedString(@"PICKEDUP") forState:UIControlStateNormal];
                                             [_rejectBtn setTitle:LocalizedString(@"CANCEL") forState:UIControlStateNormal];
                                             
                                             [_statusBtn setTitle:LocalizedString(@"PICKEDUP") forState:UIControlStateNormal];
                                             [UIView animateWithDuration:0.45 animations:^{
                                                 
                                       _notifyView.frame = CGRectMake(0,self.view.frame.size.height - _notifyView.frame.size.height, self.view.frame.size.width,  _notifyView.frame.size.height);
                                                 
                                                 if (!leftmenuFlag)
                                                 {
                                                     [self.view bringSubviewToFront:leftMenuViewClass];
                                                 }
                                                 else
                                                 {
                                                     [self.view bringSubviewToFront:_notifyView];
                                                 }
                                             }];
                                            
//                                             s_Lat= [NSString stringWithFormat: @"%@", [requestDict valueForKey:@"s_latitude"]];
//                                             s_Lng= [NSString stringWithFormat: @"%@", [requestDict valueForKey:@"s_longitude"]];
//                                             d_Lat= [NSString stringWithFormat: @"%@", [response valueForKey:@"d_latitude"]];
//                                             d_Lng= [NSString stringWithFormat: @"%@", [response valueForKey:@"d_longitude"]];
//                                             
//                                             [defaults setObject:s_Lat forKey:@"S_LAT"];
//                                             [defaults setObject:s_Lng forKey:@"S_LNG"];
//                                             [defaults setObject:d_Lat forKey:@"D_LAT"];
//                                             [defaults setObject:d_Lng forKey:@"D_LNG"];
                                             [self getPath];
                                             
                                             
                                         }
                                         else if ([status isEqualToString:@"PICKEDUP"])
                                         {
                                             [self.view bringSubviewToFront:_sosBtn];
                                             [_sosBtn setHidden:NO];
                                             
                                             travelStatus = @"driving";
                                             
                                             locationMapped = false;
                                             [_SKUView setHidden:NO];
                                             [_pickupImg setHighlighted:YES];
                                             [_arrivedImg setHighlighted:YES];
                                             _pickupDropAddressView.hidden = NO;
                                             
                                            if(_TrackStatus){
                                        if(_timerForTrackStatus == nil){
                                        
                                        _timerForTrackStatus=[NSTimer scheduledTimerWithTimeInterval:5.0
                                                                                                           target:self
                                                                                                         selector:@selector(GetTrackStatusUpdate)
                                                                                                         userInfo:nil
                                                                                                          repeats:YES];
                                                 }
                                                 
                                             }
                                             [self.view bringSubviewToFront:_navigationBtn];
                                             
                                             [_navigationBtn setHidden:NO];
                                             [_statusBtn setTitle:LocalizedString(@"DROPPED") forState:UIControlStateNormal];
                                             [UIView animateWithDuration:0.45 animations:^{
                                                 
                                                 _notifyView.frame = CGRectMake(0, (self.view.frame.size.height - _notifyView.frame.size.height), self.view.frame.size.width,  _notifyView.frame.size.height);
                                                 
                                                 if (!leftmenuFlag)
                                                 {
                                                     [self.view bringSubviewToFront:leftMenuViewClass];
                                                 }
                                                 else
                                                 {
                                                     [self.view bringSubviewToFront:_notifyView];
                                                 }
                                                 
                                             }];
                                         }
                                         else if ([status isEqualToString:@"DROPPED"])
                                         {
                                             [self.view bringSubviewToFront:_sosBtn];
                                             [_sosBtn setHidden:NO];
                                             
                                             travelStatus = @"stopped";
                                             
                                             [_navigationBtn setHidden:YES];
                                             [_pickupImg setHighlighted:YES];
                                             [_arrivedImg setHighlighted:YES];
                                             [_finishedImg setHighlighted:YES];
                                             _pickupDropAddressView.hidden = NO;
                                             
                                             [_statusBtn setTitle:LocalizedString(@"ARRIVED") forState:UIControlStateNormal];
                                             
                                             [UIView animateWithDuration:0.45 animations:^{
                                                 
                                                 _notifyView.frame = CGRectMake(0, (self.view.frame.origin.y +self.view.frame.size.height +_notifyView.frame.size.height), self.view.frame.size.width,  _notifyView.frame.size.height);
                                             }];
                                             NSString *currency = [defaults valueForKey:@"currency"];
                                             NSDictionary *paymentDict = [requestDict valueForKey:@"payment"];

                                             if (!_WalletStatus)
                                             {
                                                 
//                         hjgjgj
                                                 
                                                 _baseValueLbl.text =[NSString stringWithFormat:@"%@%@", currency,  [paymentDict valueForKey:@"fixed"]];
                                                 _totalValueLbl.text =[NSString stringWithFormat:@"%@%@", currency,  [paymentDict valueForKey:@"total"]];
                                                 _taxValueLbl.text =[NSString stringWithFormat:@"%@%@", currency,  [paymentDict valueForKey:@"tax"]];
                                                 _distanceLbl.text =[NSString stringWithFormat:@"%@%@", currency,  [paymentDict valueForKey:@"distance"]];
                                                 
                                                 
                                                 NSString *wallet=[NSString stringWithFormat:@"%@",[paymentDict valueForKey:@"distance"]];
                                              //   if ([wallet isEqualToString:@"0.00"] || [wallet isEqualToString:@"0"])
                                                //// {
                                                    // [_invoice_WalletAmt setHidden:YES];
                                                    // [_invoice_WalletLbl setHidden:YES];
                                                // }
                                                 //else
                                                // {
//                                                     [_invoice_WalletAmt setHidden:NO];
//                                                     [_invoice_WalletLbl setHidden:NO];
                                                 
                                                     _invoice_WalletAmt.text=[NSString stringWithFormat:@"%@%@",currency,wallet];
                                                // }
                                                 
                                                 NSString *discount=[NSString stringWithFormat:@"%@",[paymentDict valueForKey:@"payable"]];
                                                 [_invoice_discountLbl setHidden:NO];
                                                 [_invoice_discountAmt setHidden:NO];
                                                 _invoice_discountAmt.text=[NSString stringWithFormat:@"%@%@",currency,discount];
                                                 
                                                 if ([discount isEqualToString:@"0.00"] || [discount isEqualToString:@"0"])
                                                 {
                                                     //No discount
                                                     [_invoice_discountLbl setHidden:YES];
                                                     [_invoice_discountAmt setHidden:YES];
                                                 }
                                                 else
                                                 {
                                                     [_invoice_discountLbl setHidden:NO];
                                                     [_invoice_discountAmt setHidden:NO];
                                                     _invoice_discountAmt.text=[NSString stringWithFormat:@"%@%@",currency,discount];
                                                 }
                                                 
                                                 NSString *payMode = [NSString stringWithFormat:@"%@", [requestDict valueForKey:@"payment_mode"]];
                                                 _paymentModeLbl.text =payMode;
                                                 
                                                 if ([payMode isEqualToString:@"CASH"])
                                                 {
                                                     _imgPayment.image = [UIImage imageNamed:@"money_icon"];
                                                 }
                                                 else
                                                 {
                                                     _imgPayment.image = [UIImage imageNamed:@"payment"];
                                                 }
                                                 
                                                 [_paymentBtn setTitle:LocalizedString(@"CONFIRM PAYMENT") forState:UIControlStateNormal];
                                                 [UIView animateWithDuration:0.45 animations:^{
                                                     
                                                     _commonRateView.frame = CGRectMake(0, self.view.frame.origin.y+self.view.frame.size.height - _commonRateView.frame.size.height, self.view.frame.size.width,  _commonRateView.frame.size.height);
                                                     
                                                     _invoiceView.frame = CGRectMake(0, _invoiceView.frame.origin.y, self.view.frame.size.width, _invoiceView.frame.size.height);
                                                     _rateViewView.frame = CGRectMake(self.view.frame.origin.x+self.view.frame.size.width+20,0, self.view.frame.size.width, _rateViewView.frame.size.height);
                                                     
                                                     if (!leftmenuFlag)
                                                     {
                                                         [self.view bringSubviewToFront:leftMenuViewClass];
                                                     }
                                                     else
                                                     {
                                                         [self.view bringSubviewToFront:_commonRateView];
                                                     }
                                                     
                                                 }];
                                             }
                                             else
                                             {
                                                 int paid = [[requestDict valueForKey:@"paid"]intValue];

                                                 if (paid == 0)
                                                 {
                                                     
                                                     _baseValueLbl.text =[NSString stringWithFormat:@"%@%@", currency,  [paymentDict valueForKey:@"fixed"]];
                                                     
                                                     _totalValueLbl.text =[NSString stringWithFormat:@"%@%@", currency,  [paymentDict valueForKey:@"total"]];
                                                     _taxValueLbl.text =[NSString stringWithFormat:@"%@%@", currency,  [paymentDict valueForKey:@"tax"]];
                                                     _distanceLbl.text =[NSString stringWithFormat:@"%@%@", currency,  [paymentDict valueForKey:@"distance"]];

                                                     NSString *wallet=[NSString stringWithFormat:@"%@",[paymentDict valueForKey:@"distance"]];
                                                     
                                                     [_invoice_WalletAmt setHidden:YES];[_invoice_WalletLbl setHidden:YES];

//                                                     [_invoice_WalletAmt setHidden:NO];
//                                                     [_invoice_WalletLbl setHidden:NO];
                                                     
                                                     _invoice_WalletAmt.text=[NSString stringWithFormat:@"%@%@",currency,wallet];
                                                     
                                                     
                                                     NSString *discount=[NSString stringWithFormat:@"%@",[paymentDict valueForKey:@"payable"]];
                                                     [_invoice_discountLbl setHidden:NO];
                                                     [_invoice_discountAmt setHidden:NO];
                                                     _invoice_discountAmt.text=[NSString stringWithFormat:@"%@%@",currency,discount];
                                                     
                                                     if ([discount isEqualToString:@"0.00"] || [discount isEqualToString:@"0"])
                                                     {
                                                         //No discount
                                                         [_invoice_discountLbl setHidden:YES];
                                                         [_invoice_discountAmt setHidden:YES];
                                                     }
                                                     else
                                                     {
                                                         [_invoice_discountLbl setHidden:NO];
                                                         [_invoice_discountAmt setHidden:NO];
                                                         _invoice_discountAmt.text=[NSString stringWithFormat:@"%@%@",currency,discount];
                                                     }
                                                     
                                                     NSString *payMode = [NSString stringWithFormat:@"%@", [requestDict valueForKey:@"payment_mode"]];
                                                     _paymentModeLbl.text =payMode;
                                                     
                                                     if ([payMode isEqualToString:@"CASH"])
                                                     {
                                                         _imgPayment.image = [UIImage imageNamed:@"money_icon"];
                                                     }
                                                     else
                                                     {
                                                         _imgPayment.image = [UIImage imageNamed:@"payment"];
                                                     }
                                                     
                                                     [_paymentBtn setTitle:LocalizedString(@"CONFIRM PAYMENT") forState:UIControlStateNormal];
                                                     [UIView animateWithDuration:0.45 animations:^{
                                                         
                                                         _commonRateView.frame = CGRectMake(0, self.view.frame.origin.y+self.view.frame.size.height - _commonRateView.frame.size.height, self.view.frame.size.width,  _commonRateView.frame.size.height);
                                                         
                                                         _invoiceView.frame = CGRectMake(0, _invoiceView.frame.origin.y, self.view.frame.size.width, _invoiceView.frame.size.height);
                                                         _rateViewView.frame = CGRectMake(self.view.frame.origin.x+self.view.frame.size.width+20,0, self.view.frame.size.width, _rateViewView.frame.size.height);
                                                         
                                                         if (!leftmenuFlag)
                                                         {
                                                             [self.view bringSubviewToFront:leftMenuViewClass];
                                                         }
                                                         else
                                                         {
                                                             [self.view bringSubviewToFront:_commonRateView];
                                                         }
                                                         
                                                     }];
                                                 }
                                                 else
                                                 {
                                                     [UIView animateWithDuration:0.45 animations:^{
                                                         
                                                         _commonRateView.frame = CGRectMake(0, self.view.frame.origin.y+self.view.frame.size.height -_commonRateView.frame.size.height, self.view.frame.size.width,  _commonRateView.frame.size.height);
                                                         
                                                         _invoiceView.frame = CGRectMake( -self.view.frame.origin.x, _invoiceView.frame.origin.y, self.view.frame.size.width, _invoiceView.frame.size.height);
                                                         _rateViewView.frame = CGRectMake(0, 0, self.view.frame.size.width, _rateViewView.frame.size.height);
                                                         
                                                         if (!leftmenuFlag)
                                                         {
                                                             [self.view bringSubviewToFront:leftMenuViewClass];
                                                         }
                                                         else
                                                         {
                                                             [self.view bringSubviewToFront:_commonRateView];
                                                         }
                                                         
                                                     }];
                                                 }
                                             }
                                         }
                                         else if ([status isEqualToString:@"COMPLETED"])
                                         {
                                             [_navigationBtn setHidden:YES];
                                             
                                             [UIView animateWithDuration:0.45 animations:^{
                                                 
                                                 _notifyView.frame = CGRectMake(0, (self.view.frame.size.height +_notifyView.frame.size.height), self.view.frame.size.width,  _notifyView.frame.size.height);
                                             }];
                                             
                                             [UIView animateWithDuration:0.45 animations:^{
                                                 
                                                 _commonRateView.frame = CGRectMake(0, self.view.frame.origin.y+self.view.frame.size.height - _commonRateView.frame.size.height, self.view.frame.size.width,  _commonRateView.frame.size.height);
                                                 
                                                 _invoiceView.frame = CGRectMake( -self.view.frame.origin.x, _invoiceView.frame.origin.y, self.view.frame.size.width, _invoiceView.frame.size.height);
                                                 _rateViewView.frame = CGRectMake(0, 0, self.view.frame.size.width, _rateViewView.frame.size.height);
                                                 
                                                 
                                                 if (!leftmenuFlag)
                                                 {
                                                     [self.view bringSubviewToFront:leftMenuViewClass];
                                                 }
                                                 else
                                                 {
                                                     [self.view bringSubviewToFront:_commonRateView];
                                                 }
                                                 
                                             }];
                                         }
                                         
                                         if ((locationMapped) || (moveNextLocation))
                                         {
                                             //Location mapped already
                                         }
                                         else
                                         {
                                             [self.view bringSubviewToFront:_navigationBtn];
                                             [_navigationBtn setHidden:NO];
                                             
                                             if ([status isEqualToString:@"SEARCHING"] || [status isEqualToString:@"STARTED"] || [status isEqualToString:@"ARRIVED"])
                                             {
                                                 GMSPolyline *polyline;
                                                 polyline.map = nil;
                                                 [_mkap clear];
                                                 
                                                 locationMapped = true;
                                                 switchMapStr = @"Driver";
                                                 
                                                 d_Lat =@"";
                                                 d_Lng = @"";
                                                 
                                                 d_Lat = [NSString stringWithFormat:@"%@",s_Lat];
                                                 d_Lng = [NSString stringWithFormat:@"%@",s_Lng];
                                                 
                                                 s_Lat =  [NSString stringWithFormat:@"%.8f", myLocation.coordinate.latitude];
                                                 s_Lng =  [NSString stringWithFormat:@"%.8f", myLocation.coordinate.longitude];
                                                 
                                                 NSLog(@"%@, %@, %@, %@", s_Lat, s_Lng, d_Lat, d_Lng);
                                             }
                                             else
                                             {
                                                 GMSPolyline *polyline;
                                                 polyline.map = nil;
                                                 [_mkap clear];
                                                 moveNextLocation = true;
                                                 switchMapStr = @"User";
                                             }
                                             
                                             if ((s_Lat == (id)[NSNull null] || s_Lat.length == 0 || [s_Lat isEqualToString:@"<null>"] || [s_Lat isEqualToString:@" "] || [s_Lat isEqualToString:@"<nil>"] || [s_Lat isEqualToString:@""]) || (d_Lat == (id)[NSNull null] || d_Lat.length == 0 || [d_Lat isEqualToString:@"<null>"] || [d_Lat isEqualToString:@" "] || [d_Lat isEqualToString:@"<nil>"] || [d_Lat isEqualToString:@""]))
                                             {
                                                 
                                             }
                                             else
                                             {
                                                 [defaults setObject:s_Lat forKey:@"S_LAT"];
                                                 [defaults setObject:s_Lng forKey:@"S_LNG"];
                                                 
                                                 [defaults setObject:d_Lat forKey:@"D_LAT"];
                                                 [defaults setObject:d_Lng forKey:@"D_LNG"];
                                                 
                                                 bounds = [[GMSCoordinateBounds alloc] init];
                                                 startLocationMarker.map=nil;
                                                 startLocationMarker=[[GMSMarker alloc]init];
                                                 startLocationMarker.position=CLLocationCoordinate2DMake([s_Lat doubleValue], [s_Lng doubleValue]);
                                                 startLocationMarker.icon = [UIImage imageNamed:@"ub__ic_pin_pickup"];
                                                 //startLocationMarker.icon=[self createCustomMarkerImageWithMarker:startLocationMarker WithImagename:@"ub__ic_pin_pickup" :s_address];;
                                                 
                                                 startLocationMarker.groundAnchor=CGPointMake(0.5,0.5);
                                                 startLocationMarker.appearAnimation = kGMSMarkerAnimationPop;
                                                 bounds = [bounds includingCoordinate:startLocationMarker.position];
                                                 
                                                 endLocationMarker.map=nil;
                                                 endLocationMarker=[[GMSMarker alloc]init];
                                                 endLocationMarker.position=CLLocationCoordinate2DMake([d_Lat doubleValue], [d_Lng doubleValue]);
                                                 endLocationMarker.icon = [UIImage imageNamed:@"ub__ic_pin_dropoff"];
                                                // endLocationMarker.icon=[self createCustomMarkerImageWithMarker:startLocationMarker WithImagename:@"ub__ic_pin_dropoff" :d_address];
                                                 endLocationMarker.groundAnchor=CGPointMake(0.5,0.5);
                                                 endLocationMarker.appearAnimation = kGMSMarkerAnimationPop;
                                                 bounds = [bounds includingCoordinate:endLocationMarker.position];
                                                 
                                                 startLocationMarker.map=_mkap;
                                                 endLocationMarker.map=_mkap;
                                                 
                                                 [_mkap animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:80.0f]];
                                                 [self getPath];
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                     else
                     {
                         [defaults setObject:@"0" forKey:@"Active"];
                         [self offlineView];
                         [_logoutBtn setHidden:YES];
                         [_menuBtn setHidden:NO];
                         [_menuImgBtn setHidden:NO];
                         [_onlineBtn setHidden:NO];
                         [_gifView setHidden:YES];
                         [_currentLocationBtn setHidden:YES];
                     }
                 }
             }
             else
             {
                 NSLog(@"%@",error);
                 if ([strErrorCode intValue]==2)
                 {
                     if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                     {
                         //Refresh token
                         [self logoutMethod];
                     }
                     else
                     {
                         [self logoutMethod];
                     }
                 }
             }
         }];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


- (void)getPath
{
    
    NSString *googleUrl = @"https://maps.googleapis.com/maps/api/directions/json";
    
    NSString *urlString = [NSString stringWithFormat:@"%@?origin=%@,%@&destination=%@,%@&sensor=false&waypoints=%@&mode=driving", googleUrl, s_Lat, s_Lng, d_Lat, d_Lng, @""];
    
    NSLog(@"my driving api URL --- %@", urlString);
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error)
      {
          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
          
          NSArray *routesArray = [json objectForKey:@"routes"];
          
          if ([routesArray count] > 0)
          {
              dispatch_async(dispatch_get_main_queue(), ^{
                  GMSPolyline *polyline = nil;
                  [polyline setMap:nil];
                  NSDictionary *routeDict = [routesArray objectAtIndex:0];
                  NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                  NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                  GMSPath *path = [GMSPath pathFromEncodedPath:points];
                  polyline = [GMSPolyline polylineWithPath:path];
                  polyline.strokeWidth = 6.f;
                  //polyline.strokeColor = BASE2Color;
                  //polyline.map = nil;
                  // [polyline.map clear];
                  polyline.map = _mkap;
              });
          }
      }] resume];
    
}

- (void)updateCounter:(NSTimer *)theTimer
{
    if(secondsLeft > 0 ) {
        secondsLeft -- ;
        hours = secondsLeft / 3600;
        minutes = (secondsLeft % 3600) / 60;
        NSString*str= [NSString stringWithFormat:@"%d",secondsLeft];
        _timeLbl.text=str;
    } else {
        secondsLeft = 0;
        [audioPlayer stop];
        audioPlayer=nil;
        
        //        locationMapped = false;
        //        moveNextLocation = false;
        //        GMSPolyline *polyline;
        //        polyline.map = nil;
        //        [_mkap clear];
        //        [_navigationBtn setHidden:YES];
        //
        //        startLocationMarker.map=nil;
        //        startLocationMarker=[[GMSMarker alloc]init];
        //
        //        endLocationMarker.map=nil;
        //        endLocationMarker=[[GMSMarker alloc]init];
        //
        //        totalDistance =@"";
        //        TotalM = 0;
        //        TotalKM = 0;
        //
        //        _notifyView.frame = CGRectMake(0, (self.view.frame.origin.y +self.view.frame.size.height +50), self.view.frame.size.width,  185);
    }
}

-(void)countdownTimer {
    
    secondsLeft = hours = minutes = 0;
    if([_timer isValid]) {
    }
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCounter:) userInfo:nil repeats:YES];
}

- (void)locationManager:(CLLocationManager* )manager didUpdateLocations:(NSArray* )locations
{
    myLocation = (CLLocation *)[locations lastObject];
    
    NSLog(@"lat ...%@", [NSString stringWithFormat:@"%.8f", myLocation.coordinate.latitude]);
    NSLog(@"lat ...%@", [NSString stringWithFormat:@"%.8f", myLocation.coordinate.longitude]);
    
    if (socket.status ==3)
    {
        NSString *strReqID=[[NSUserDefaults standardUserDefaults] valueForKey:@"request_id"];
        NSString *strproID=[[NSUserDefaults standardUserDefaults] valueForKey:@"provider_id"];
        
        [socket emit:@"update location" with:@[@{@"latitude":[NSString stringWithFormat:@"%.8f", myLocation.coordinate.latitude], @"longitude":[NSString stringWithFormat:@"%.8f", myLocation.coordinate.longitude],@"request_id":strReqID,@"provider_id":strproID}]];
    }
    
    if(!gotLocation)
    {
        gotLocation = true;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:myLocation.coordinate.latitude
                                     
                                                                longitude:myLocation.coordinate.longitude
                                                                     zoom:14];
        
        [_mkap animateToCameraPosition:camera];
        [self.view bringSubviewToFront:_currentLocationBtn];
    }
    
    CLLocation *loc = locations.lastObject;
    
    //Distance Calc by every latlong
    CurrentLocation =[[CLLocation alloc] initWithLatitude:loc.coordinate.latitude longitude:loc.coordinate.longitude];
    
    if ([travelStatus isEqualToString:@"driving"])
    {
        if (StartLocationTaken == FALSE)
        {
            StartLocationTaken = TRUE;
            StartLocation = [[CLLocation alloc] initWithLatitude:CurrentLocation.coordinate.latitude longitude:CurrentLocation.coordinate.longitude];
            
            NewLocaton = [[CLLocation alloc] init];
            OldLocation = [[CLLocation alloc] init];
            
            OldLocation = StartLocation;
            
        }
        NewLocaton = CurrentLocation;
        
        if ((NewLocaton.coordinate.latitude == OldLocation.coordinate.latitude) && (NewLocaton.coordinate.longitude == OldLocation.coordinate.longitude))
        {
            NSLog(@"Same location");
        }
        else
        {
            TotalM = TotalM+[NewLocaton distanceFromLocation:OldLocation];
        }
        
        TotalKM = (TotalM / 1000); //Converting Meters to KM
        
        OldLocation = NewLocaton;
        
        totalDistance = @"";
        totalDistance = [NSString stringWithFormat:@"%.2f", TotalKM];
        
        NSLog(@"totalDistance: %@",[NSString stringWithFormat:@"%.2f KM", TotalKM]);
    }
}

-(IBAction)myLocaton:(id)sender
{
    gotLocation = false;
    [self currentLocation];
}
- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture
{
    [self.view bringSubviewToFront:_currentLocationBtn];
    
}

-(void)onlineView
{
    //[self BackgroundPerfomenceStartMethod];
    [self.view bringSubviewToFront:_mapView];
    [self.view bringSubviewToFront:_onlineBtn];
    [self.view bringSubviewToFront:_menuImgBtn];
    [self.view bringSubviewToFront:_menuBtn];
    [self.view bringSubviewToFront:_currentLocationBtn];
    [_navigationBtn setHidden:YES];
    [_sosBtn setHidden:YES];
    
    [_onlineBtn setTitle:LocalizedString(@"OFFLINE") forState:UIControlStateNormal];
}

-(void)offlineView
{
    [self.view bringSubviewToFront:_whiteView];
    [self.view bringSubviewToFront:_onlineBtn];
    [self.view bringSubviewToFront:_menuImgBtn];
    [self.view bringSubviewToFront:_menuBtn];
    [_navigationBtn setHidden:YES];
    [_sosBtn setHidden:YES];
    [_gifView setHidden:YES];
    [_onlineBtn setTitle:LocalizedString(@"ONLINE") forState:UIControlStateNormal];
    //[self BackgroundPerfoemenceStopMethod];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setDesignStyles
{
    MINT_METHOD_TRACE_START
    [CSS_Class APP_Blackbutton2:_onlineBtn];
    
    [CSS_Class APP_Blackbutton:_statusBtn];
    
    [CSS_Class APP_base2Color:_rejectBtn];
    [CSS_Class APP_Blackbutton:_acceptBtn];
    
    [CSS_Class APP_Blackbutton:_paymentBtn];
    [CSS_Class APP_Blackbutton:_submitBtn];
    
    [CSS_Class APP_Blackbutton:_logoutBtn];

    _navigationBtn.layer.shadowRadius = 2.0f;
    _navigationBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _navigationBtn.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    _navigationBtn.layer.shadowOpacity = 0.5f;
    _navigationBtn.layer.masksToBounds = NO;
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_notifyView.frame];
    _notifyView.layer.masksToBounds = NO;
    _notifyView.layer.shadowColor = [UIColor blackColor].CGColor;
    _notifyView.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    _notifyView.layer.shadowOpacity = 1.5f;
    _notifyView.layer.shadowPath = shadowPath.CGPath;
    
    UIBezierPath *shadows = [UIBezierPath bezierPathWithRect:_commonRateView.frame];
    _commonRateView.layer.masksToBounds = NO;
    _commonRateView.layer.shadowColor = [UIColor blackColor].CGColor;
    _commonRateView.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    _commonRateView.layer.shadowOpacity = 1.5f;
    _commonRateView.layer.shadowPath = shadows.CGPath;
    
    UIBezierPath *shadow = [UIBezierPath bezierPathWithRect:_timerView.bounds];
    _timerView.layer.masksToBounds = NO;
    _timerView.layer.shadowColor = [UIColor grayColor].CGColor;
    _timerView.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    _timerView.layer.shadowOpacity = 1.5f;
    _timerView.layer.shadowPath = shadow.CGPath;
    
    _timerView.layer.cornerRadius = _timerView.frame.size.height/2;
    _timerView.clipsToBounds = YES;
    
    _userImg.layer.cornerRadius = _userImg.frame.size.height/2;
    _userImg.clipsToBounds = YES;
    
    _rateUserImg.layer.cornerRadius = _rateUserImg.frame.size.height/2;
    _rateUserImg.clipsToBounds = YES;
    
    [CSS_Class APP_textfield_Outfocus:_commentsText];
    MINT_NONARC_METHOD_TRACE_STOP
}

-(IBAction)onlineBtn:(id)sender
{
    [audioPlayer stop];
    globalStatus =@"";
    NSString *status;
    
    if ([_onlineBtn.titleLabel.text isEqualToString:LocalizedString(@"ONLINE")])
    {
        status = @"active";
        [self getIncomingRequest];
    }
    else
    {
        status = @"offline";
    }
    
    [self updateAvailablity:status];
}

-(void)updateAvailablity:(NSString *)status
{
    MINT_METHOD_TRACE_START
    if([appDelegate internetConnected])
    {
        params=@{@"service_status":status};
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn getDataFromPath:STATUS withParamData:params withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
         {
             if (response)
             {
                 if ([response objectForKey:@"error"])
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[response objectForKey:@"error"] viewController:self okPop:NO];
                 }
                 else
                 {
                     //Success
                     NSLog(@"service_status ...%@", response);
                     NSString *first_name = [response valueForKey:@"first_name"];
                     NSString *last_name = [response valueForKey:@"last_name"];
                     
                     NSString *avatar =[Utilities removeNullFromString:[response valueForKey:@"avatar"]];
                     NSString *status =[Utilities removeNullFromString:[response valueForKey:@"status"]];
                     
                     NSDictionary *dict = [response valueForKey:@"service"];
                     NSString *serviceStatus = [dict valueForKey:@"status"];
                     
                     if ([serviceStatus isEqualToString:@"active"])
                     {
                         [_mapView setHidden:NO];
                         [_whiteView setHidden:YES];
                         [_onlineBtn setTitle:LocalizedString(@"OFFLINE") forState:UIControlStateNormal];
                         [self onlineView];
                     }
                     else
                     {
                         [_mapView setHidden:YES];
                         [_whiteView setHidden:NO];
                         [_onlineBtn setTitle:LocalizedString(@"ONLINE") forState:UIControlStateNormal];
                         [self offlineView];
                     }
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setObject:first_name forKey:@"first_name"];
                     [defaults setObject:last_name forKey:@"last_name"];
                     [defaults setObject:avatar forKey:@"avatar"];
                     [defaults setObject:status forKey:@"status"];
                 }
                 
             }
             else
             {
                 if ([strErrorCode intValue]==1)
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
                 else if ([strErrorCode intValue]==2)
                 {
                     if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                     {
                         //Refresh token
                     }
                     else
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[error valueForKey:@"error"]  viewController:self okPop:NO];
                     }
                 }
                 else if ([strErrorCode intValue]==3)
                 {
                     if ([error objectForKey:@"email"]) {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                     }
                     else if ([error objectForKey:@"password"]) {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"password"] objectAtIndex:0]  viewController:self okPop:NO];
                     }
                     else
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG")  viewController:self okPop:NO];
                     }
                 }
                 else
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG")  viewController:self okPop:NO];
                 }
                 NSLog(@"%@",error);
                 
             }
         }];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    MINT_NONARC_METHOD_TRACE_STOP
}

-(IBAction)menuBtn:(id)sender
{
    if (leftmenuFlag)
    {
        leftmenuFlag = false;
        [leftMenuViewClass setHidden:NO];
        [self LeftMenuView];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
 
    
    [self BackgroundPerfomenceStartMethod];
//    UITapGestureRecognizer *tapGesture_condition=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ViewOuterTap)];
//    tapGesture_condition.cancelsTouchesInView=NO;
//    tapGesture_condition.delegate=self;
//    [self.view addGestureRecognizer:tapGesture_condition];
    
    leftMenuViewClass = [[[NSBundle mainBundle] loadNibNamed:@"LeftMenuView" owner:self options:nil] objectAtIndex:0];
    [leftMenuViewClass setFrame:CGRectMake(-([UIScreen mainScreen].bounds.size.width - 100), 0, self.view.frame.size.width - 100, self.view.frame.size.height)];
    
    leftMenuViewClass.LeftMenuViewDelegate =self;
    leftMenuViewClass.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:leftMenuViewClass];
    
    [leftMenuViewClass setHidden:YES];
    
    [self setDesignStyles];
}

-(void)BackgroundPerfomenceStartMethod{
    if(_timerForReqProgress == nil){
        _timerForReqProgress=[NSTimer scheduledTimerWithTimeInterval:5.0
                                                              target:self
                                                            selector:@selector(getIncomingRequest)
                                                            userInfo:nil
                                                             repeats:YES];
    }
    
}

-(void)LeftMenuView
{
    if(leftMenuViewClass.frame.origin.x == -([UIScreen mainScreen].bounds.size.width - 100)){
        [UIView animateWithDuration:0.3 animations:^{
            
            leftMenuViewClass.frame = CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width - 100,  self.view.frame.size.height);
        }];
        [self BackgroundPerfoemenceStopMethod];
        waitingBGView = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width  ,self.view.frame.size.height)];
        UITapGestureRecognizer *tapGesture_condition=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ViewOuterTap)];
        tapGesture_condition.cancelsTouchesInView=NO;
        tapGesture_condition.delegate=self;
        [waitingBGView addGestureRecognizer:tapGesture_condition];
        [waitingBGView setBackgroundColor:[UIColor blackColor]];
        [waitingBGView setAlpha:0.6];
        [self.view addSubview:waitingBGView];
        [self.view bringSubviewToFront:leftMenuViewClass];
        
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            
            leftMenuViewClass.frame = CGRectMake(-([UIScreen mainScreen].bounds.size.width - 100), 0, self.view.frame.size.width - 100,  self.view.frame.size.height);
            
        }];
        [self BackgroundPerfomenceStartMethod];
        leftmenuFlag = true;
        [waitingBGView removeFromSuperview];
    }
    

    
}

- (void)ViewOuterTap
{

    [self LeftMenuView];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer* )gestureRecognizer shouldReceiveTouch:(UITouch* )touch
{
    if ([touch.view isDescendantOfView:leftMenuViewClass])
    {
        return NO;
    }
    return YES;
}

-(void)yourTripsView
{
    [self ViewOuterTap];
    YourTripViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"YourTripViewController"];
    [self.navigationController pushViewController:wallet animated:YES];
}
-(void)summaryView
{
    [self ViewOuterTap];
    SummaryViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"SummaryViewController"];
    [self.navigationController pushViewController:wallet animated:YES];
}

-(void)helpView
{
    [self ViewOuterTap];
    HelpViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    [self.navigationController pushViewController:wallet animated:YES];

}

-(void)Settings{
    [self ViewOuterTap];
	NSLog(@"Use new settings view controller");
}
-(void)shareView
{
    [self ViewOuterTap];
    
    UIImage *img = [UIImage imageNamed:@"icon"];
    NSMutableArray *sharingItems = [NSMutableArray new];
    [sharingItems addObject:@"coOper DRIVER"];
    [sharingItems addObject:img];
    [sharingItems addObject:[NSURL URLWithString:@"https://itunes.apple.com/us/app/coOper-driver/id1204269279?mt=8"]];
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}

-(void)profileView
{
    [self ViewOuterTap];
    ProfileViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    [self.navigationController pushViewController:wallet animated:YES];
}

-(void)earningsView
{
    [self ViewOuterTap];
    EarningsViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"EarningsViewController"];
    [self.navigationController pushViewController:wallet animated:YES];
}

-(void)logOut
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"Are you sure want to logout?") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction actionWithTitle:LocalizedString(@"NO") style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self logoutMethod_FromApp];
        
    }];
    [alertController addAction:ok];
    [alertController addAction:no];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)logoutMethod
{
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn refreshMethod_NoLoader:
     @""withBlock:^(id responseObject, NSDictionary *error, NSString *errorcode) {
         
         if (responseObject)
         {
             NSLog(@"Refresh Method ...%@", responseObject);
             NSString *access_token = [responseObject valueForKey:@"access_token"];
             NSString *first_name = [responseObject valueForKey:@"first_name"];
             NSString *avatar =[Utilities removeNullFromString:[responseObject valueForKey:@"avatar"]];
             NSString *status =[Utilities removeNullFromString:[responseObject valueForKey:@"status"]];
             NSString *currencyStr=[responseObject valueForKey:@"currency"];
             NSString *socialId=[Utilities removeNullFromString:[responseObject valueForKey:@"social_unique_id"]];
             NSString *last_name = [responseObject valueForKey:@"last_name"];
             
             
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setObject:last_name forKey:@"last_name"];
             [defaults setObject:access_token forKey:@"access_token"];
             [defaults setObject:first_name forKey:@"first_name"];
             [defaults setObject:avatar forKey:@"avatar"];
             [defaults setObject:status forKey:@"status"];
             [defaults setValue:currencyStr forKey:@"currency"];
             [defaults setValue:socialId forKey:@"social_unique_id"];
             [defaults setObject:[responseObject valueForKey:@"id"] forKey:@"id"];
             [defaults setObject:[responseObject valueForKey:@"sos"] forKey:@"sos"];
         }
         else
         {
             [self ViewOuterTap];
             [socket disconnect];
			 
			 /*
             [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
             PageViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
             [self.navigationController pushViewController:wallet animated:YES]; */
			 WelcomeViewController *viewController = [WelcomeViewController initController];
			 UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
			 [self.navigationController presentViewController:navigationController animated:YES completion:^{
				 NSLog(@"Presented welcome view controller");
			 }];
         }
     }];
}

-(void)logoutMethod_FromApp
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *idStr = [defaults valueForKey:@"id"];
        NSDictionary *param = @{@"id":idStr};
        
        NSString *url = [NSString stringWithFormat:@"/api/provider/logout"];
        [afn getDataFromPath_NoLoader:url withParamData:param withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                [self ViewOuterTap];
                [socket disconnect];
                
                [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
				/*
                PageViewController * wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];

                [self.navigationController pushViewController:wallet animated:YES];*/
				WelcomeViewController *viewController = [WelcomeViewController initController];
				UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
				[self.navigationController presentViewController:navigationController animated:YES completion:^{
					NSLog(@"Presented welcome view controller");
				}];
                
            }
            else
            {
                [CSS_Class alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CSS_Class alertviewController_title:LocalizedString(@"Alert") MessageAlert:NSLocalizedString(@"CHKNET", nil) viewController:self okPop:NO];
    }
    
}

-(IBAction)acceptBtn:(id)sender
{
    [audioPlayer stop];
    audioPlayer=nil;
    
    MINT_METHOD_TRACE_START
    
    if ([_acceptBtn.currentTitle isEqualToString:LocalizedString(@"ARRIVED")])
    {
        [_statusBtn setTitle:LocalizedString(@"ARRIVED") forState:UIControlStateNormal];
        [self statusBtnAction:self];
    }
    else if ([_acceptBtn.currentTitle isEqualToString:LocalizedString(@"PICKEDUP")])
    {
        
//        [self.view bringSubviewToFront:_otpView];
        [self.view bringSubviewToFront:_bgView];
        [AnimationView alertViewAnimation:_otpView bgView:_bgView];
//        _bgView.hidden = NO;
//        _otpView.hidden = NO;
    }
    else if ([_acceptBtn.currentTitle isEqualToString:LocalizedString(@"ACCEPT")])
    {
        if([appDelegate internetConnected])
        {
            if (isSchedule)
            {
                scheduleStr = @"true";
            }
            else
            {
                scheduleStr = @"false";
            }
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *request_ID = [defaults valueForKey:@"request_id"];
            
            NSString *url = [NSString stringWithFormat: @"/api/provider/trip/%@", request_ID];
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:url  withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
             {
                 if (response)
                 {
                     NSLog(@"RESPONSE ...%@", response);
                     
                     if ([scheduleStr isEqualToString:@"true"])
                     {
                         [_statusView setHidden:NO];
                     }
                     else
                     {
                         //                         [_statusView setHidden:YES];
                     }
                     [self getIncomingRequest];
                     
                     [_timerView setHidden:YES];
                 }
                 else
                 {
                     if ([strErrorCode intValue]==1)
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                     else if ([strErrorCode intValue]==2)
                     {
                         if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                         {
                             //Refresh token
                         }
                         else
                         {
                             [self logoutMethod];
                         }
                     }
                     else if ([strErrorCode intValue]==3)
                     {
                         if ([error objectForKey:@"password"]) {
                             [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                         }
                         else
                         {
                             [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                         }
                     }
                     else
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                     NSLog(@"%@",error);
                     
                 }
             }];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    else
    {
        //Nothing to call
    }
}

-(IBAction)rejectBtn:(id)sender
{
    [audioPlayer stop];
    audioPlayer=nil;
    
    if ([_rejectBtn.currentTitle isEqualToString:LocalizedString(@"CANCEL")])
    {
        [self cancelBtn:self];
    }
    else
    {
        
        MINT_METHOD_TRACE_START
        if([appDelegate internetConnected])
        {
            scheduleStr = @"false";
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *request_ID = [defaults valueForKey:@"request_id"];
            
            NSString *url = [NSString stringWithFormat: @"%@/api/provider/trip/%@", SERVICE_URL, request_ID];
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:DELETE_METHOD];
            [afn getDataFromPath:url  withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
             {
                 if (response)
                 {
                     NSLog(@"RESPONSE ...%@", response);
                     
                     GMSPolyline *polyline;
                     polyline.map = nil;
                     [_mkap clear];
                     
                     startLocationMarker.map=nil;
                     startLocationMarker=[[GMSMarker alloc]init];
                     
                     endLocationMarker.map=nil;
                     endLocationMarker=[[GMSMarker alloc]init];
                     
                     
                     
                     [UIView animateWithDuration:0.45 animations:^{
                         
                         //                         _notifyView.frame = CGRectMake(0, (self.view.frame.origin.y +self.view.frame.size.height +50), self.view.frame.size.width,  185);
                         _notifyView.frame = CGRectMake(0,self.view.frame.size.height +_notifyView.frame.size.height, self.view.frame.size.width,  _notifyView.frame.size.height);
                         [self.view sendSubviewToBack:_notifyView];
                     }];
                 }
                 else
                 {
                     if ([strErrorCode intValue]==1)
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                     else if ([strErrorCode intValue]==2)
                     {
                         if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                         {
                             //Refresh token
                         }
                         else
                         {
                             [self logoutMethod];
                         }
                     }
                     else if ([strErrorCode intValue]==3)
                     {
                         if ([error objectForKey:@"password"]) {
                             [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                         }
                         else
                         {
                             [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                         }
                     }
                     else
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                     NSLog(@"%@",error);
                     
                 }
             }];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString
                                 (@"OK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        MINT_NONARC_METHOD_TRACE_STOP
    }
}

-(IBAction)callBtn:(id)sender
{
    if ([mobileStr isEqualToString:@""])
    {
        [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:LocalizedString(@"User was not gave the mobile number") viewController:self okPop:NO];
    }
    else
    {
        NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:mobileStr]];
        NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:mobileStr]];
        
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl])
        {
            [UIApplication.sharedApplication openURL:phoneUrl options:@{} completionHandler:nil];
        }
        else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl])
        {
            [UIApplication.sharedApplication openURL:phoneFallbackUrl options:@{} completionHandler:nil];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CALL") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];    }
    }}

- (IBAction)statusBtnAction:(id)sender
{
    MINT_METHOD_TRACE_START
    if ([_statusBtn.currentTitle isEqualToString:LocalizedString(@"ARRIVED")])
    {
        [self updateStatus:@"ARRIVED"];
    }
    else if ([_statusBtn.currentTitle isEqualToString:LocalizedString(@"PICKEDUP")])
    {
        [self updateStatus:@"PICKEDUP"];
        if(_TrackStatus){
            if(_timerForTrackStatus == nil){
                _timerForTrackStatus=[NSTimer scheduledTimerWithTimeInterval:5.0
                                                                      target:self
                                                                    selector:@selector(GetTrackStatusUpdate)
                                                                    userInfo:nil
                                                                     repeats:YES];
            }
            
        }
       
    }
    else if ([_statusBtn.currentTitle isEqualToString:LocalizedString(@"DROPPED")])
    {
        [self updateStatus:@"DROPPED"];
        [_timerForTrackStatus invalidate];
        _timerForTrackStatus = nil;
    }
    else if ([_statusBtn.currentTitle isEqualToString:LocalizedString(@"CONFIRM PAYMENT")])
    {
        [self updateStatus:@"COMPLETED"];
    }
}

-(void)GetTrackStatusUpdate{
    MINT_METHOD_TRACE_START
    if([appDelegate internetConnected])
    {
        
        CLGeocoder *ceo = [[CLGeocoder alloc]init];
        [ceo reverseGeocodeLocation:CurrentLocation
                  completionHandler:^(NSArray *placemarks, NSError *error) {
                      CLPlacemark *placemark = [placemarks objectAtIndex:0];
                      if (placemark)
                      {
                          NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                          
                          NSLog(@"I am currently at %@",locatedAt);
                          locationString = locatedAt;
                          
                         
                          params = @{@"latitude":[NSString stringWithFormat:@"%.8f", CurrentLocation.coordinate.latitude], @"longitude": [NSString stringWithFormat:@"%.8f", CurrentLocation.coordinate.longitude]};
                          
                          [self updateStatuTrack:params];
                      }
                      else {
                          NSLog(@"Could not locate");
                          locationString = @"";
                      }
                  }
         ];
        
    }
}
-(void)updateStatuTrack :(NSDictionary *)paramms{
    
    MINT_METHOD_TRACE_START
    if([appDelegate internetConnected])
    {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *request_ID = [defaults valueForKey:@"request_id"];
        
        NSString *url = [NSString stringWithFormat: @"/api/provider/trip/%@/calculate", request_ID];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath_NoLoader:url  withParamData:paramms withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
         {
             if (response)
             {
                 d_Lat= [NSString stringWithFormat: @"%@", [response valueForKey:@"d_latitude"]];
                 d_Lng= [NSString stringWithFormat: @"%@", [response valueForKey:@"d_longitude"]];
                 [defaults setObject:d_Lat forKey:@"D_LAT"];
                 [defaults setObject:d_Lng forKey:@"D_LNG"];
                 [self getPath];
                 
                 
                 //                 NSLog(@"RESPONSE ...%@", response);
                 //
                 //                 GMSPolyline *polyline;
                 //                 polyline.map = nil;
                 //                 [_mkap clear];
                 //
                 //                 startLocationMarker.map=nil;
                 //                 startLocationMarker=[[GMSMarker alloc]init];
                 //
                 //                 endLocationMarker.map=nil;
                 //                 endLocationMarker=[[GMSMarker alloc]init];
                 //
                 //                 [UIView animateWithDuration:0.45 animations:^{
                 //
                 //                     //                         _notifyView.frame = CGRectMake(0, (self.view.frame.origin.y +self.view.frame.size.height +50), self.view.frame.size.width,  185);
                 //                     _notifyView.frame = CGRectMake(0, (self.view.frame.origin.y +self.view.frame.size.height +50), self.view.frame.size.width,  230);
                 //
                 //                 }];
             }
             else
             {
                 if ([strErrorCode intValue]==1)
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
                 else if ([strErrorCode intValue]==2)
                 {
                     if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                     {
                         //Refresh token
                     }
                     else
                     {
                         [self logoutMethod];
                     }
                 }
                 else if ([strErrorCode intValue]==3)
                 {
                     if ([error objectForKey:@"password"]) {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                     }
                     else
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                 }
                 else
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
                 NSLog(@"%@",error);
                 
             }
         }];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    MINT_NONARC_METHOD_TRACE_STOP
    
}



-(void)updateStatus:(NSString *)status
{
    if([appDelegate internetConnected])
    {
        if ([status isEqualToString:@"DROPPED"])
        {
            CLGeocoder *ceo = [[CLGeocoder alloc]init];
            [ceo reverseGeocodeLocation:CurrentLocation
                      completionHandler:^(NSArray *placemarks, NSError *error) {
                          CLPlacemark *placemark = [placemarks objectAtIndex:0];
                          if (placemark)
                          {
                              NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                              
                              NSLog(@"I am currently at %@",locatedAt);
                              locationString = locatedAt;
                              
                              if(_TrackStatus){
                                params = @{@"status":status,@"address":locationString ,@"_method":@"PATCH"};
                              }else{
                                params = @{@"status":status ,@"_method":@"PATCH"};
                              }
                              
                              [self statusUpdateCompleteMethod:params];
                          }
                          else {
                              NSLog(@"Could not locate");
                              locationString = @"";
                          }
                      }
             ];
        }
        else
        {
            params = @{@"status":status};
            [self statusUpdateMethod:params];
        }
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)statusUpdateMethod:(NSDictionary*)parameter
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *request_ID = [defaults valueForKey:@"request_id"];
    
    NSString *url = [NSString stringWithFormat: @"%@/api/provider/trip/%@", SERVICE_URL, request_ID];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:PATCH_METHOD];
    [afn getDataFromPath:url  withParamData:parameter withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
     {
         if (response)
         {
             NSLog(@"RESPONSE ...%@", response);
             
             
             [self getIncomingRequest];
         }
         else
         {
             if ([strErrorCode intValue]==1)
             {
                 [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
             }
             else if ([strErrorCode intValue]==2)
             {
                 if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                 {
                     //Refresh token
                 }
                 else
                 {
                     [self logoutMethod];
                 }
             }
             else if ([strErrorCode intValue]==3)
             {
                 if ([error objectForKey:@"password"]) {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                 }
                 else
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
             }
             else
             {
                 [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
             }
             NSLog(@"%@",error);
             
         }
     }];
}



-(void)statusUpdateCompleteMethod:(NSDictionary*)parameter
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *request_ID = [defaults valueForKey:@"request_id"];
    
    NSString *url = [NSString stringWithFormat: @"/api/provider/trip/%@", request_ID];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:url  withParamData:parameter withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
     {
         if (response)
         {
             NSLog(@"RESPONSE ...%@", response);
             [self getIncomingRequest];
         }
         else
         {
             if ([strErrorCode intValue]==1)
             {
                // [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
             }
             else if ([strErrorCode intValue]==2)
             {
                 if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                 {
                     //Refresh token
                 }
                 else
                 {
                     [self logoutMethod];
                 }
             }
             else if ([strErrorCode intValue]==3)
             {
                 if ([error objectForKey:@"password"]) {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                 }
                 else
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
             }
             else
             {
                 [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
             }
             NSLog(@"%@",error);
             
         }
     }];
}


- (IBAction)paymentBtnAction:(id)sender
{
    if ([[_paymentBtn titleForState:UIControlStateNormal] isEqualToString:LocalizedString(@"CONFIRM PAYMENT")])
    {
        [self updateStatus:@"COMPLETED"];
    }
    else
    {
        //Waiting for payment from user
    }
}
- (IBAction)submitBtnAction:(id)sender
{
    MINT_METHOD_TRACE_START
    if([appDelegate internetConnected])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *request_ID = [defaults valueForKey:@"request_id"];
        
        NSInteger num=(NSUInteger)(floor(_rating.value));
        NSString*rat=[NSString stringWithFormat:@"%ld",num];
        
        params = @{@"rating":rat,@"comment":_commentsText.text};
        
        NSString *url = [NSString stringWithFormat: @"/api/provider/trip/%@/rate", request_ID];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:url  withParamData:params withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
         {
             if (response)
             {
                 NSLog(@"RESPONSE ...%@", response);
                 _commentsText.text = @"";
                 _rating.value = 0.00;
                 
                 [UIView animateWithDuration:0.45 animations:^{
                     
                     _commonRateView.frame = CGRectMake(0,self.view.frame.origin.y+self.view.frame.size.height+_commonRateView.frame.size.height, self.view.frame.size.width,  _commonRateView.frame.size.height);
                     
                     _invoiceView.frame = CGRectMake(0, _invoiceView.frame.origin.y, self.view.frame.size.width, _invoiceView.frame.size.height);
                     _rateViewView.frame = CGRectMake(self.view.frame.origin.x+self.view.frame.size.width+20, 0, self.view.frame.size.width, _rateViewView.frame.size.height);
                     
                 }];
                 [self myLocaton:self];
                 [self getIncomingRequest];
             }
             else
             {
                 if ([strErrorCode intValue]==1)
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
                 else if ([strErrorCode intValue]==2)
                 {
                     if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                     {
                         //Refresh token
                     }
                     else
                     {
                         [self logoutMethod];
                     }
                 }
                 else if ([strErrorCode intValue]==3)
                 {
                     if ([error objectForKey:@"password"]) {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                     }
                     else
                     {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                 }
                 else
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
                 NSLog(@"%@",error);
                 
             }
         }];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    MINT_NONARC_METHOD_TRACE_STOP
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_commentsText)
    {
        [_commentsText resignFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [CSS_Class APP_textfield_Infocus:textField];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [CSS_Class APP_textfield_Outfocus:textField];
    return YES;
}

-(UIImage*)createCustomMarkerImageWithMarker:(GMSMarker *)marker WithImagename:(NSString *)imagename :(NSString *)title
{
    
    inforView.frame = CGRectMake(0, -10, 150, 71);
    inforView.backgroundColor = [UIColor clearColor];
    //    //CGRect priceLabelRect = [marker.title boundingRectWithSize:CGSizeMake(500, 50)
    //                                                       options:NSStringDrawingUsesLineFragmentOrigin
    //                                                    attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10]}
    //                                                       context:nil];
    
    UILabel *addressLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0,inforView.frame.size.width-20,50)];
    addressLbl.text = [NSString stringWithFormat:@"%@",title];
    addressLbl.textAlignment = NSTextAlignmentCenter;
    addressLbl.textColor = [UIColor blackColor];
    addressLbl.backgroundColor = [UIColor whiteColor];
    addressLbl.layer.shadowOffset = CGSizeMake(0, 0);
    addressLbl.layer.shadowOpacity = 0.5;
    addressLbl.font = [UIFont systemFontOfSize:11];
    [inforView addSubview:addressLbl];
    UIView * bgView = [[UIView alloc]initWithFrame:CGRectMake(addressLbl.frame.size.width, 0, inforView.frame.size.width-addressLbl.frame.size.width, addressLbl.frame.size.height)];
    bgView.backgroundColor= [UIColor blackColor];
    [inforView addSubview:bgView];
    
    UIImageView * image = [[UIImageView alloc]initWithFrame:CGRectMake(inforView.frame.size.width/2 - 10.5, addressLbl.frame.origin.y + addressLbl.frame.size.height+1, 21, 21)];
    image.image =[UIImage imageNamed:imagename];
    [inforView addSubview:image];
    
    UIGraphicsBeginImageContextWithOptions(inforView.bounds.size, NO, [[UIScreen mainScreen] scale]);
    [inforView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * icon = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return icon;
}


#pragma SOCKET

-(void)socketConnect
{
    MINT_METHOD_TRACE_START
    
    NSURL* url = [[NSURL alloc] initWithString:WEB_SOCKET];
    socket = [[SocketIOClient alloc] initWithSocketURL:url config:@{@"log": @YES,@"forcePolling":@YES}];
    
    [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSLog(@"socket connected");
        socketConnectFlag = true;
    }];
    
    [socket on:@"disconnect" callback:^(NSArray *data, SocketAckEmitter *ack) {
        NSLog(@"disconnect");
        socketConnectFlag = false;
    }];
    
    MINT_NONARC_METHOD_TRACE_STOP
}

-(IBAction)navigationBtn:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *source_Lat = [defaults valueForKey:@"S_LAT"];
    NSString *source_Lng = [defaults valueForKey:@"S_LNG"];
    
    NSString *des_Lat = [defaults valueForKey:@"D_LAT"];
    NSString *des_Lng = [defaults valueForKey:@"D_LNG"];
    
    NSLog(@"%@, %@, %@, %@", source_Lat, source_Lng, des_Lat, des_Lng);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=%@,%@&daddr=%@,%@&dirflg=r",source_Lat, source_Lng, des_Lat, des_Lng]];
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://?"]])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        [CSS_Class alertviewController_title:LocalizedString(@"Alert!")  MessageAlert:LocalizedString(@"Please download google maps for navigation") viewController:self okPop:NO];
    }
}

-(void)calculationUpdateMethod
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *request_ID = [defaults valueForKey:@"request_id"];
    
    NSString *url = [NSString stringWithFormat: @"/api/provider/trip/%@/calculate", request_ID];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    
    NSDictionary *parameter = @{@"latitude":[NSString stringWithFormat:@"%.8f", CurrentLocation.coordinate.latitude], @"longitude": [NSString stringWithFormat:@"%.8f", CurrentLocation.coordinate.longitude]};
    
    [afn getDataFromPath_NoLoader:url withParamData:parameter withBlock:^(id response, NSDictionary *error, NSString *strErrorCode) {
        
        if (response)
        {
            NSLog(@"Calculate RESPONSE ...%@", response);
        }
        else
        {
            if ([strErrorCode intValue]==1)
            {
                [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
            else if ([strErrorCode intValue]==2)
            {
                if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                {
                    //Refresh token
                }
                else
                {
                    [self logoutMethod];
                }
            }
            else if ([strErrorCode intValue]==3)
            {
                if ([error objectForKey:@"password"]) {
                    [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                }
                else
                {
                    [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
            }
            else
            {
                [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
            NSLog(@"%@",error);
            
        }
    }];
}


-(IBAction)cancelBtn:(id)sender
{
    if([appDelegate internetConnected])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *request_ID = [defaults valueForKey:@"request_id"];
        
        NSString *url = [NSString stringWithFormat: @"/api/provider/cancel"];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        NSDictionary *param = @{@"id":request_ID};
        [afn getDataFromPath:url  withParamData:param withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
         {
             if (response)
             {
                 NSLog(@"CANCEL RESPONSE ...%@", response);
                 
                 GMSPolyline *polyline;
                 polyline.map = nil;
                 [_mkap clear];
                 
                 startLocationMarker.map=nil;
                 startLocationMarker=[[GMSMarker alloc]init];
                 
                 endLocationMarker.map=nil;
                 endLocationMarker=[[GMSMarker alloc]init];
                 
                 [UIView animateWithDuration:0.45 animations:^{
                     
                     _notifyView.frame = CGRectMake(0, self.view.frame.origin.y+self.view.frame.size.height+_notifyView.frame.size.height, self.view.frame.size.width,  _notifyView.frame.size.height);
                     NSLog(@"%@",_notifyView);
                  [self.view sendSubviewToBack:_notifyView];
                 }];
             }
             else
             {
                 if ([strErrorCode intValue]==2)
                 {
                     if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                     {
                         //Refresh token
                     }
                     else
                     {
                         [self logoutMethod];
                     }
                 }
                 else {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR")  MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
                 NSLog(@"%@",error);
                 
             }
         }];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    MINT_NONARC_METHOD_TRACE_STOP
}

-(IBAction)sosBtnAction:(id)sender
{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *sosNumber = [Utilities removeNullFromString:[def valueForKey:@"sos"]];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"Are you sure want to Call Emergency?") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction actionWithTitle:LocalizedString(@"NO") style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([sosNumber isEqualToString:@""])
        {
            //No SOS number was provided
        }
        else
        {
            NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:sosNumber]];
            NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:sosNumber]];
            
            if ([UIApplication.sharedApplication canOpenURL:phoneUrl])
            {
                [UIApplication.sharedApplication openURL:phoneUrl options:@{} completionHandler:nil];
            }
            else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl])
            {
                [UIApplication.sharedApplication openURL:phoneFallbackUrl options:@{} completionHandler:nil];
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert") message:LocalizedString(@"Your device does not support calling") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];    }
        }
        
    }];
    [alertController addAction:ok];
    [alertController addAction:no];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)BackgroundPerfoemenceStopMethod{
    [_timerForReqStatus invalidate];
    _timerForReqStatus = nil;
    [_timerForReqProgress invalidate];
    _timerForReqProgress = nil;
    
    [_timerForTrackStatus invalidate];
    _timerForTrackStatus = nil;
    if(audioPlayer != nil){
        [audioPlayer stop];
        audioPlayer = nil;
    }

}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self BackgroundPerfoemenceStopMethod];
}


- (IBAction)logoutBtnDidtab:(UIButton *)sender {
    [self logOut];
}

/***TEXTFIELD DELEGATE ****/

-(void)textFieldDidChange:(UITextField *)textField{
    
    NSString * stringText = textField.text;
    
    if (stringText.length >=1) {
        
        switch (textField.tag) {
                
            case 1:
                [self.secontTxt becomeFirstResponder];
                break;
                
            case 2:
                [self.thirdTxt becomeFirstResponder];
                break;
                
            case 3:
                [self.fourthTxt becomeFirstResponder];
                break;
                
            case 4:
                [self.fourthTxt resignFirstResponder];
                break;

                
            default:
                break;
        }
        
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((textField.text.length == 1) && (string.length == 1))
    {
        NSInteger nextTag = textField.tag + 1;
        // Try to find next responder
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder)
            nextResponder = [textField.superview viewWithTag:1];
        
        if (nextResponder){
            // Found next responder, so set it.
            [nextResponder becomeFirstResponder];
        }
        
        return NO;
    }
    return YES;
    
}



-(void)addToolBar:(UITextField *)textField{
    
    UIToolbar * numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
   // numberToolbar.tintColor = BASE1Color;
    numberToolbar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:
                                                     UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    
    textField.inputAccessoryView = numberToolbar;
}

-(void)doneWithNumberPad{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.firstTxt resignFirstResponder];
        [self.secontTxt resignFirstResponder];
        [self.thirdTxt resignFirstResponder];
        [self.fourthTxt resignFirstResponder];
        
    }];
    
}


- (IBAction)otpAction:(id)sender {
    
    NSString * checkOtp = [NSString stringWithFormat:@"%@%@%@%@",self.firstTxt.text,self.secontTxt.text,self.thirdTxt.text,self.fourthTxt.text];
    
    if (self.firstTxt.text.length==0 || self.secontTxt.text.length==0 || self.thirdTxt.text.length==0|| self.fourthTxt.text.length==0) {
        
        [CSS_Class alertviewController_title:@"Alert" MessageAlert:@"Enter the OTP from User" viewController:self okPop:NO];
        
    }else if(![otpStr isEqualToString:checkOtp]){
    
        [CSS_Class alertviewController_title:@"Alert" MessageAlert:@"Please enter the correct OTP" viewController:self okPop:NO];

    }
    else{
        
        [AnimationView closePopUp:_otpView bgView:_bgView];

//            _bgView.hidden = YES;
//            _otpView.hidden = YES;
        
            self.firstTxt.text = @"";
            self.secontTxt.text = @"";
            self.thirdTxt.text = @"";
            self.fourthTxt.text = @"";

            
        [_statusBtn setTitle:LocalizedString(@"PICKEDUP") forState:UIControlStateNormal];
        [self statusBtnAction:self];
    }
    

}
@end
