//
//  AFNHelper.h
//  Truck
//
//  Created by veena on 1/12/17.
//  Copyright © 2017 appoets. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "LoadingViewClass.h"
#import "AppDelegate.h"

@class LoadingViewClass;
@class AppDelegate;

#define POST_METHOD @"POST"
#define GET_METHOD  @"GET"
#define DELETE_METHOD @"DELETE"
#define PATCH_METHOD @"PATCH"

typedef void (^RequestCompletionBlock)(id response, NSDictionary *error, NSString *errorcode);

typedef void (^successTask)(BOOL Status);

typedef void (^ResponeSucess)(id response);

typedef void (^FailurewithValidations)(NSString *errorMessage);

typedef void (^FailurewithErrors)(NSString * error);

typedef void (^FailurewithInternet)(NSString *internetFailure);
@interface AFNHelper : NSObject
{
//blocks
    RequestCompletionBlock dataBlock;
    LoadingViewClass *loading;
    AppDelegate *appDelegate;
}

@property(nonatomic,copy)NSString *strReqMethod;

-(id)initWithRequestMethod:(NSString *)method;

-(void)getExtraDataFromPath:(NSString *)constantPath withParamData:(NSDictionary *)dictParam withBlock:(RequestCompletionBlock)block;

-(void)getDataFromPath:(NSString *)path withParamData:(NSDictionary *)dictParam withBlock:(RequestCompletionBlock)block;

-(void)getDataFromPath:(NSString *)path withParamDataImage:(NSDictionary *)dictParam andImage:(UIImage *)image withBlock:(RequestCompletionBlock)block;

-(void)getDataFromPath:(NSString *)path withDocumentImage:(UIImage *)image withBlock:(RequestCompletionBlock)block;

-(void)getDataFromPath_NoLoader:(NSString *)path withParamData:(NSDictionary *)dictParam withBlock:(RequestCompletionBlock)block;

-(void)refreshMethod_NoLoader:(NSString *)path withBlock:(RequestCompletionBlock)block;

//NEW METHOD API CALL


-(void)getDataFromPath:(NSString *)path WithType:(NSString *)type WithParameters :(NSDictionary *)params WithCompletedSuccess:(ResponeSucess)success OrValidationFailure:(FailurewithValidations)Errorvalidations OrErrorCode:(FailurewithErrors)ErrorCode OrIntentet:(FailurewithInternet) failureNetwork;


@end
