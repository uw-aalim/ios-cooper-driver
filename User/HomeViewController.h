//
//  HomeViewController.h
//  User
//
//  Created by iCOMPUTERS on 12/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftMenuView.h"
#import "LoadingViewClass.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "AppDelegate.h"
#import <HCSStarRatingView/HCSStarRatingView.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <MessageUI/MessageUI.h>
#import "Provider-Swift.h"

@class AppDelegate;
@class LoadingViewClass;
@import GoogleMaps;
@import SocketIO;

@interface HomeViewController : UIViewController< UIGestureRecognizerDelegate, GMSMapViewDelegate, CLLocationManagerDelegate, AVAudioPlayerDelegate, MFMessageComposeViewControllerDelegate,UITextFieldDelegate>
{
    AppDelegate *appDelegate;
    LeftMenuView *leftMenuViewClass;
    UIView *waitingBGView;
    LoadingViewClass *loading;
    UIView *backgroundView, *popUpView;
    BOOL gotLocation, locationMapped, moveNextLocation, leftmenuFlag, socketConnectFlag, isSchedule;
    NSString *s_Lat, *s_Lng, *d_Lat, *d_Lng, *mobileStr, *locationString, *switchMapStr;
    int secondsLeft, hours, minutes;
    NSString *dS_Lat, *dS_Lng, *dD_Lat, *dD_Lng, *scheduleStr, *globalStatus, *accountStatus;
    NSDictionary *params;
    
}

@property(strong,nonatomic) NSString * paymentCompleted;

@property (weak, nonatomic) IBOutlet UILabel *awaittingApproveLbl;

@property(nonatomic, strong) NSTimer *timerForReqProgress;
@property(nonatomic, strong) NSTimer *timerForTrackStatus;

@property (nonatomic)BOOL  TrackStatus, WalletStatus;

@property(nonatomic, strong) NSTimer *timerForReqStatus;
@property(nonatomic, strong) NSTimer *timer;
@property(nonatomic, strong) NSTimer *soundTimer;

@property (weak, nonatomic) IBOutlet UIButton *onlineBtn;
@property (weak, nonatomic) IBOutlet UIView *whiteView;
@property (weak, nonatomic) IBOutlet UIView *mapView;

@property(strong,nonatomic)IBOutlet GMSMarker*marker;
@property (strong, nonatomic) IBOutlet GMSMapView *mkap;
@property(nonatomic,retain) CLLocationManager *locationManager;

@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIImageView *menuImgBtn;

@property (weak, nonatomic) IBOutlet UIView *notifyView;
@property (weak, nonatomic) IBOutlet UIView *timerView;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
@property (weak, nonatomic) IBOutlet UIButton *rejectBtn;

@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@property (weak, nonatomic) IBOutlet UIButton *statusBtn;
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
@property (weak, nonatomic) IBOutlet UIImageView *rateUserImg;

@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;

@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet UIView *rateViewView;
@property (weak, nonatomic) IBOutlet UIView *invoiceView;
@property (weak, nonatomic) IBOutlet UIView *commonRateView;

@property (weak, nonatomic) IBOutlet UIScrollView *rateScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imgPayment;


@property (weak, nonatomic) IBOutlet UITextField *commentsText;
@property (weak, nonatomic) IBOutlet UIButton *paymentBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@property (strong, nonatomic) IBOutlet HCSStarRatingView *rating;
@property (strong, nonatomic) IBOutlet HCSStarRatingView *rating_user;

@property (weak, nonatomic) IBOutlet UIButton *navigationBtn;
@property (weak, nonatomic) IBOutlet UIButton *currentLocationBtn;

@property (weak, nonatomic) IBOutlet UIView *gifView;
@property (weak, nonatomic) IBOutlet UIImageView *gifImage;

@property (weak, nonatomic) IBOutlet UILabel *waitingLbl;

@property (weak, nonatomic) IBOutlet UIButton *logoutBtn;

/////Invoice
@property (weak, nonatomic) IBOutlet UILabel *totalLbl;


@property (weak, nonatomic) IBOutlet UILabel *baseFareLbl;
@property (weak, nonatomic) IBOutlet UILabel *taxLbl;
@property (weak, nonatomic) IBOutlet UILabel *serviceChargeLbl;


@property (weak, nonatomic) IBOutlet UILabel *baseValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *taxValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *distanceValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *paymentModeLbl;
@property (weak, nonatomic) IBOutlet UILabel *rateNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *commisionLbl;
@property (weak, nonatomic) IBOutlet UILabel *distanceLbl;
@property (weak, nonatomic) IBOutlet UILabel *invoiceIdLbl;

@property (weak, nonatomic) IBOutlet UILabel *invoice_WalletLbl;
@property (weak, nonatomic) IBOutlet UILabel *invoice_WalletAmt;

@property (weak, nonatomic) IBOutlet UILabel *invoice_discountAmt;
@property (weak, nonatomic) IBOutlet UILabel *invoice_discountLbl;


/// SKU Img
@property (weak, nonatomic) IBOutlet UIImageView *arrivedImg;
@property (weak, nonatomic) IBOutlet UIImageView *pickupImg;
@property (weak, nonatomic) IBOutlet UIImageView *finishedImg;
@property (weak, nonatomic) IBOutlet UIView *SKUView;

//Schedule
@property (weak, nonatomic) IBOutlet UILabel *scheduleLbl;
@property (weak, nonatomic) IBOutlet UIButton *sosBtn;

@property (weak, nonatomic) IBOutlet UILabel *pickUpAddressLbl;
@property (weak, nonatomic) IBOutlet UILabel *dropAddressLbl;
@property (weak, nonatomic) IBOutlet UIView *pickupDropAddressView;

- (IBAction)logoutBtnDidtab:(UIButton *)sender;

/****OTP Verification*****/

@property (weak, nonatomic) IBOutlet UITextField *firstTxt;
@property (weak, nonatomic) IBOutlet UITextField *secontTxt;
@property (weak, nonatomic) IBOutlet UITextField *thirdTxt;
@property (weak, nonatomic) IBOutlet UITextField *fourthTxt;
@property (weak, nonatomic) IBOutlet UIButton *verfySubmit;
@property (weak, nonatomic) IBOutlet UIView *otpView;
- (IBAction)otpAction:(id)sender;

@end
