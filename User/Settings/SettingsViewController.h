//
//  SettingsViewController.h
//  Provider
//
//  Created by Benjamin Cortens on 2018-11-20.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#ifndef SettingsController_h
#define SettingsController_h

@class AppDelegate;

@interface SettingsViewController : UIViewController <UIScrollViewDelegate> {
	AppDelegate *appDelegate;
}


@property (weak, nonatomic) IBOutlet UILabel *titleBarLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *largeTitleViewTop;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIButton *editProfileButton;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *profileNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *profileAddressPhoneLabel;

@property (strong, nonatomic) IBOutlet UILabel *privacyLabel;
@property (strong, nonatomic) IBOutlet UILabel *signOutLabel;



+(instancetype)initController;

-(IBAction) backBtn:(id)sender;
-(IBAction) editProfilePressed:(UIButton *)sender;
-(IBAction) signOutPressed:(UIButton *)sender;

@end

#endif
