//
//  SettingsViewController.m
//  Provider
//
//  Created by Benjamin Cortens on 2018-11-20.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import "SettingsViewController.h"
#import "Provider-Swift.h"
#import "ProfileViewController.h"
#import "UIColor+CoOper-Objc.h"
#import "AFNHelper.h"
#import "Utilities.h"
#import "CSS_Class.h"
#import "config.h"
#import "Constant.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

//	============================================================================================================
//	MARK:- Override Properties
//	============================================================================================================

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}





//	============================================================================================================
//	MARK:- Initialization
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
+(instancetype)initController {
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle: nil];
	SettingsViewController *settingsController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
	return settingsController;
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad {
	[super viewDidLoad];
	
	self->appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	[self setupViewDesign];
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	[self getProfileDetails];
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
-(void)setupViewDesign {
	self.profileImageView.layer.borderColor = UIColor.blackColor.CGColor;
	self.profileImageView.layer.borderWidth = 1.0;
	
	self.editProfileButton.tintColor = [UIColor tealColourObjC];
	
	self.scrollView.delegate = self;
}
//	------------------------------------------------------------------------------------------------------------




//	============================================================================================================
//	MARK:- Remote Data Loading
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void)getProfileDetails {
	if([appDelegate internetConnected]) {
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
		appDelegate.viewControllerName = @"Profile";
		[afn getDataFromPath:PROFILE withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *strErrorCode) {
			if (response) {
				NSLog(@"RESPONSE ...%@", response);
				
				NSString *firstName = [Utilities removeNullFromString: response[@"first_name"]];
				NSString *lastName = [Utilities removeNullFromString: response[@"last_name"]];
				NSString *phoneString = [Utilities removeNullFromString: response[@"mobile"]];
				NSString *emailString = [Utilities removeNullFromString: response[@"email"]];
				
				NSString *strProfile=[Utilities removeNullFromString:response[@"picture"]];
				
				NSString *nameStr = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
				NSString *phoneEmailStr = [NSString stringWithFormat:@"%@\n%@",phoneString,emailString];
				
				self.profileNameLabel.text = nameStr;
				self.profileAddressPhoneLabel.text = phoneEmailStr;
				
				if (![strProfile isEqualToString:@""]) {
					
					NSString *strSub = [strProfile stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
					NSURL *imgUrl;
					
					if ([strProfile containsString:@"http"]) {
						imgUrl = [NSURL URLWithString:strProfile];
					}
					else {
						imgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/storage/%@", SERVICE_URL, strSub]];
					}
					
					dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
					dispatch_async(q, ^{
						/* Fetch the image from the server... */
						NSData *data = [NSData dataWithContentsOfURL:imgUrl];
						UIImage *img = [[UIImage alloc] initWithData:data];
						
						dispatch_async(dispatch_get_main_queue(), ^{
							//[self->appDelegate onEndLoader];
							//                            [_btnProfilePic setBackgroundImage:img forState:UIControlStateNormal];
							[self.profileImageView setImage:img];
						});
					});
				}
			}
			else {
				if ([strErrorCode intValue]==1)  {
					[CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
				}
				else if ([strErrorCode intValue]==2) {
					[CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
					if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"]) {
						//Refresh token
					}
					else {
						//[self refreshMethod];
						
						//                             [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
						//                             ViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
						//                             [self.navigationController pushViewController:wallet animated:YES];
					}
				}
				else {
					[CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
				}
				NSLog(@"%@",error);
				
			}
		}];
	}
	else {
		UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
		UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
		[alertController addAction:ok];
		[self presentViewController:alertController animated:YES completion:nil];
	}
}
//	------------------------------------------------------------------------------------------------------------





//	============================================================================================================
//	MARK:- Action Responders
//	============================================================================================================


//	------------------------------------------------------------------------------------------------------------
-(IBAction)backBtn:(id)sender
{
	if (self.navigationController == NULL){
		[self dismissViewControllerAnimated:YES completion:nil];
	}
	else if (self.navigationController.viewControllers.count <= 1) {
		[self.navigationController.presentingViewController dismissViewControllerAnimated:true completion:NULL];
	}
	else {
		[self.navigationController popViewControllerAnimated:YES];
	}
	
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
- (IBAction)editProfilePressed:(UIButton *)sender {
	ProfileViewController *viewController = [ProfileViewController initController];
	[self.navigationController pushViewController:viewController animated:true];
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
- (IBAction)signOutPressed:(UIButton *)sender {
	
	if ([appDelegate internetConnected]) {
		UIAlertController *alertController = [UIAlertController
											  alertControllerWithTitle:@"LOGOUT"
											  message:@"Are you sure you want to logout?"
											  preferredStyle:UIAlertControllerStyleAlert];
		UIAlertAction *noAction = [UIAlertAction
								   actionWithTitle:@"NO"
								   style:UIAlertActionStyleCancel
								   handler:^(UIAlertAction * _Nonnull action) {
									   NSLog(@"Dont logout");
								   }];
		UIAlertAction *yesAction = [UIAlertAction
									actionWithTitle:@"YES"
									style:UIAlertActionStyleDefault
									handler:^(UIAlertAction * _Nonnull action) {
										
										
										
										AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
										NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
										NSString *idStr = [defaults valueForKey:@"id"];
										NSDictionary *param = @{@"id":idStr};
										
										NSString *url = [NSString stringWithFormat:@"/api/provider/logout"];
										[afn getDataFromPath_NoLoader:url withParamData:param withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
											
											[KeyConstants constants].RideStatus = @"";
											[KeyConstants constants].RideUpdateStatus = @"";
											[defaults setValue:[NSNumber numberWithBool:0] forKey:@"is_valid"];
											
											[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
											
											UINavigationController *navController = self.navigationController;											
											WelcomeViewController *viewController = [WelcomeViewController initController];
											UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
											[navController presentViewController:navigationController animated:YES completion:^{
												NSLog(@"Presented welcome view controller");
												[self.navigationController popToRootViewControllerAnimated:false];
											}];
											
											//if (response) {
												//[self ViewOuterTap];
											
											/*}
											else {
												if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"]) {
													//Refresh token
													
													
												}else{
													[CSS_Class alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
												}
												
											}*/
										}];
										
										/*
										[NSUserDefaults.standardUserDefaults removeObjectForKey:@"isLoggedin"];
										NSString *bundleId = NSBundle.mainBundle.bundleIdentifier;
										[NSUserDefaults.standardUserDefaults removePersistentDomainForName:bundleId];
										
										UIViewController *presentingViewController = self.navigationController.presentingViewController;
										[self.navigationController dismissViewControllerAnimated:true completion:^{
											WelcomeViewController *viewController = [WelcomeViewController initController];
											UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:viewController];
											[navigationController.navigationBar setHidden:TRUE];
											[presentingViewController presentViewController:navigationController animated:true completion:NULL];
										}];*/
									}];
		
		[alertController addAction:noAction];
		[alertController addAction:yesAction];
		[self presentViewController:alertController animated:true completion:NULL];
	}
	
	else {
		[CSS_Class alertviewController_title:LocalizedString(@"Alert") MessageAlert:NSLocalizedString(@"CHKNET", nil) viewController:self okPop:NO];
	}
	
}
//	------------------------------------------------------------------------------------------------------------



//	============================================================================================================
//	MARK:- UIScrollViewDelegate
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
	if (scrollView.contentOffset.y >= 0.0) {
		if (scrollView.contentOffset.y <= 44.0) {
			self.largeTitleViewTop.constant = -(scrollView.contentOffset.y);
		}
		else {
			self.largeTitleViewTop.constant = -44.0;
		}
	}
	else {
		self.largeTitleViewTop.constant = 0;
	}
	
	//	Start showing the new title bar label when we have scrolled at least 12 points
	if (scrollView.contentOffset.y > 12) {
		CGFloat alpha = (scrollView.contentOffset.y - 12) / 32.0;
		if (alpha > 1.0) {
			self.titleBarLabel.alpha = 1.0;
		}
		else if (alpha < 0.0) {
			self.titleBarLabel.alpha = 0.0;
		}
		else {
			self.titleBarLabel.alpha = alpha;
		}
	}
	else {
		self.titleBarLabel.alpha = 0.0;
	}
}
//	------------------------------------------------------------------------------------------------------------


@end
