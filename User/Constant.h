//
//  Constant.h
//  Provider
//
//  Created by APPLE on 12/7/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <Foundation/Foundation.h>

//Staging
extern NSString *const SERVICE_URL;
extern NSString * APP_IMG_URL;
extern NSString * APP_NAME;
extern NSString * ClientID;
extern NSString * Client_SECRET;
extern NSString * WEB_SOCKET;

extern NSString *const  REGISTER;
extern NSString *const  LOGIN;
extern NSString *const  PROFILE;
extern NSString *const  STATUS;
extern NSString *const  CHANGE_PASSWORD;
extern NSString *const  LOCATION;
extern NSString *const  INCOMING_REQUEST;
extern NSString *const  STATUS_CHECK;
extern NSString *const  PAST_TRIPS;
extern NSString *const  HISTORY_DETAILS;

extern NSString *const  UPCOMING_HISTORYDETAILS;
extern NSString *const  UPCOMING_TRIPS;

extern NSString *const  CANCEL_REQUEST;

extern NSString *const  GET_EARNINGS;

extern NSString *const  MD_FORGOTPASSWORD;
extern NSString *const  MD_RESETPASSWORD;
extern NSString *const  MD_FACEBOOK;
extern NSString *const  MD_GOOGLE;
extern NSString *const  MD_HELP;
extern NSString *const  MD_SUMMARY;
extern NSString *const  MD_REFRESH;

extern NSString *const UD_ACCESS_TOKEN;
extern NSString *const UD_TOKEN_TYPE;

extern NSString *const UD_REFERSH_TOKEN;
extern NSString *const UD_SOCIAL;
