//
//  ProfileViewController.m
//  Provider
//
//  Created by iCOMPUTERS on 17/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "ProfileViewController.h"
#import "CSS_Class.h"
#import "config.h"
#import "Colors.h"
#import "UIScrollView+EKKeyboardAvoiding.h"
#import "AFNHelper.h"
#import "Utilities.h"
#import "ChangePasswordViewController.h"
#import "HomeViewController.h"
#import "ViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SDWebImage/UIImageView+WebCache.h"
#import <SDWebImage/UIButton+WebCache.h>
#import "LanguageController.h"
#import "Provider-Swift.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

//	============================================================================================================
//	MARK:- Override Properties
//	============================================================================================================

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}






//	============================================================================================================
//	MARK:- Initialization
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
+(instancetype)initController {
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Profile" bundle: nil];
	//UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileNavigationController"];
	ProfileViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
	return viewController;
}
//	------------------------------------------------------------------------------------------------------------






//	============================================================================================================
//	MARK:- View Loading and Appearance
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self->appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	[self setupViewDesign];
	[self getProfileDetails];
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:YES];

}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	
	//	Once the view is visible - setup the notifications
	//	Doing so after the view has appeared is key to preventing multiple registration attempts
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(keyboardWillShow:)
	 name:UIKeyboardWillShowNotification
	 object:NULL];
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(keyboardWillShow:)
	 name:UIKeyboardWillChangeFrameNotification
	 object:NULL];
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(keyboardWillHide:)
	 name:UIKeyboardWillHideNotification
	 object:NULL];
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void) viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	
	[[NSNotificationCenter defaultCenter]
	 removeObserver:self];
}
//	------------------------------------------------------------------------------------------------------------





//	============================================================================================================
//	MARK:- View Design
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void)setupViewDesign {
	
	self.firstNameText.delegate = self;
	self.lastNameText.delegate = self;
	self.phoneText.delegate = self;
	self.emailText.delegate = self;
	
	self.scrollView.delegate = self;
	self.profileOutline.layer.borderColor = UIColor.blackColor.CGColor;
	self.profileOutline.layer.borderWidth = 1.0;
}
//	------------------------------------------------------------------------------------------------------------






//	============================================================================================================
//	MARK:- Remote Data Loading
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void)getProfileDetails {
    if([appDelegate internetConnected]) {
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
            appDelegate.viewControllerName = @"Profile";
            [afn getDataFromPath:PROFILE withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *strErrorCode) {
                 if (response) {
                     NSLog(@"RESPONSE ...%@", response);
                     NSString *strProfile = [Utilities removeNullFromString:[response valueForKey:@"avatar"]];
					 
					 self->_firstNameText.text=  [Utilities removeNullFromString: response[@"first_name"]];
					 self->_lastNameText.text= [Utilities removeNullFromString: response[@"last_name"]];
					 self->_phoneText.text= [Utilities removeNullFromString: response[@"mobile"]];
					 self->_emailText.text= [Utilities removeNullFromString: response[@"email"]];
					 
                     NSDictionary *serviceDict = [response valueForKey:@"service"];
                     NSDictionary *nameDict = [serviceDict valueForKey:@"service_type"];
                     self->_serviceTypeText.text = [Utilities removeNullFromString:[nameDict valueForKey:@"name"]];
					 
					 if (![strProfile isKindOfClass:[NSNull class]]) {
						 NSURL *imgUrl;
						 if ([strProfile length]!=0) {
							 if ([strProfile containsString:@"http"]) {
								 imgUrl = [NSURL URLWithString:strProfile];
								 
								 [self->_profileImg sd_setImageWithURL:imgUrl
													  placeholderImage:[UIImage imageNamed:@"menu_profile_placeholder"]];
							 }
							 else {
								 imgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/storage/%@", SERVICE_URL, strProfile]];
								 
								 [self->_profileImg sd_setImageWithURL:imgUrl
													  placeholderImage:[UIImage imageNamed:@"menu_profile_placeholder"]];
							 }
						 }
						 else {
							 self->_profileImg.image = [UIImage imageNamed:@"menu_profile_placeholder"];
						 }
					 }
                 }
                 else {
                     if ([strErrorCode intValue]==1)  {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                     else if ([strErrorCode intValue]==2) {
                         if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"]) {
                             //Refresh token
                         }
                         else {
                             [self refreshMethod];

//                             [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
//                             ViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//                             [self.navigationController pushViewController:wallet animated:YES];
                         }
                     }
                     else {
                         [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                     }
                     NSLog(@"%@",error);

                 }
             }];
        }
        else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void)refreshMethod {
	AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
	[afn refreshMethod_NoLoader:
	 @""withBlock:^(id responseObject, NSDictionary *error, NSString *errorcode) {
		 
		 if (responseObject)
		 {
			 NSLog(@"Refresh Method ...%@", responseObject);
			 NSString *access_token = [responseObject valueForKey:@"access_token"];
			 NSString *first_name = [responseObject valueForKey:@"first_name"];
			 NSString *avatar =[Utilities removeNullFromString:[responseObject valueForKey:@"avatar"]];
			 NSString *status =[Utilities removeNullFromString:[responseObject valueForKey:@"status"]];
			 NSString *currencyStr=[responseObject valueForKey:@"currency"];
			 NSString *socialId=[Utilities removeNullFromString:[responseObject valueForKey:@"social_unique_id"]];
			 NSString *last_name = [responseObject valueForKey:@"last_name"];
			 
			 
			 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			 [defaults setObject:last_name forKey:@"last_name"];
			 [defaults setObject:access_token forKey:@"access_token"];
			 [defaults setObject:first_name forKey:@"first_name"];
			 [defaults setObject:avatar forKey:@"avatar"];
			 [defaults setObject:status forKey:@"status"];
			 [defaults setValue:currencyStr forKey:@"currency"];
			 [defaults setValue:socialId forKey:@"social_unique_id"];
			 [defaults setObject:[responseObject valueForKey:@"id"] forKey:@"id"];
			 [defaults setObject:[responseObject valueForKey:@"sos"] forKey:@"sos"];
		 }
		 else {
			 [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
			 //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
			 
			 /*
			  PageViewController * wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];
			  
			  [self.navigationController pushViewController:wallet animated:YES]; */
			 WelcomeViewController *viewController = [WelcomeViewController initController];
			 UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
			 [self.navigationController presentViewController:navigationController animated:YES completion:^{
				 NSLog(@"Presented welcome view controller");
			 }];
		 }
	 }];
}
//	------------------------------------------------------------------------------------------------------------





//	============================================================================================================
//	MARK:- Action Responders
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(IBAction)backBtn:(id)sender {
	if (self.navigationController.viewControllers.count <= 1) {
		[self.navigationController.presentingViewController dismissViewControllerAnimated:true completion:NULL];
	}
	else {
		[self.navigationController popViewControllerAnimated:YES];
	}
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
- (IBAction)onChangePwd:(id)sender {
	//UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
	ChangePasswordViewController *viewController = [ChangePasswordViewController initController];
	[self.navigationController pushViewController:viewController animated:YES];
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(IBAction) onProfilePic: (id) sender {
	UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.allowsEditing = NO;
	picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	
	[self presentViewController:picker animated:YES completion:nil];
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(IBAction)saveBtn:(id)sender
{
	[self.view endEditing:YES];
	
	if ([_firstNameText.text isEqualToString:@""] ||[_lastNameText.text isEqualToString:@""] || [_phoneText.text isEqualToString:@""]) {
		UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"Please enter all the fields") preferredStyle:UIAlertControllerStyleAlert];
		UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
		[alertController addAction:ok];
		[self presentViewController:alertController animated:YES completion:nil];
	}
	else {
		if([appDelegate internetConnected])
		{
			NSDictionary*params;
			params=@{@"mobile":_phoneText.text, @"first_name":_firstNameText.text, @"last_name":_lastNameText.text};
			
			NSLog(@"PARAMS...%@", params);
			
			AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
			appDelegate.viewControllerName = @"Profile";
			[afn getDataFromPath: PROFILE
			  withParamDataImage: params
						andImage: self->_profileImg.image
					   withBlock: ^(id response, NSDictionary *error, NSString *strErrorCode)
			{
				 
			
				if (response) {
					 NSLog(@"RESPONSE ...%@", response);
					 NSString *first_name = [response valueForKey:@"first_name"];
					 NSString *avatar =[Utilities removeNullFromString:[response valueForKey:@"avatar"]];
					 NSString *status =[Utilities removeNullFromString:[response valueForKey:@"status"]];
					 NSString *last_name = [response valueForKey:@"last_name"];
					 
					 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
					 [defaults setObject:last_name forKey:@"last_name"];
					 [defaults setObject:first_name forKey:@"first_name"];
					 [defaults setObject:avatar forKey:@"avatar"];
					 [defaults setObject:status forKey:@"status"];
					 [defaults setObject:[response valueForKey:@"id"] forKey:@"id"];
					 
					 
					 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Success!") message:LocalizedString(@"Profile Updated")preferredStyle:UIAlertControllerStyleAlert];
					 UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
					 [alertController addAction:ok];
					 [self presentViewController:alertController animated:YES completion:nil];
				 }
				 else
				 {
					 if ([strErrorCode intValue]==1)
					 {
						 [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
					 }
					 else if ([strErrorCode intValue]==2)
					 {
						 if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
						 {
							 //Refresh token
						 }
						 else
						 {
							 [self refreshMethod];
							 
							 //                             [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
							 //                             ViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
							 //                             [self.navigationController pushViewController:wallet animated:YES];
						 }
					 }
					 else if ([strErrorCode intValue]==3)
					 {
						 if ([error objectForKey:@"email"]) {
							 [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
						 }
						 else if ([error objectForKey:@"first_name"]) {
							 [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"first_name"] objectAtIndex:0]  viewController:self okPop:NO];
						 }
						 else if ([error objectForKey:@"last_name"]) {
							 [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"last_name"] objectAtIndex:0]  viewController:self okPop:NO];
						 }
						 else if ([error objectForKey:@"mobile"]) {
							 [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"mobile"] objectAtIndex:0]  viewController:self okPop:NO];
						 }
						 else if ([error objectForKey:@"picture"])
						 {
							 [CSS_Class alertviewController_title:@"" MessageAlert:[[error objectForKey:@"picture"] objectAtIndex:0]  viewController:self okPop:NO];
						 }
						 else
						 {
							 [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
						 }
					 }
					 else
					 {
						 [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
					 }
					 NSLog(@"%@",error);
					 
				 }
			 }];
		}
		else
		{
			UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
			UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
			[alertController addAction:ok];
			[self presentViewController:alertController animated:YES completion:nil];
		}
	}
}
//	------------------------------------------------------------------------------------------------------------






//	============================================================================================================
//	MARK:- TextFieldDelegate
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if(textField==self.firstNameText) {
		[self.lastNameText becomeFirstResponder];
	}
	else if(textField==self.lastNameText) {
		[self.phoneText becomeFirstResponder];
	}
	else if(textField==self.phoneText) {
		[self.emailText becomeFirstResponder];
	}
	else {
		[textField resignFirstResponder];
	}
	return YES;
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	if((textField == _firstNameText) || (textField == _lastNameText)) {
		NSUInteger oldLength = [textField.text length];
		NSUInteger replacementLength = [string length];
		NSUInteger rangeLength = range.length;
		
		NSUInteger newLength = oldLength - rangeLength + replacementLength;
		
		BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
		
		return newLength <= INPUTLENGTH || returnKey;
	}
	else if (textField == _phoneText) {
		NSUInteger oldLength = [textField.text length];
		NSUInteger replacementLength = [string length];
		NSUInteger rangeLength = range.length;
		
		NSUInteger newLength = oldLength - rangeLength + replacementLength;
		
		BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
		
		return newLength <= PHONELENGTH || returnKey;
	}
	else {
		return YES;
	}
	
	return NO;
}
//	------------------------------------------------------------------------------------------------------------





//	============================================================================================================
//	MARK:- Image Picker Controller Delegage
//	============================================================================================================



//	------------------------------------------------------------------------------------------------------------
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(image) forKey:@"profileimage"];
    [self.profileImg setImage:image];
    [picker dismissViewControllerAnimated:true completion:nil];
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}
//	------------------------------------------------------------------------------------------------------------





//	============================================================================================================
//	MARK:- Keyboard Notifications
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void) keyboardWillShow: (NSNotification *) notification {
	CGRect keyboardFrame = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	double keyboardDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	
	self.scrollViewBottom.constant = keyboardFrame.size.height;
	NSLog(@"Setting scroll view bottom constraint %f",keyboardFrame.size.height);
	[self.view setNeedsLayout];
	[UIView animateWithDuration:keyboardDuration animations:^{
		[self.view layoutIfNeeded];
	}];
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void) keyboardWillHide: (NSNotification *) notification {
	
	double keyboardDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	self.scrollViewBottom.constant = 0.0;
	[self.view setNeedsLayout];
	[UIView animateWithDuration:keyboardDuration animations:^{
		[self.view layoutIfNeeded];
	}];
}
//	------------------------------------------------------------------------------------------------------------




//	============================================================================================================
//	MARK:- UIScrollViewDelegate
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
	if (scrollView.contentOffset.y >= 0.0) {
		if (scrollView.contentOffset.y <= 44.0) {
			self.largeTitleViewTop.constant = -(scrollView.contentOffset.y);
		}
		else {
			self.largeTitleViewTop.constant = -44.0;
		}
	}
	else {
		self.largeTitleViewTop.constant = 0;
	}
	
	//	Start showing the new title bar label when we have scrolled at least 12 points
	if (scrollView.contentOffset.y > 12) {
		CGFloat alpha = (scrollView.contentOffset.y - 12) / 32.0;
		if (alpha > 1.0) {
			self.titleBarLabel.alpha = 1.0;
		}
		else if (alpha < 0.0) {
			self.titleBarLabel.alpha = 0.0;
		}
		else {
			self.titleBarLabel.alpha = alpha;
		}
	}
	else {
		self.titleBarLabel.alpha = 0.0;
	}
}
//	------------------------------------------------------------------------------------------------------------

@end
