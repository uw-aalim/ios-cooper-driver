
// Config Files Contains URL, Colors, Validations

//Staging
//#define SERVICE_URL @"http://schedule.coOper.co/"
//#define WEB_SOCKET @"http://schedule.coOper.co:7000"

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define MINT_METHOD_TRACE_START
#define MINT_NONARC_METHOD_TRACE_STOP

#define INPUTLENGTH 255
#define PHONELENGTH 15
#define PASSWORDLENGTH 20

//#define CLIENT_SECRET @"VuVjIz4Jt0oCo8EXjPSBrYKfqTTyfeg3VKPdKIdC"
//#define CLIENT_ID @"2"
#define ACCEPTABLE_CHARECTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"


//#define REGISTER @"api/provider/register"
//#define LOGIN @"api/provider/oauth/token"
//#define PROFILE @"api/provider/profile"
//#define STATUS @"api/provider/profile/available"
//#define CHANGE_PASSWORD @"api/provider/profile/password"
//#define LOCATION @"api/provider/location"
//#define INCOMING_REQUEST @"api/provider/trip"
//#define STATUS_CHECK @"api/provider/requests/status"
//#define PAST_TRIPS @"api/provider/requests/history"
//#define HISTORY_DETAILS @"api/provider/requests/history/details"
//
//#define UPCOMING_HISTORYDETAILS @"api/provider/requests/upcoming/details"
//#define UPCOMING_TRIPS @"api/provider/requests/upcoming"
//
//#define CANCEL_REQUEST @"api/provider/cancel"
//
//#define GET_EARNINGS @"api/provider/target"
//
//#define MD_FORGOTPASSWORD @"api/provider/forgot/password"
//#define MD_RESETPASSWORD @"api/provider/reset/password"
//#define MD_FACEBOOK @"api/provider/auth/facebook"
//#define MD_GOOGLE @"api/provider/auth/google"
//#define MD_HELP @"api/provider/help"
//#define MD_SUMMARY @"api/provider/summary"
//#define MD_REFRESH @"api/provider/refresh/token"
