//
//  PasswordViewController.h
//  User
//
//  Created by iCOMPUTERS on 12/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadingViewClass.h"
#import "AppDelegate.h"

@class LoadingViewClass;
@class AppDelegate;

@interface PasswordViewController : UIViewController<UIGestureRecognizerDelegate>
{
    LoadingViewClass *loading;
    AppDelegate *appDelegate;
}

@property (weak, nonatomic) IBOutlet UILabel *helpLbl;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;

@property (weak, nonatomic) IBOutlet UILabel *password_Lbl;

//Email
@property (strong, nonatomic) NSString *emailStr;

@end
