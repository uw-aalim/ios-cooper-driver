//
//  PasswordViewController.m
//  User
//
//  Created by iCOMPUTERS on 12/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "PasswordViewController.h"
#import "CSS_Class.h"
#import "config.h"
#import "HomeViewController.h"
#import "ForgotPasswordViewController.h"
#import "RegisterViewController.h"
#import "AFNHelper.h"
#import "Utilities.h"
#import "ViewController.h"
#import "newHomeController.h"

@interface PasswordViewController ()

@end

@implementation PasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    [self setDesignStyles];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapReceived:)];
    [tapGestureRecognizer setDelegate:self];
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self LocalizationUpdate];
}

-(void)LocalizationUpdate{
    
    _helpLbl.text = LocalizedString(@"Welcome back, sign in to continue");
    _password_Lbl.text = LocalizedString(@"I forgot my password");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tapReceived:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.view endEditing:YES];
}

-(void)setDesignStyles
{
    [CSS_Class APP_labelName:_helpLbl];
    [CSS_Class APP_textfield_Outfocus:_passwordText];
    
    [CSS_Class APP_SocialLabelName:_password_Lbl];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_passwordText)
    {
        [_passwordText resignFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField == _passwordText)
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= PASSWORDLENGTH || returnKey;
    }
    else
    {
        return YES;
    }
    
    return NO;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [CSS_Class APP_textfield_Infocus:textField];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [CSS_Class APP_textfield_Outfocus:textField];
    return YES;
}

-(IBAction)Nextbtn:(id)sender
{
    [self.view endEditing:YES];
    
//    if(_passwordText.text.length==0)
//    {
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT")  message:LocalizedString(@"Password is required") preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
//        [alertController addAction:ok];
//        [self presentViewController:alertController animated:YES completion:nil];
//        
//    }
//    else if(_passwordText.text.length <= 5)
//    {
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT")  message:LocalizedString(@"The password must be greater than 6 characters") preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
//        [alertController addAction:ok];
//        [self presentViewController:alertController animated:YES completion:nil];
//        
//    }
//    
//    
//    else
//    {
//    if([self Passwordvalidaton:_passwordText.text]){
        if([appDelegate internetConnected])
        {
             appDelegate.device_tokenStr = @"No Device";
            NSDictionary*params;
            params=@{@"email":_emailStr,
					 @"password":_passwordText.text,
					 @"device_token":appDelegate.device_tokenStr,
					 @"device_id":appDelegate.device_UDID ,
					 @"device_type":@"ios"};
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            
            [afn getDataFromPath:LOGIN withParamData:params withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
            {
                if (response)
                {
                    NSLog(@"RESPONSE ...%@", response);
                    NSString *access_token = [response valueForKey:@"access_token"];
                    NSString *first_name = [response valueForKey:@"first_name"];
                    NSString *avatar =[Utilities removeNullFromString:[response valueForKey:@"avatar"]];
                    NSString *status =[Utilities removeNullFromString:[response valueForKey:@"status"]];
                    if([status isEqualToString:@"active"]){
                        [KeyConstants constants].RideStatus = @"ONLINE";
                    }else{
                        [KeyConstants constants].RideStatus = @"NOPERMISSION";
                    }
                    NSString *currencyStr=[response valueForKey:@"currency"];
                    NSString *socialId=[Utilities removeNullFromString:[response valueForKey:@"social_unique_id"]];
                    NSString *last_name = [response valueForKey:@"last_name"];
                    NSString *nameStr = [NSString stringWithFormat:@"%@ %@", [Utilities removeNullFromString: response[@"first_name"]], [Utilities removeNullFromString: response[@"last_name"]]];
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:last_name forKey:@"last_name"];
                    [defaults setObject:access_token forKey:@"access_token"];
                    [defaults setObject:first_name forKey:@"first_name"];
                    [defaults setObject:avatar forKey:@"avatar"];
                    
                    [defaults setObject:status forKey:@"status"];
                    [defaults setValue:currencyStr forKey:@"currency"];
                    [defaults setValue:socialId forKey:@"social_unique_id"];
                    [defaults setObject:[response valueForKey:@"id"] forKey:@"id"];
                    [defaults setObject:[response valueForKey:@"sos"] forKey:@"sos"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"User" forKey:@"SocialLogin"];
                    
                    // MARK: Set User Infomation locally
                    [defaults setObject:SERVICE_URL forKey:@"serviceurl"];
                    [defaults setObject:Client_SECRET forKey:@"passport"];
                    [defaults setObject:ClientID forKey:@"clientid"];
                    [defaults setObject:WEB_SOCKET forKey:@"websocket"];
                    [defaults setObject:APP_NAME forKey:@"appname"];
                    [defaults setObject:nameStr forKey:@"username"];
                    //            [user setObject:APP_IMG_URL forKey:@"appimg"];
//                    [defaults setObject:GMSMAP_KEY forKey:@"googleApiKey"];
//                    [defaults setObject:Stripe_KEY forKey:@"stripekey"];
                    
                    [defaults setObject:@"1" forKey:@"is_valid"];
                    [defaults synchronize];
                    [self nextControler];
                  
                    
                }
                else          

                {
                    _passwordText.text = @"";
                    if ([strErrorCode intValue]==1)
                    {
                        [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                    }
                    else if ([strErrorCode intValue]==2)
                    {
                        if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                        {
                            //Refresh token
                        }
                        else
                        {
                            [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[error valueForKey:@"error"]  viewController:self okPop:NO];
                        }
                    }
                    else if ([strErrorCode intValue]==3)
                    {
                        if ([error objectForKey:@"email"]) {
                            [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                        }
                        else if ([error objectForKey:@"password"]) {
                            [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:[[error objectForKey:@"password"] objectAtIndex:0]  viewController:self okPop:NO];
                        }
                        else
                        {
                            [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG")  viewController:self okPop:NO];
                        }
                    }
                    else
                    {
                        [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG")  viewController:self okPop:NO];
                    }
                    NSLog(@"%@",error);
                    
                }
            }];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
//    }
    
}
-(void)nextControler{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"New" bundle: nil];
    newHomeController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"newHomeController"];
    [self.navigationController pushViewController:controller animated:YES];
}

-(BOOL)Passwordvalidaton:(NSString *)password{
    
//    if(password.length != 0){
//        NSString *stricterFilterString = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,15}$";
//        NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
//        if( [passwordTest evaluateWithObject:password]){
//
//            return true;
//        }else{
//            [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:LocalizedString(@"The password must be minimum 8 characters and 1 Uppercase, 1 Lowercase , 1 number , and 1 special characters and maximum 16 characters ") viewController:self okPop:NO];
//        }
//
//    }
    if(password.length == 0){
        [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:LocalizedString(@"Password is required") viewController:self okPop:NO];
    }
    return false;
}


-(IBAction)passwordbtn:(id)sender
{
    ForgotPasswordViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
    [self presentViewController:controller animated:YES completion:nil];
}


- (IBAction)ShowPass:(UIButton *)sender{
    if (self.passwordText.secureTextEntry == YES){
        self.passwordText.secureTextEntry = NO;
    }
    else{
        self.passwordText.secureTextEntry = YES;
    }
}

- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
