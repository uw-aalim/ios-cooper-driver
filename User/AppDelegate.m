//
//  AppDelegate.m
//  User
//
//  Created by iCOMPUTERS on 12/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "Reachability.h"
#import <SplunkMint/SplunkMint.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "AFNHelper.h"
#import "Utilities.h"
#import "ViewController.h"
#import "newHomeController.h"
#import "config.h"
#import "IQKeyboardManager.h"

#import "CSS_Class.h"
#import "LanguageController.h"

#import "Harpy.h"
#import "Provider-Swift.h"

@import GoogleMaps;
@import GooglePlaces;
@import SplunkMint;
@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;


#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0

@import UserNotifications;

#endif


#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@interface AppDelegate () <HarpyDelegate, UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate
@synthesize device_tokenStr, device_UDID, viewControllerName;
NSString *const kGCMMessageIDKey = @"gcm.message_id";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self CheckversionUpdate];
    
    
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setPreviousNextDisplayMode:IQPreviousNextDisplayModeAlwaysHide];
    [self NavicationBarAppearence];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    NSError *configureError;
    [launchOptions valueForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    application.applicationIconBadgeNumber = 0;
    
    
    // SANDY TODO
    //    [[GGLContext sharedInstance] configureWithError:&configureError];
    //    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    
    device_tokenStr = @"no device";
    viewControllerName = @"";
    device_UDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *tokenStr = [defaults valueForKey:@"access_token"];
    
    NSString *mapKey = [defaults valueForKey:@"mapkey"];
    
    if (mapKey == nil || [mapKey isEqualToString:@""]) {
        [GMSServices provideAPIKey:@"AIzaSyAsVN1AHgbMr2n3SYtNZxFiT7e1bz3tP5s"];
        [GMSPlacesClient provideAPIKey:@"AIzaSyAsVN1AHgbMr2n3SYtNZxFiT7e1bz3tP5s"];
    } else {
        [GMSServices provideAPIKey:mapKey];////AIzaSyAsVN1AHgbMr2n3SYtNZxFiT7e1bz3tP5s
        [GMSPlacesClient provideAPIKey:mapKey];////AIzaSyAsVN1AHgbMr2n3SYtNZxFiT7e1bz3tP5s
    }
    
    
    //    [[Mint sharedInstance] disableNetworkMonitoring];
    //    [[Mint sharedInstance] initAndStartSessionWithAPIKey:@"dd424d93"];
    //    [Mint sharedInstance].applicationEnvironment = SPLAppEnvUserAcceptanceTesting;
    
    // if([[NSUserDefaults standardUserDefaults]objectForKey:@"FirstLoad"] !=nil){
    
    if (tokenStr == (id) [NSNull null] || tokenStr.length == 0 || [tokenStr isEqualToString:@""]) {
        LocalizationSetLanguage([[NSUserDefaults standardUserDefaults] objectForKey:@"LanguageCode"]);
        //            [[NSUserDefaults standardUserDefaults]setValue:@"Load" forKey:@"FirstLoad"];
        
        /*
         UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
         PageViewController * infoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];
         */
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"New" bundle:nil];
        
        newHomeController *infoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"newHomeController"];
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:infoController];
        self.window.rootViewController = self.navigationController;
        self.window.backgroundColor = [UIColor whiteColor];
        [self.window makeKeyAndVisible];
        
        WelcomeViewController *viewController = [WelcomeViewController initController];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        [self.navigationController presentViewController:navigationController animated:YES completion:^{
            NSLog(@"Presented welcome view controller");
        }];
        
        /*
         [[NSUserDefaults standardUserDefaults]setValue:@"Load" forKey:@"FirstLoad"];
         UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
         PageViewController * infoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];
         //infoController.page_identifier = @"Appdelegate";
         self.navigationController = [[UINavigationController alloc] initWithRootViewController:infoController];
         self.window.rootViewController = self.navigationController;
         self.window.backgroundColor = [UIColor whiteColor];
         [self.window makeKeyAndVisible];
         
         */
    } else {
        
        
        NSString *validBoolStr = [defaults valueForKey:@"is_valid"];
        
        int boolCheck = [validBoolStr intValue];
        
        if (boolCheck == 1) {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"New" bundle:nil];
            
            newHomeController *infoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"newHomeController"];
            self.navigationController = [[UINavigationController alloc] initWithRootViewController:infoController];
            self.window.rootViewController = self.navigationController;
            self.window.backgroundColor = [UIColor whiteColor];
            [self.window makeKeyAndVisible];
            
            
        } else {
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"New" bundle:nil];
            
            newHomeController *infoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"newHomeController"];
            self.navigationController = [[UINavigationController alloc] initWithRootViewController:infoController];
            self.window.rootViewController = self.navigationController;
            self.window.backgroundColor = [UIColor whiteColor];
            [self.window makeKeyAndVisible];
            
            WelcomeViewController *viewController = [WelcomeViewController initController];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
            [self.navigationController presentViewController:navigationController animated:YES completion:^{
                NSLog(@"Presented welcome view controller");
            }];
            
            /*
             UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
             PageViewController * infoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];
             */
            //self.navigationController = [[UINavigationController alloc] initWithRootViewController:infoController];
            /*self.window.rootViewController = self.navigationController;
             self.window.backgroundColor = [UIColor whiteColor];
             [self.window makeKeyAndVisible]; */
        }
        
        /*
         UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
         HomeViewController* infoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
         self.navigationController = [[UINavigationController alloc] initWithRootViewController:infoController];
         self.window.rootViewController = self.navigationController;
         self.window.backgroundColor = [UIColor whiteColor];
         [self.window makeKeyAndVisible];
         */
    }
    
    
    
    //    }else{
    //
    //    }
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    
    // ================================ FIREBASE BEGIN ================================
    
    // [START configure_firebase]
    [FIRApp configure];
    // [END configure_firebase]
    
    // [START set_messaging_delegate]
    [FIRMessaging messaging].delegate = self;
    // [END set_messaging_delegate]
    
    // Register for remote notifications. This shows a permission dialog on first run, to
    // show the dialog at a more appropriate time move this registration accordingly.
    // [START register_for_notifications]
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    [application registerForRemoteNotifications];
    // [END register_for_notifications]
    
    
    // ================================ FIREBASE END ================================
    
    
    return YES;
}

- (void)NavicationBarAppearence {
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    
    [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"back_icon"]];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-1000, -1000) forBarMetrics:UIBarMetricsDefault];
    if (@available(iOS 11, *)) {
        [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-200, 0) forBarMetrics:UIBarMetricsDefault];
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor clearColor]} forState:UIControlStateNormal];
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor clearColor]} forState:UIControlStateHighlighted];
        
        
    } else {
        [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-1000, -1000) forBarMetrics:UIBarMetricsDefault];
    }
    //
    UIBarButtonItem *appearance = [UIBarButtonItem appearance];
    NSMutableDictionary *textAttrs = [NSMutableDictionary dictionary];
    textAttrs[NSForegroundColorAttributeName] = [UIColor blackColor];
    textAttrs[NSFontAttributeName] = [UIFont systemFontOfSize:15];
    [appearance setTitleTextAttributes:textAttrs forState:UIControlStateNormal];
}


- (void)CheckversionUpdate {
    [[Harpy sharedInstance] setPresentingViewController:_window.rootViewController];
    
    // (Optional) Set the Delegate to track what a user clicked on, or to use a custom UI to present your message.
    [[Harpy sharedInstance] setDelegate:self];
    
    // (Optional) When this is set, the alert will only show up if the current version has already been released for X days.
    // By default, this value is set to 1 (day) to avoid an issue where Apple updates the JSON faster than the app binary propogates to the App Store.
    //    [[Harpy sharedInstance] setShowAlertAfterCurrentVersionHasBeenReleasedForDays:3];
    
    // (Optional) The tintColor for the alertController
    //    [[Harpy sharedInstance] setAlertControllerTintColor:[UIColor purpleColor]];
    
    // (Optional) Set the App Name for your app
    //    [[Harpy sharedInstance] setAppName:@"iTunes Connect Mobile"];
    
    /* (Optional) Set the Alert Type for your app
     By default, Harpy is configured to use HarpyAlertTypeOption */
    [[Harpy sharedInstance] setAlertType:HarpyAlertTypeOption];
    
    /* (Optional) If your application is not available in the U.S. App Store, you must specify the two-letter
     country code for the region in which your applicaiton is available. */
    //    [[Harpy sharedInstance] setCountryCode:@"en-US"];
    
    /* (Optional) Overrides system language to predefined language.
     Please use the HarpyLanguage constants defined in Harpy.h. */
    //    [[Harpy sharedInstance] setForceLanguageLocalization:HarpyLanguageRussian];
    
    // Turn on Debug statements
    [[Harpy sharedInstance] setDebugEnabled:true];
    
    // Perform check for new version of your app
    [[Harpy sharedInstance] checkVersion];
    
    
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    UIAlertView *notificationAlert = [[UIAlertView alloc] initWithTitle:@"Notification" message:@"This local notification"
                                                               delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    [notificationAlert show];
    // NSLog(@"didReceiveLocalNotification");
}


- (void)applicationWillResignActive:(UIApplication *)application {
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    
}

- (BOOL)internetConnected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    BOOL connectionRequired = [reachability connectionRequired];
    
    if ((internetStatus == NotReachable) || connectionRequired) {
        return NO;
    } else {
        return YES;
    }
}

#pragma mark state preservation / restoration

- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder {
    return YES;
}

- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder {
    return YES;
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //[self.window makeKeyAndVisible];
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
}


- (void)showLoadingWithTitle:(NSString *)title {
    
}

- (void)hideLoadingView {
}

- (void)addShadowto:(UIView *)view {
    // drop shadow
}

//connected
- (BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return (networkStatus != NotReachable);
}

//shared appdelegate
+ (AppDelegate *)sharedAppDelegate {
    return (AppDelegate *) [[UIApplication sharedApplication] delegate];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    NSLog(@"Unable to register for remote notifications: %@", error);
}

#pragma mark - Class Methods


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = ([[FBSDKApplicationDelegate sharedInstance] application:application
                                                                   openURL:url
                                                         sourceApplication:sourceApplication
                                                                annotation:annotation
                     ] || [[GIDSignIn sharedInstance] handleURL:url
                                              sourceApplication:sourceApplication
                                                     annotation:annotation]);
    // Add any custom logic here.
    return handled;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
    
    return (
            [[FBSDKApplicationDelegate sharedInstance] application:application
                                                           openURL:url
                                                 sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                        annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
             ]
            ||
            [[GIDSignIn sharedInstance] handleURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]]
            );
}

#pragma mark - HarpyDelegate

- (void)harpyDidShowUpdateDialog {
    NSLog(@"%s", __FUNCTION__);
}

- (void)harpyUserDidLaunchAppStore {
    NSLog(@"%s", __FUNCTION__);
}

- (void)harpyUserDidSkipVersion {
    NSLog(@"%s", __FUNCTION__);
}

- (void)harpyUserDidCancel {
    NSLog(@"%s", __FUNCTION__);
}

- (void)harpyDidDetectNewVersionWithoutAlert:(NSString *)message {
    NSLog(@"%@", message);
}


- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    
}


#pragma mark - FIRMessagingDelegate

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
    
    
    device_tokenStr = fcmToken;
    
    [[NSUserDefaults standardUserDefaults] setObject:device_tokenStr forKey:@"device_Token"];
    
}

- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    NSLog(@"Received data message: %@", remoteMessage.appData);
}

#pragma mark - UNUserNotificationCenterDelegate


- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionSound);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    NSString *dataString = userInfo[@"data"];
    //    if (dataString ==)
    
    
    if (dataString != (id) [NSNull null] && dataString.length != 0 && ![dataString isEqualToString:@""]) {
        
        NSLog(@"%@", dataString);
        
    }
    
    completionHandler();
}



@end
