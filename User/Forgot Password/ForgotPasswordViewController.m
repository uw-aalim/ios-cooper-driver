//
//  ForgotPasswordViewController.m
//  User
//
//  Created by iCOMPUTERS on 12/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "EmailViewController.h"
#import "CSS_Class.h"
#import "config.h"
#import "Constant.h"
#import "AFNHelper.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import "Provider-Swift.h"
#import "UIViewController+CoOper-Objc.h"


@interface ForgotPasswordViewController ()
{
    AppDelegate *appDelegate;
    NSString *emailString, *emailAddressStr, *idStr, *strEmail;
}

@end

@implementation ForgotPasswordViewController

+(instancetype)initController {
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ForgotPassword" bundle: nil];
	ForgotPasswordViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
	return viewController;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self setDesignStyles];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapReceived:)];
    [tapGestureRecognizer setDelegate:self];
    [self.view addGestureRecognizer:tapGestureRecognizer];
	
	if (self.emailAddressString != NULL) {
		self.emailAddress.text = self.emailAddressString;
		[self validateEmailAddress];
	}
	
	self.scrollView.delegate = self;
	
    //strEmail=appDelegate.strEmail;
    //_emailAddress.text=strEmail;
    //[_emailAddress becomeFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    //self.screenName = @"Forget password";
    [self LocalizationUpdate];
}


-(void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillChangeFrame:)
												 name:UIKeyboardWillShowNotification
											   object:NULL];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillChangeFrame:)
												 name:UIKeyboardWillChangeFrameNotification
											   object:NULL];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillHide:)
												 name:UIKeyboardWillHideNotification
											   object:NULL];
}

-(void) viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)LocalizationUpdate{
    //_helpLbl.text = LocalizedString(@"Enter the Password");
    //_confirmPassLbl.text = LocalizedString(@"Confirm Password");
    //_otpLbl.text = LocalizedString(@"Otp");
    //_passLbl.text = LocalizedString(@"New Password");
    //_emailLbl.text = LocalizedString(@"Email");
    //[_changePasswordBtn setTitle:LocalizedString(@"Reset password") forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tapReceived:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.view endEditing:YES];
}

-(void)setDesignStyles
{
  //  [CSS_Class APP_labelName:_helpLbl];
   // [CSS_Class APP_textfield_Outfocus:_emailAddress];
    /*[CSS_Class APP_textfield_Outfocus:_passText];
    [CSS_Class APP_textfield_Outfocus:_confirmPassText];
    [CSS_Class APP_textfield_Outfocus:_OtpText];

    [CSS_Class APP_labelName_Small:_passLbl];
    [CSS_Class APP_labelName_Small:_confirmPassLbl];
    [CSS_Class APP_Blackbutton:_changePasswordBtn];*/
}

-(IBAction)backBtn:(id)sender
{
   // EmailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EmailViewController"];
    //[self.navigationController pushViewController:controller animated:YES];
	[self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_emailAddress)
    {
        [self validateEmailAddress];
    }
    else if(textField==_passText)
    {
        [_confirmPassText becomeFirstResponder];
    }
    else if(textField==_confirmPassText)
    {
        [_confirmPassText resignFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField == _passText)
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= PASSWORDLENGTH || returnKey;
    }
    else
    {
        return YES;
    }
    
    return NO;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [CSS_Class APP_textfield_Infocus:textField];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField == _emailAddress)
    {
        [self validateEmailAddress];
    }
    [CSS_Class APP_textfield_Outfocus:textField];
    return YES;
}

-(void)validateEmailAddress
{
    [self.view endEditing:YES];
    emailString = @"true";
    
    if(_emailAddress.text.length != 0)
    {
        NSString *email = _emailAddress.text;
        NSString *emailRegEx =
        @"(?:[A-Za-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
        @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
        @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
        @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
        @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
        @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
        @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        
        NSPredicate *regExPredicate =
        [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:email];
        
        if(myStringMatchesRegEx)
        {
            emailString = @"true";
        }
        else
        {
            emailString = @"false";
        }
    }
    
    if(_emailAddress.text.length==0)
    {
        emailString = false;
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"EMAIL_REQ") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    else if ([emailString isEqualToString:@"false"])
    {
        emailString = false;
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"EMAIL_INVALID") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
	
    if ([emailString isEqualToString:@"true"])
    {
        NSDictionary *params=@{@"email":_emailAddress.text};
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        //[appDelegate onStartLoader];
        
        [afn getDataFromPath:MD_FORGOTPASSWORD withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            //[appDelegate onEndLoader];
            if (response) {
                
                NSDictionary *dict = [response valueForKey:@"user"];
                
				self->emailAddressStr = [dict valueForKey:@"email"];
                self->idStr = [dict valueForKey:@"id"];
				self->strOtp=[NSString stringWithFormat:@"%@", [dict valueForKey:@"otp"]];
                [self->_OtpText becomeFirstResponder];
				
				//[self showAlertView]
                [self showAlertviewControllerWithTitle:@"" messageAlert:[response valueForKey:@"message"]];
            }
            else
            {
                if ([errorcode intValue]==1)
                {
                    [self showAlertviewControllerWithTitle:@"" messageAlert:LocalizedString(@"ERRORMSG") ];
                }
                //else if([errorcode  intValue]==3)
               // {
                    //[self logoutMethod];
                    
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
//                    
//                    ViewController *logout = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//                    [self.navigationController pushViewController:logout animated:YES];
              //  }
                else{
                    if ([error objectForKey:@"email"]) {
                        [self showAlertviewControllerWithTitle:@"" messageAlert:[[error objectForKey:@"email"] objectAtIndex:0]];
						self->_emailAddress.text =@"";
						[self->_emailAddress becomeFirstResponder];
                    }
                    else {
                        [self showAlertviewControllerWithTitle:@""  messageAlert:LocalizedString(@"ERRORMSG")];
                    }
                    
                }
            }
        }];
        
    }
}

/*
-(void)logoutMethod
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn refreshMethod_NoLoader:MD_REFRESH_TOKEN withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
                [user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
                [user setValue:response[@"refresh_token"] forKey:UD_REFERSH_TOKEN];
            }
            else
            {
                [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}
*/


/*
-(void)refreshMethod
{
	AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
	[afn refreshMethod_NoLoader:
	 @""withBlock:^(id responseObject, NSDictionary *error, NSString *errorcode) {
		 
		 if (responseObject)
		 {
			 NSLog(@"Refresh Method ...%@", responseObject);
			 NSString *access_token = [responseObject valueForKey:@"access_token"];
			 NSString *first_name = [responseObject valueForKey:@"first_name"];
			 NSString *avatar =[Utilities removeNullFromString:[responseObject valueForKey:@"avatar"]];
			 NSString *status =[Utilities removeNullFromString:[responseObject valueForKey:@"status"]];
			 NSString *currencyStr=[responseObject valueForKey:@"currency"];
			 NSString *socialId=[Utilities removeNullFromString:[responseObject valueForKey:@"social_unique_id"]];
			 NSString *last_name = [responseObject valueForKey:@"last_name"];
			 
			 
			 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			 [defaults setObject:last_name forKey:@"last_name"];
			 [defaults setObject:access_token forKey:@"access_token"];
			 [defaults setObject:first_name forKey:@"first_name"];
			 [defaults setObject:avatar forKey:@"avatar"];
			 [defaults setObject:status forKey:@"status"];
			 [defaults setValue:currencyStr forKey:@"currency"];
			 [defaults setValue:socialId forKey:@"social_unique_id"];
			 [defaults setObject:[responseObject valueForKey:@"id"] forKey:@"id"];
			 [defaults setObject:[responseObject valueForKey:@"sos"] forKey:@"sos"];
		 }
		 else
		 {
			 [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
			 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
			 
			 WelcomeViewController *viewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];
			 
			 [self.navigationController pushViewController:wallet animated:YES];
		 }
	 }];
}*/




-(IBAction)Nextbtn:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *otp = [NSString stringWithFormat:@"%@", _OtpText.text];
   
    if(_emailAddress.text.length==0)
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:NSLocalizedString(@"EMAIL_PWD", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    else if(_passText.text.length==0)
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"NEW_PWD") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    else if(_confirmPassText.text.length==0)
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CON_PWD") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    else if(![_passText.text isEqualToString:_confirmPassText.text])
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"MATCH_PWD") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if ([_OtpText.text isEqualToString:@""])
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"Please enter the Password Recovery Code you were sent.") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if (![otp isEqualToString:strOtp])
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"You entered wrong Password Recovery Code.") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        NSDictionary *params=@{@"email":emailAddressStr,@"password":_passText.text,@"password_confirmation":_confirmPassText.text, @"id":idStr};
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        //[appDelegate onStartLoader];
        
        [afn getDataFromPath:MD_RESETPASSWORD withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            // [appDelegate onEndLoader];
            if (response) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"PWD_CHD", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok=[UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isLoggedin"];
                    
					//	Pop back to previous view controller to login with your new password
					NSUInteger currentViewControllersCount = self.navigationController.viewControllers.count;
					if ((currentViewControllersCount - 2) > 0) {
						LoginViewController *viewController = (LoginViewController *)self.navigationController.viewControllers[(currentViewControllersCount - 2)];
						viewController.emailTextField.text = self.emailAddress.text;
						[self.navigationController popViewControllerAnimated:YES];
					}
                    
                }];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
            }
            else {
                if ([errorcode intValue]==1) {
                    [self showAlertviewControllerWithTitle:@"" messageAlert:LocalizedString(@"ERRORMSG")];
                }
                //else if([errorcode  intValue]==3) {
                    //[self logoutMethod];
                    
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
//                    
//                    ViewController *logout = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//                    [self.navigationController pushViewController:logout animated:YES];
               // }
                else{
                    if ([error objectForKey:@"old_password"]) {
                        [self showAlertviewControllerWithTitle:@"" messageAlert:[[error objectForKey:@"old_password"] objectAtIndex:0]];
                    }
                    else if ([error objectForKey:@"password"]) {
                        [self showAlertviewControllerWithTitle:@"" messageAlert:[[error objectForKey:@"password"] objectAtIndex:0]];
                    }
                    else if ([error objectForKey:@"password_confirmation"]) {
                        [self showAlertviewControllerWithTitle:@"" messageAlert:[[error objectForKey:@"password_confirmation"] objectAtIndex:0]];
                    }
                   
                }
            }
        }];
     
    }
}




//	============================================================================================================
//	MARK:- Keyboard Responders
//	============================================================================================================

-(void) keyboardWillChangeFrame: (NSNotification *) notification {
	
	CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
	double duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	
	self.scrollViewBottom.constant = keyboardFrame.size.height;
	
	[UIView animateWithDuration:duration animations:^{
		[[self view] layoutIfNeeded];
	} completion:^(BOOL finished) {
		//Finishd keyboard animateion
	}];
}

-(void) keyboardWillHide: (NSNotification *) notification {
	
	double duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	
	self.scrollViewBottom.constant = 0;
	
	[UIView animateWithDuration:duration animations:^{
		[[self view] layoutIfNeeded];
	} completion:^(BOOL finished) {
		//Finishd keyboard animateion
	}];
}





//	============================================================================================================
//	MARK:- Scroll View Delegate
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
	
	
	//	Automatically push the large title beneath the title bar
	if (scrollView.contentOffset.y >= 0.0) {
		if (scrollView.contentOffset.y <= 44.0) {
			self.largeTitleViewTop.constant = -(scrollView.contentOffset.y);
		}
		else {
			self.largeTitleViewTop.constant = -44.0;
		}
	}
	else {
		self.largeTitleViewTop.constant = 0;
	}
	
	//	Start showing the new title bar label when we have scrolled at least 12 points
	if (scrollView.contentOffset.y > 12) {
		CGFloat alpha = (scrollView.contentOffset.y - 12) / 32.0;
		if (alpha > 1.0) {
			self.titleBarLabel.alpha = 1.0;
		}
		else if (alpha < 0.0) {
			self.titleBarLabel.alpha = 0.0;
		}
		else {
			self.titleBarLabel.alpha = alpha;
		}
	}
	else {
		self.titleBarLabel.alpha = 0.0;
	}
}
//	------------------------------------------------------------------------------------------------------------

@end
