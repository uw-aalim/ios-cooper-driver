//
//  ViewController.m
//  User
//
//  Created by iCOMPUTERS on 12/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "ViewController.h"
#import "SocailMediaViewController.h"
#import "EmailViewController.h"
#import "CSS_Class.h"
#import "config.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setDesignStyles];
    [self getBatteryStatus];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self ViewLocalizationupdate];
}
-(void)ViewLocalizationupdate{
    _titleLbl.text = LocalizedString(@"Get moving with coOper Driver");
    _socialLbl.text = LocalizedString(@"or connect with social");
    _emailLbl.text = LocalizedString(@"Enter your mail ID");
    
}
-(void) getBatteryStatus
{
    UIDevice *myDevice = [UIDevice currentDevice];
    [myDevice setBatteryMonitoringEnabled:YES];
    
    int state = [myDevice batteryState];
    NSLog(@"battery status: %d",state); // 0 unknown, 1 unplegged, 2 charging, 3 full
    
    double batLeft = (float)[myDevice batteryLevel] * 100;
    NSLog(@"battery left: %f", batLeft);
    
    if ((batLeft < 20) && state ==1)
    {
        NSLog(@"you need to charge");
        [self startLocalNotification];
    }
    else
    {
        NSLog(@"Need not to charge");
    }
}

-(void)startLocalNotification {  // Bind this method to UIButton action
    NSLog(@"startLocalNotification");
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:7];
    notification.alertBody = LocalizedString(@"Your battery power is less to continue, Please plug it to get the request");
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.soundName = UILocalNotificationDefaultSoundName;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setDesignStyles
{
    [CSS_Class APP_SocialLabelName:_socialLbl];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_xuberView.bounds];
    _xuberView.layer.masksToBounds = NO;
    _xuberView.layer.shadowColor = [UIColor blackColor].CGColor;
    _xuberView.layer.shadowOffset = CGSizeMake(0.3f, 0.3f);
    _xuberView.layer.shadowOpacity = 0.3f;
    _xuberView.layer.shadowPath = shadowPath.CGPath;
    _xuberView.layer.cornerRadius = 5.0f;
}

-(IBAction)socialbtn:(id)sender
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

    SocailMediaViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"SocailMediaViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

-(IBAction)emailBtn:(id)sender
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

    EmailViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"EmailViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}


@end
