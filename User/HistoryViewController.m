//
//  HistoryViewController.m
//  Provider
//
//  Created by iCOMPUTERS on 17/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "HistoryViewController.h"
#import "CSS_Class.h"
#import "config.h"
#import "Colors.h"
#import "ViewController.h"
#import "AFNHelper.h"
#import "Utilities.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "Provider-Swift.h"


@interface HistoryViewController ()

@end

@implementation HistoryViewController

- (void)viewDidLoad
{
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    app_Name = NSLocalizedString(@"APPNAME", nil);
    [super viewDidLoad];
    [self setDesignStyles];
    
    if ([_historyHintStr isEqualToString:@"UPCOMING"])
    {
        [_commentsView setHidden:YES];
        [_cashLb setHidden:YES];
    }
    
    [self getHistoryDetails];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapReceived:)];
    [tapGestureRecognizer setDelegate:self];
    [self.view addGestureRecognizer:tapGestureRecognizer];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self LocalizationUpdate];
    
}

-(void)LocalizationUpdate{
    _headerLbl.text = LocalizedString(@"History");
    _paymentLb.text = LocalizedString(@"Payment method");
    _commentTitleLb.text = LocalizedString(@"Comments");
    
}

-(void)tapReceived:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [UIView animateWithDuration:0.45 animations:^{
        
        _invoiceView.frame = CGRectMake(0, self.view.frame.size.height +30, self.view.frame.size.width,  300);
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setDesignStyles
{
    [CSS_Class APP_fieldValue_Small:_dateLb];
    [CSS_Class APP_SmallText:_timeLb];
    [CSS_Class APP_fieldValue:_nameLb];
    [CSS_Class APP_labelName:_paymentLb];
    [CSS_Class APP_labelName:_commentTitleLb];
    [CSS_Class APP_fieldValue_Small:_payTypeLb];
    [CSS_Class APP_fieldValue:_cashLb];
    [_timeLb setTextColor:TEXTCOLOR_LIGHT];
    
    [CSS_Class APP_Blackbutton:_callBtn];
    [CSS_Class APP_Blackbutton:_cancelBtn];
  //  [CSS_Class APP_Blackbutton:_receiptBtn];
    
    [CSS_Class APP_labelName:_lblBacePrice];
    [CSS_Class APP_labelName:_lblTaxPrice];
    [CSS_Class APP_labelName:_lblDistance];
    [CSS_Class APP_labelName:_invoice_discountAmt];
    [CSS_Class APP_labelName:_invoice_WalletAmt];
    [CSS_Class APP_fieldValue:_lblTotalAmt];
    
    [CSS_Class APP_SmallText:_pickLb];
    [CSS_Class APP_SmallText:_dropLb];
    [CSS_Class APP_SmallText:_commentsLb];
    
    _userImg.layer.cornerRadius=_userImg.frame.size.height/2;
    _userImg.clipsToBounds=YES;
}

-(IBAction)backBtn:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getHistoryDetails
{
    if([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        NSDictionary *params = @{@"request_id":_request_idStr};
        
        NSString *serviceStr;
        
        if([_historyHintStr isEqualToString:@"PAST"])
        {
            serviceStr = HISTORY_DETAILS;
        }
        else
        {
            serviceStr = UPCOMING_HISTORYDETAILS;
        }
        
        [afn getDataFromPath:serviceStr withParamData:params withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
         {
             if (response)
             {
                 NSLog(@"HISTORY RESPONSE ...%@", response);
                 NSArray *arrLocal=response;
                 if (arrLocal.count!=0)
                 {
                     NSDictionary *dictVal = [arrLocal objectAtIndex:0];
                     NSString *strVal=[dictVal valueForKey:@"static_map"];
                     id_Str=[dictVal valueForKey:@"id"];

                      _invoiceIdLbl.text=[NSString stringWithFormat:@"%@ - %@",LocalizedString(@"INVOICE ID"),[Utilities removeNullFromString:[dictVal valueForKey:@"booking_id"]]];
                     
                     NSString *escapedString =[strVal stringByReplacingOccurrencesOfString:@"%7C" withString:@"|"];
                     NSURL *mapUrl = [NSURL URLWithString:[escapedString stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
                     _mapImg.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:mapUrl]];
                     
                     _dateLb.text =[Utilities convertDateTimeToGMT:[dictVal valueForKey:@"assigned_at"]];
                     _timeLb.text=[Utilities  convertTimeFormat:[dictVal valueForKey:@"assigned_at"]];
                     _bookingIdLbl.text =[Utilities  removeNullFromString:[dictVal valueForKey:@"booking_id"]];
                     _dropLb.text =[Utilities removeNullFromString:[dictVal valueForKey:@"d_address"]];
                     _pickLb.text =[Utilities  removeNullFromString:[dictVal valueForKey:@"s_address"]];
                     
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     NSString *currencyStr = [defaults valueForKey:@"currency"];
                     
                     if (![[dictVal valueForKey:@"payment"] isKindOfClass:[NSNull class]])
                     {
                         if([_historyHintStr isEqualToString:@"PAST"])
                         {
                             _cashLb.text= [NSString stringWithFormat:@"%@%@", currencyStr, [dictVal[@"payment"] valueForKey:@"total"]];
                             _lblTotalAmt.text= [NSString stringWithFormat:@"%@%@", currencyStr, [dictVal[@"payment"] valueForKey:@"total"]];
                             
                             _lblBacePrice.text= [NSString stringWithFormat:@"%@%@", currencyStr, [dictVal[@"payment"] valueForKey:@"fixed"]];
                             _lblDistance.text= [NSString stringWithFormat:@"%@%@", currencyStr, [dictVal[@"payment"] valueForKey:@"distance"]];
                             _lblTaxPrice.text= [NSString stringWithFormat:@"%@%@", currencyStr, [dictVal[@"payment"] valueForKey:@"tax"]];
                             _invoice_WalletAmt.text= [NSString stringWithFormat:@"%@%@", currencyStr, [dictVal[@"payment"] valueForKey:@"wallet"]];
                             _invoice_discountAmt.text= [NSString stringWithFormat:@"%@%@", currencyStr, [dictVal[@"payment"] valueForKey:@"discount"]];
                             
                             [self.receiptBtn setHidden:NO];
                             [self.bottomView setHidden:YES];
                         }
                         else
                         {
                             _cashLb.text= [NSString stringWithFormat:@"%@0.00", currencyStr];
                             _dateLb.text =[Utilities convertDateTimeToGMT:[dictVal valueForKey:@"schedule_at"]];
                             _timeLb.text=[Utilities  convertTimeFormat:[dictVal valueForKey:@"schedule_at"]];
                             
                             [self.bottomView setHidden:NO];
                             [self.receiptBtn setHidden:YES];
                         }
                     }
                     else
                     {
                         [_cashLb setText:[NSString stringWithFormat:@"%@0.00", currencyStr]];
                     }
                     
                     if (![[dictVal valueForKey:@"user"] isKindOfClass:[NSNull class]])
                     {
                         strProviderCell =[dictVal[@"user"] valueForKey:@"mobile"];

                         [_nameLb setText:[[dictVal valueForKey:@"user"] valueForKey:@"first_name"]];
                         _rating_user.value=[[[dictVal valueForKey:@"user"] valueForKey:@"rating"]floatValue];
                         [_payTypeLb setText:[[dictVal valueForKey:@"user"] valueForKey:@"payment_mode"]];
                         NSString *imageUrl = [Utilities removeNullFromString:[[dictVal valueForKey:@"user"] valueForKey:@"picture"]];
                         
                         if ([imageUrl containsString:@"http"])
                         {
                             imageUrl = [NSString stringWithFormat:@"%@",[dictVal[@"user"] valueForKey:@"picture"]];
                         }
                         else
                         {
                             imageUrl = [NSString stringWithFormat:@"%@/storage/%@",SERVICE_URL, [dictVal[@"user"] valueForKey:@"picture"]];
                         }
                         
                         [ _userImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                                      placeholderImage:[UIImage imageNamed:@"userProfile"]];
                         
                     }
                     else
                     {
                         //                         [_cashLb setText:@"0"];
                     }
                     if (![[dictVal valueForKey:@"rating"] isKindOfClass:[NSNull class]])
                     {
                         if ([[dictVal[@"rating"] valueForKey:@"provider_comment"] isEqualToString:@""])
                         {
                             _commentsLb.text=@"no comments";
                         }
                         else
                         {
                             _commentsLb.text=[dictVal[@"rating"] valueForKey:@"provider_comment"];
                         }
                     }
                 }
                 else
                 {
                     
                 }
             }
             else
             {
                 if ([strErrorCode intValue]==1)
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
                 else if ([strErrorCode intValue]==2)
                 {
                     if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                     {
                         //Refresh token
                     }
                     else
                     {
//                         [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
//                         ViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//                         [self.navigationController pushViewController:wallet animated:YES];
                         [self refreshMethod];
                     }
                 }
                 else
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
                 NSLog(@"%@",error);
                 
             }
         }];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)refreshMethod
{
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn refreshMethod_NoLoader:
     @""withBlock:^(id responseObject, NSDictionary *error, NSString *errorcode) {
         
         if (responseObject)
         {
             NSLog(@"Refresh Method ...%@", responseObject);
             NSString *access_token = [responseObject valueForKey:@"access_token"];
             NSString *first_name = [responseObject valueForKey:@"first_name"];
             NSString *avatar =[Utilities removeNullFromString:[responseObject valueForKey:@"avatar"]];
             NSString *status =[Utilities removeNullFromString:[responseObject valueForKey:@"status"]];
             NSString *currencyStr=[responseObject valueForKey:@"currency"];
             NSString *socialId=[Utilities removeNullFromString:[responseObject valueForKey:@"social_unique_id"]];
             NSString *last_name = [responseObject valueForKey:@"last_name"];
             
             
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setObject:last_name forKey:@"last_name"];
             [defaults setObject:access_token forKey:@"access_token"];
             [defaults setObject:first_name forKey:@"first_name"];
             [defaults setObject:avatar forKey:@"avatar"];
             [defaults setObject:status forKey:@"status"];
             [defaults setValue:currencyStr forKey:@"currency"];
             [defaults setValue:socialId forKey:@"social_unique_id"];
             [defaults setObject:[responseObject valueForKey:@"id"] forKey:@"id"];
             [defaults setObject:[responseObject valueForKey:@"sos"] forKey:@"sos"];
         }
         else
         {
             [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
			 
			 /*
              UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
             PageViewController * wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];

             [self.navigationController pushViewController:wallet animated:YES];
			 */
			 WelcomeViewController *viewController = [WelcomeViewController initController];
			 UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
			 [self.navigationController presentViewController:navigationController animated:YES completion:^{
				 NSLog(@"Presented welcome view controller");
			 }];
         }
     }];
}


-(IBAction)callBtn:(id)sender
{
    if ([strProviderCell isEqualToString:@""])
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert") message:LocalizedString(@"Driver was not provided the number to call.") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:strProviderCell]];
        NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:strProviderCell]];
        
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl])
        {
            [UIApplication.sharedApplication openURL:phoneUrl options:@{} completionHandler:nil];
        }
        else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl])
        {
            [UIApplication.sharedApplication openURL:phoneFallbackUrl options:@{} completionHandler:nil];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert") message:LocalizedString(@"Your device does not support calling") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];    }
    }
}

-(IBAction)cancelActionBtn:(id)sender
{
    if ([appDelegate internetConnected])
    {
        NSDictionary *para = @{@"id": id_Str};
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:CANCEL_REQUEST  withParamData:para withBlock:^(id response, NSDictionary *error, NSString *strErrorCode)
         {
             if (response)
             {
                 NSLog(@"CANCEL RESPONSE...%@", response);
                 
                 [self backBtn:self];
             }
             else
             {
                 if ([strErrorCode intValue]==1)
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
                 else if ([strErrorCode intValue]==2)
                 {
                     if ([[error valueForKey:@"error"] isEqualToString:@"token_expired"])
                     {
                         //Refresh token
                     }
                     else
                     {
                         [self refreshMethod];
                         
                         //                         [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
                         //                         ViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                         //                         [self.navigationController pushViewController:wallet animated:YES];
                     }
                 }
                 else
                 {
                     [CSS_Class alertviewController_title:LocalizedString(@"ERROR") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                 }
                 NSLog(@"%@",error);
                 
             }
             
         }];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


-(IBAction)receiptBtn:(id)sender
{
    [UIView animateWithDuration:0.45 animations:^{
        
        _invoiceView.frame = CGRectMake(0, self.view.frame.size.height -300, self.view.frame.size.width,  300);
    }];
}

@end
