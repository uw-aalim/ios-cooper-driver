//
//  SocialMediaViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-14.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class SocialMediaViewController2: UIViewController {

	//	============================================================================================================
	//	MARK:- Constants
	//	============================================================================================================
	
	let termsURLString = ""
	let privacyURLString = ""
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	@IBOutlet weak var titleBarLabel: UILabel!
	@IBOutlet weak var largeTitleViewTop: NSLayoutConstraint!
	
	//	The view which contains the scroll view
	@IBOutlet weak var scrollContainerView: UIView!
	//	The scroll view
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var scrollViewBottom: NSLayoutConstraint!
	
	@IBOutlet weak var travelSplashImageView: UIImageView!
	
	@IBOutlet weak var termsAndPrivacyTextView: UITextView!
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Controller Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	class func initController() -> SocialMediaViewController2 {
		return SocialMediaViewController2(
			nibName: "SocialMediaViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Loading Appearance and Dissapearance
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.scrollView.delegate = self
		
		self.configureViewLabels()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidAppear(
		_ animated: Bool) {
		super.viewDidAppear(animated)
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(SocialMediaViewController2.loginRegistrationFinishedSuccess(_:)),
			name: CoOperNotification.loginRegisterProcessSuccess,
			object: nil)
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(SocialMediaViewController2.loginRegistrationFinishedFailed(_:)),
			name: CoOperNotification.loginRegisterProcessFailed,
			object: nil)
		
		self.navigationController?.interactivePopGestureRecognizer?.delegate = self
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		NotificationCenter.default.removeObserver(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	//	============================================================================================================
	//	MARK: View Load Configuring
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func configureViewLabels() {
		
		let textString = self.termsAndPrivacyTextView.text! as NSString
		let termsRange = textString.range(of: "Terms and Conditions")
		let privacyRange = textString.range(of: "Privacy Policy")
		//let range = textString.range(of: textString as String)
		let attributedString = NSMutableAttributedString(string: textString as String)
		attributedString.addAttribute(.link, value: self.termsURLString, range: termsRange)
		attributedString.addAttribute(.link, value: self.privacyURLString, range: privacyRange)
		//attributedString.addAttribute(.link, value: UIFont.koindFontRegular(14.0)!, range: range)
		//attributedString.addAttribute(NSFontAttributeName, value: UIFont.koindFontRegular(12.0))
		attributedString.addAttribute(.foregroundColor, value: UIColor.tealColour(), range: termsRange)
		attributedString.addAttribute(.foregroundColor, value: UIColor.tealColour(), range: privacyRange)
		
		self.termsAndPrivacyTextView.text = ""
		
		self.termsAndPrivacyTextView.tintColor = UIColor.tealColour()
		self.termsAndPrivacyTextView.attributedText = attributedString
		self.termsAndPrivacyTextView.textAlignment = .center
		self.termsAndPrivacyTextView.isUserInteractionEnabled = true
		self.termsAndPrivacyTextView.delegate = self
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Layout
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		//	Whenever the subviews are layed out it is possible that it may be necessary to hide or show the
		//	image views depending on the height of the new layout.
		self.checkHeightForImageViews(
			withHeight: self.view.frame.height)
		
		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewWillTransition(
		to size: CGSize,
		with coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransition(to: size, with: coordinator)
		
		/*
		Call into the check height method to make sure that during transitions from one size class to another
		that the view correctly hides image views.
		*/
		self.checkHeightForImageViews(
			withHeight: size.height)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func checkHeightForImageViews(
		withHeight height: CGFloat) {
		
		
		if height <= 568 {
			//	On an device with iPhone SE height the travelSplashImageView
			self.travelSplashImageView.isHidden = true
		}
		else {
			//	On all other device heights hide the travelSplashImageView
			self.travelSplashImageView.isHidden = false
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Action Responders
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func backButtonPressed(sender: AnyObject) {
		let _ = self.navigationController?.popViewController(animated: true)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func facebookButtonPressed(sender: AnyObject) {
		
		//	Attempt to login using the facebook login api
		LoginRegisterService.sharedInstance().loginWithFacebook(in: self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func googleButtonPressed(sender: AnyObject) {
		
		//	Attempt to login using the google login api
		LoginRegisterService.sharedInstance().loginGoogle(in: self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func loginButtonPressed(sender: AnyObject) {
		
		//	Push into signup view controller
		let loginViewController = LoginViewController.initController()
		self.navigationController?.pushViewController(loginViewController, animated: true)
	}
	//	------------------------------------------------------------------------------------------------------------


}
