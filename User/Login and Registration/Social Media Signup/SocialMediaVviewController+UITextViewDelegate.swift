//
//  SocialMediaVviewController+UITextViewDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-14.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

//	============================================================================================================
//	MARK:- UITextViewDelegate
//	============================================================================================================

extension SocialMediaViewController2: UITextViewDelegate {
	
	//	------------------------------------------------------------------------------------------------------------
	func textView(
		_ textView: UITextView,
		shouldInteractWith URL: URL,
		in characterRange: NSRange) -> Bool {
		
		if (URL.absoluteString == self.termsURLString) {
			if #available(iOS 10.0, *) {
				UIApplication.shared.open(URL, options: [:], completionHandler: nil)
			} else {
				UIApplication.shared.openURL(URL)
			}
		}
		else if (URL.absoluteString == self.privacyURLString) {
			if #available(iOS 10.0, *) {
				UIApplication.shared.open(URL, options: [:], completionHandler: nil)
			} else {
				UIApplication.shared.openURL(URL)
			}
		}
		return false
	}
	//	------------------------------------------------------------------------------------------------------------
}
