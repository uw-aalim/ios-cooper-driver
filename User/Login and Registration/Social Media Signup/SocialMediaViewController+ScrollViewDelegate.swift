//
//  SocialMediaViewController+ScrollViewDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-14.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension SocialMediaViewController2: UIScrollViewDelegate {
	
	//	============================================================================================================
	//	MARK:- UIScrollViewDelegate
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func scrollViewDidScroll(
		_ scrollView: UIScrollView) {
		
		//	Automatically push the large title beneath the title bar
		if scrollView.contentOffset.y >= 0.0 {
			if scrollView.contentOffset.y <= 44.0 {
				self.largeTitleViewTop.constant = -(scrollView.contentOffset.y)
			}
			else {
				self.largeTitleViewTop.constant = -44.0
			}
		}
		else {
			self.largeTitleViewTop.constant = 0
		}
		
		//	Start showing the new title bar label when we have scrolled at least 12 points
		if scrollView.contentOffset.y > 12 {
			let alpha = (scrollView.contentOffset.y - 12) / 32.0
			if alpha > 1.0 {
				self.titleBarLabel.alpha = 1.0
			}
			else if alpha < 0.0 {
				self.titleBarLabel.alpha = 0.0
			}
			else {
				self.titleBarLabel.alpha = alpha
			}
		}
		else {
			self.titleBarLabel.alpha = 0.0
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
