//
//  LoginViewController+LoginNotifications.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension LoginViewController {
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func registrationFinishedSuccess(_ notification: Notification) {
		DispatchQueue.main.async {
			let mainStoryboard = UIStoryboard(name: "New", bundle: nil) //storyboardWithName:@"New" bundle: nil];
		//	let controller = mainStoryboard.instantiateViewController(withIdentifier: "newHomeController")
			//instantiateViewControllerWithIdentifier:@"newHomeController"];
			//self.navigationController pushViewController:controller animated:YES];
			self.navigationController?.presentingViewController?.dismiss(
				animated: true,
				completion: {
					//	May need to perform some login related functionality here
			})
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func registrationFinishedFailed(_ notification: Notification) {
		DispatchQueue.main.async {
			var error = "There was an error during the login process."
			if let userInfo = notification.userInfo,
				let errorMessage = userInfo["ERRORMSG"] as? String {
				
				error = errorMessage
			}
			self.showError(
				withTitle: "Login Error",
				andMessage: error)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
