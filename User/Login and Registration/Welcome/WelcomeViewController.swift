//
//  WelcomeViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-07.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Constants
	//	============================================================================================================
	
	let termsURLString = "www.trycooper.com/privacy"
	let privacyURLString = "www.trycooper.com/terms"
	
	let scrollFireTime = 6.0
	let cloudAnimationDuration = 4.5
	let leftCloudAnimationDelay = 0.0
	let rightCloudAnimationDelay = 2.75
	
	/**
	When the view is a compact state the clouds are half hidden by the edges of the view with a leading and
	trailing constraing value which is negative. This hides them until the animation pushes them slightly towards the
	centre.
	*/
	let compactNormalCloudLeadingTrailing: CGFloat = -92.0
	let regularNormalCloudLeadingTrailing: CGFloat = 92.0
	
	let compactAnimatedCloudLeadingTrailing: CGFloat = -76.0
	let regularAnimatedCloudLeadingTrailing: CGFloat = 120.0
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	@IBOutlet weak var pageControl: UIPageControl!
	@IBOutlet weak var scrollView: UIScrollView!
	
	@IBOutlet weak var leftCloudLeading: NSLayoutConstraint!
	@IBOutlet weak var rightCloudTrailing: NSLayoutConstraint!
	
	//	============================================================================================================
	//	MARK: Scroll View Pages
	//	============================================================================================================
	
	@IBOutlet weak var termsAndPrivacyTextView: UITextView!
	
	//	============================================================================================================
	//	MARK: General Proeprties
	//	============================================================================================================
	
	var scrollViewTimer: Timer?
	var leftCloudAnimating: Bool = false
	var rightCloudAnimating: Bool = false
	var endAllAnimations = false
	var normalCloudLeadingTrailing: CGFloat = -94.0
	var animatedCloudLeadingTrailing: CGFloat = -32.0
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Controller Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc class func initController() -> WelcomeViewController {
		return WelcomeViewController(
			nibName: "WelcomeViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Loading Appearance and Dissapearance
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.scrollView.delegate = self
		
		self.configureViewLabels()
    }
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewWillAppear(
		_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.navigationController?.navigationBar.isHidden = true 
		
		if self.traitCollection.horizontalSizeClass == .compact {
			self.leftCloudLeading.constant = self.compactNormalCloudLeadingTrailing
			self.rightCloudTrailing.constant = self.compactNormalCloudLeadingTrailing
			
			self.normalCloudLeadingTrailing = self.compactNormalCloudLeadingTrailing
			self.animatedCloudLeadingTrailing = self.compactAnimatedCloudLeadingTrailing
		}
		else {
			self.leftCloudLeading.constant = self.regularNormalCloudLeadingTrailing
			self.rightCloudTrailing.constant = self.regularNormalCloudLeadingTrailing
			
			self.normalCloudLeadingTrailing = self.regularNormalCloudLeadingTrailing
			self.animatedCloudLeadingTrailing = self.regularAnimatedCloudLeadingTrailing
		}
		//	Ensure the view gets layed out if necessary
		self.view.layoutIfNeeded()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidAppear(
		_ animated: Bool) {
		super.viewDidAppear(animated)
		
		print("WelcomeViewController - viewDidAppear")
		
		self.scrollViewTimer = Timer.scheduledTimer(
			timeInterval: self.scrollFireTime,
			target: self,
			selector: #selector(WelcomeViewController.scrollTimerDidFire(_:)),
			userInfo: nil,
			repeats: true)
		
		//	Run an animation that will move the clouds back and forth
		self.endAllAnimations = false
		self.runCloudAnimation(withConstraint: self.leftCloudLeading)
		self.runCloudAnimation(withConstraint: self.rightCloudTrailing)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidDisappear(
		_ animated: Bool) {
		super.viewDidDisappear(animated)
		
		self.scrollViewTimer?.invalidate()
		self.scrollViewTimer = nil
		
		self.endAllAnimations = true
		self.view.layer.removeAllAnimations()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	//	============================================================================================================
	//	MARK: View Load Configuring
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func configureViewLabels() {
		
		let textString = self.termsAndPrivacyTextView.text! as NSString
		let termsRange = textString.range(of: "Terms and Conditions")
		let privacyRange = textString.range(of: "Privacy Policy")
		//let range = textString.range(of: textString as String)
		let attributedString = NSMutableAttributedString(string: textString as String)
		attributedString.addAttribute(.link, value: self.termsURLString, range: termsRange)
		attributedString.addAttribute(.link, value: self.privacyURLString, range: privacyRange)
		//attributedString.addAttribute(.link, value: UIFont.koindFontRegular(14.0)!, range: range)
		//attributedString.addAttribute(NSFontAttributeName, value: UIFont.koindFontRegular(12.0))
		attributedString.addAttribute(.foregroundColor, value: UIColor.tealColour(), range: termsRange)
		attributedString.addAttribute(.foregroundColor, value: UIColor.tealColour(), range: privacyRange)
		
		self.termsAndPrivacyTextView.text = ""
		
		self.termsAndPrivacyTextView.tintColor = UIColor.tealColour()
		self.termsAndPrivacyTextView.attributedText = attributedString
		self.termsAndPrivacyTextView.textAlignment = .center
		self.termsAndPrivacyTextView.isUserInteractionEnabled = true
		self.termsAndPrivacyTextView.delegate = self
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func scrollTimerDidFire(
		_ sender: AnyObject) {
		
		//	When the scroll timer fires the scroll view should animate to a new content offset.
		//	Based on the current content offset.
		//	Make sure that it is not currently in motion due to a user event before scrolling.
		if !self.scrollView.isDragging &&
			!self.scrollView.isDecelerating &&
			!self.scrollView.isTracking &&
			!self.scrollView.isZoomBouncing {
			
			if self.scrollView.contentOffset.x < self.view.frame.size.width {
				
				let newContentOffset = CGPoint(
					x: self.view.frame.size.width,
					y: self.scrollView.contentOffset.y)
				self.scrollView.setContentOffset(newContentOffset, animated: true)
			}
			else if self.scrollView.contentOffset.x >= self.view.frame.size.width &&
				self.scrollView.contentOffset.x < (self.view.frame.size.width * 2) {
				
				let newContentOffset = CGPoint(
					x: (self.view.frame.size.width * 2),
					y: self.scrollView.contentOffset.y)
				self.scrollView.setContentOffset(newContentOffset, animated: true)
			}
			else if self.scrollView.contentOffset.x >= (self.view.frame.size.width * 2) &&
				self.scrollView.contentOffset.x < (self.view.frame.size.width * 3) {
				
				let newContentOffset = CGPoint(
					x: (self.view.frame.size.width * 3),
					y: self.scrollView.contentOffset.y)
				self.scrollView.setContentOffset(newContentOffset, animated: true)
			}
			else if self.scrollView.contentOffset.x >= (self.view.frame.size.width * 3) {
				
				let newContentOffset = CGPoint(
					x: 0,
					y: self.scrollView.contentOffset.y)
				self.scrollView.setContentOffset(newContentOffset, animated: true)
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Layout
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		//	Whenever the subviews are layed out it is possible that it may be necessary to hide or show the
		//	image views depending on the height of the new layout.
		self.checkHeightForImageViews(
			withHeight: self.view.frame.height)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewWillTransition(
		to size: CGSize,
		with coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransition(to: size, with: coordinator)
		
		/*
		Call into the check height method to make sure that during transitions from one size class to another
		that the view correctly hides image views.
		*/
		self.checkHeightForImageViews(
			withHeight: size.height)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	override func willTransition(
		to newCollection: UITraitCollection,
		with coordinator: UIViewControllerTransitionCoordinator) {
		super.willTransition(
			to: newCollection,
			with: coordinator)
		
		
		if newCollection.horizontalSizeClass == .compact {
			self.leftCloudLeading.constant = self.compactNormalCloudLeadingTrailing
			self.rightCloudTrailing.constant = self.compactNormalCloudLeadingTrailing
			
			self.normalCloudLeadingTrailing = self.compactNormalCloudLeadingTrailing
			self.animatedCloudLeadingTrailing = self.compactAnimatedCloudLeadingTrailing
		}
		else {
			self.leftCloudLeading.constant = self.regularNormalCloudLeadingTrailing
			self.rightCloudTrailing.constant = self.regularNormalCloudLeadingTrailing
			
			self.normalCloudLeadingTrailing = self.regularNormalCloudLeadingTrailing
			self.animatedCloudLeadingTrailing = self.regularAnimatedCloudLeadingTrailing
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func checkHeightForImageViews(
		withHeight height: CGFloat) {
		
		/*
		if height <= 480 {
			//	On an device with iPhone 4s height all images and titles are hidden in the paged scroll view.
			//self.carLogoImageView.isHidden = true
			//self.titleMessageLabel.isHidden = true
			//self.titleLabel.isHidden = true
			//self.usersImageView.isHidden = true
		}
		else if height <= 568 {
			//	On an device with iPhone SE height only the usersImageView and titleLabel on the first page of the
			//	paged scroll view are hidden.
			self.carLogoImageView.isHidden = false
			self.titleMessageLabel.isHidden = false
			self.titleLabel.isHidden = true
			self.usersImageView.isHidden = true
		}
		else {
			//	On all other device heights none of the titles or images are hidden on the paged scroll view.
			self.carLogoImageView.isHidden = false
			self.titleMessageLabel.isHidden = false
			self.titleLabel.isHidden = false
			self.usersImageView.isHidden = false
		} */
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func runCloudAnimation(
		withConstraint constraint: NSLayoutConstraint) {
		
		var animating = true
		var delay = self.leftCloudAnimationDelay
		if constraint == self.rightCloudTrailing {
			delay = self.rightCloudAnimationDelay
			animating = self.rightCloudAnimating
		}
		else {
			animating = self.leftCloudAnimating
		}
		
		//print("Run cloud animations")
		if !animating {
			
			if constraint == self.rightCloudTrailing {
				self.rightCloudAnimating = true
			}
			else {
				self.leftCloudAnimating = true
			}
			
			if constraint.constant > self.normalCloudLeadingTrailing {
				constraint.constant = self.normalCloudLeadingTrailing
			}
			else {
				constraint.constant = self.animatedCloudLeadingTrailing
			}
			
			UIView.animate(
				withDuration: self.cloudAnimationDuration,
				delay: delay,
				options: .curveEaseInOut,
				animations: {
					self.view.layoutIfNeeded()
			}) { (complete: Bool) in
				
				if constraint == self.rightCloudTrailing {
					self.rightCloudAnimating = false
				}
				else {
					self.leftCloudAnimating = false
				}
				
				if !self.endAllAnimations {
					self.runCloudAnimation(withConstraint: constraint)
				}
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Action Responders
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func loginButtonPressed(
		_ sender: AnyObject) {
		
		let logInViewController = LoginViewController.initController()
		self.navigationController?.pushViewController(logInViewController, animated: true)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func signupButtonPressed(
		_ sender: AnyObject) {
		
		let signUpViewController = SignUpViewController.initController()
		self.navigationController?.pushViewController(signUpViewController, animated: true)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func socialSignupPressed(
		_ sender: AnyObject) {
		let socialMediaViewController = SocialMediaViewController2.initController()
		self.navigationController?.pushViewController(socialMediaViewController, animated: true)
	}
	//	------------------------------------------------------------------------------------------------------------
}
