//
//  WelcomeViewController+ScrollViewDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-07.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension WelcomeViewController: UIScrollViewDelegate {
	
	//	============================================================================================================
	//	MARK:- UIScrollViewDelegate
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func scrollViewDidScroll(
		_ scrollView: UIScrollView) {
		
		if self.scrollView.contentOffset.x < self.view.frame.size.width {
			self.pageControl.currentPage = 0
		}
		else if self.scrollView.contentOffset.x >= self.view.frame.size.width &&
			self.scrollView.contentOffset.x < (self.view.frame.size.width * 2) {
			self.pageControl.currentPage = 1
		}
		else if self.scrollView.contentOffset.x >= (self.view.frame.size.width * 2) &&
			self.scrollView.contentOffset.x < (self.view.frame.size.width * 3) {
			self.pageControl.currentPage = 2
		}
		else if self.scrollView.contentOffset.x >= (self.view.frame.size.width * 3) {
			self.pageControl.currentPage = 3
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func scrollViewWillBeginDragging(
		_ scrollView: UIScrollView) {
		
		//	User is going to scroll the view, invalidate the timer so it doesn't fire when they are finished.
		self.scrollViewTimer?.invalidate()
		self.scrollViewTimer = nil
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func scrollViewDidEndDragging(
		_ scrollView: UIScrollView,
		willDecelerate decelerate: Bool) {
		
		//	If the view isn't going to decelerate, user dropped it perfectly in place.
		if !decelerate {
			//	If the scroll view timer is nil after a drag event then it needs to be re-enabled.
			if self.scrollViewTimer == nil {
				//	Reneable the scroll timer.
				self.scrollViewTimer = Timer.scheduledTimer(
					timeInterval: self.scrollFireTime,
					target: self,
					selector: #selector(WelcomeViewController.scrollTimerDidFire(_:)),
					userInfo: nil,
					repeats: true)
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func scrollViewDidEndDecelerating(
		_ scrollView: UIScrollView) {
		
		//	If the scroll view timer is nil after a deceleration event then the user likely initiated a drag.
		if self.scrollViewTimer == nil {
			//	Reneable the scroll timer.
			self.scrollViewTimer = Timer.scheduledTimer(
				timeInterval: self.scrollFireTime,
				target: self,
				selector: #selector(WelcomeViewController.scrollTimerDidFire(_:)),
				userInfo: nil,
				repeats: true)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
