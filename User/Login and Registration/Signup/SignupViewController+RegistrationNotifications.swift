//
//  SignupViewController+RegistrationNotifications.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension SignUpViewController {
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func emailAddressValidationSuccess(_ notification: Notification) {
		
		DispatchQueue.main.async {
			LoginRegisterService.sharedInstance().registerAccount(
				withEmail: self.emailTextField.text!,
				withPassword: self.passwordTextField.text!,
				withFirstName: self.firstNameTextField.text!,
				withLastName: self.lastNameTextField.text!,
				in: self)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func registrationFinishedSuccess(_ notification: Notification) {
		DispatchQueue.main.async {
			print("Registration success - test")
			self.navigationController?.presentingViewController?.dismiss(
				animated: true,
				completion: {
					//	May need to perform some login related functionality here
			})
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func registrationFinishedFailed(_ notification: Notification) {
		DispatchQueue.main.async {
			var error = "There was an error during the registration process."
			if let userInfo = notification.userInfo,
				let errorMessage = userInfo["ERRORMSG"] as? String {
				
				error = errorMessage
			}
			self.showError(
				withTitle: "Registration Error",
				andMessage: error)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
