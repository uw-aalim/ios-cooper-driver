//
//  SignUpViewController+UILabelDelegate .swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-07.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

//	============================================================================================================
//	MARK:- UITextViewDelegate
//	============================================================================================================

extension SignUpViewController: UITextViewDelegate {
	
	//	------------------------------------------------------------------------------------------------------------
	func textView(
		_ textView: UITextView,
		shouldInteractWith URL: URL,
		in characterRange: NSRange) -> Bool {
		
		
		if (URL.absoluteString == self.termsURLString) {
			
			let viewController = CoOperWebViewController.initController()
			viewController.webAddressString = self.termsURLString
			viewController.titleString = "Terms & Conditions"
			self.navigationController?.pushViewController(viewController, animated: true)
		}
		else if (URL.absoluteString == self.privacyURLString) {
			//UIApplication.shared.open(URL, options: [:], completionHandler: nil)
			let viewController = CoOperWebViewController.initController()
			viewController.webAddressString = self.privacyURLString
			viewController.titleString = "Privacy"
			self.navigationController?.pushViewController(viewController, animated: true)
		}
		return false
	}
	//	------------------------------------------------------------------------------------------------------------
}
