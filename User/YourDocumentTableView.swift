import Foundation
import UIKit

class YourDocumentTableView: UITableView {
    
    var tableData = [Document]()
    let reuseIdentifier = "YourDocumentTableViewCell"
    let nibName = "YourDocumentTableView"
    let cellSpacingHeight:CGFloat = 10
    var parentVC: UIViewController? = nil
    
    
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    
    func commonInit() {
        
        self.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        
        self.rowHeight = UITableViewAutomaticDimension
        self.estimatedRowHeight = 80

        self.separatorStyle = .none
        self.backgroundView = nil
        self.backgroundColor = UIColor.clear

        self.delegate = self
        self.dataSource = self
        
        self.delaysContentTouches = false
    }
    
    func setData(_ data: [Document]) {
        self.tableData = data
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

// UITableViewDelegate
extension YourDocumentTableView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // when table item is selected
        
    }
}

// UITableViewDataSource
extension YourDocumentTableView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! YourDocumentTableViewCell
        
        cell.setCellData(tableData[indexPath.section], TableView: self)
       
        return cell
    }
    
    override func numberOfRows(inSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
}

class YourDocumentTableViewCell : UITableViewCell {

    var cellData: Document!
    var tableView: YourDocumentTableView!

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var expireAtLabel: UILabel!
    @IBOutlet weak var viewButton: UIButton!
    
    @IBOutlet weak var wrapperView: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.updateAdditionalUI()
    }
    
    func updateAdditionalUI() {
        
        self.wrapperView.layer.cornerRadius = 4
        self.wrapperView.clipsToBounds = true
        self.wrapperView.layer.borderColor = UIColor.darkGray.cgColor
        self.wrapperView.layer.borderWidth = 1
    }
    
    func setCellData(_ data: Document, TableView tableView: YourDocumentTableView) {
        
        
        self.cellData = data
        self.tableView = tableView
        
        self.nameLabel.text = self.cellData.name

        if let providerDocument = self.cellData.providerDocument {

            self.expireAtLabel.text = providerDocument.expiresAt

            self.viewButton.isUserInteractionEnabled = true

            
            if providerDocument.status == .assessing {
                self.statusLabel.text = "ASSESSING"
                self.statusLabel.backgroundColor = UIColor.init(red: 236 / 255, green: 171 / 255, blue: 32 / 255, alpha: 1)
            } else if providerDocument.status == .active {
                self.statusLabel.text = "ACTIVE"
                self.statusLabel.backgroundColor = UIColor.init(red: 98 / 255, green: 154 / 255, blue: 65 / 255, alpha: 1)
            } else {
                self.statusLabel.text = "MISSING"
                self.statusLabel.backgroundColor = UIColor.init(red: 202 / 255, green: 59 / 255, blue: 39 / 255, alpha: 1)
            }
        } else {
            self.statusLabel.text = "MISSING"
            self.statusLabel.backgroundColor = UIColor.init(red: 202 / 255, green: 59 / 255, blue: 39 / 255, alpha: 1)
            
            self.viewButton.isUserInteractionEnabled = false
            
        }
        
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    func commonInit() {
        let clearView = UIView()
        clearView.backgroundColor = UIColor.clear
        self.selectedBackgroundView = clearView
        
        self.backgroundView = clearView
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        self.selectionStyle = .none

    }
    
    @IBAction func onViewButtonClick(_ sender: Any) {
       
        
        guard let providerDocument = self.cellData.providerDocument else { return }
        
        if let vc = self.tableView.parentVC as? YourDocumentsViewController {
            vc.showDocument(providerDocument)
        }
        
    }
    
    @IBAction func onUploadButtonClick(_ sender: Any) {
        
        if let vc = self.tableView.parentVC as? YourDocumentsViewController {
            vc.uploadDocument(self.cellData)
        }
        
    }
    
    
   
}
